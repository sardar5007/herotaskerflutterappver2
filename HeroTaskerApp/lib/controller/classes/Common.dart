import 'dart:io';
import 'package:aitl/data/model/country_picker/Repository/countries_json.dart';
import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:html/parser.dart';
import 'package:intl/intl.dart';

class Common {
  static Future<String> getUDID(BuildContext context) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  static String getDeviceType() {
    return Platform.isAndroid ? 'Android' : 'iOS';
  }

  static String unEscapeString(String str) {
    //return json.decode(json.decode(str));
    return str;
  }

  static String removeTag(htmlString) {
    var document = parse(htmlString);
    String parsedString = parse(document.body.text).documentElement.text;
    return parsedString;
  }

  static String stripCountryCodePhone(String phone) {
    phone = phone.replaceAll("+", "");
    for (Map<String, String> map in countryCodes) {
      if (phone.startsWith(map['Code'])) {
        phone = phone.replaceAll(map['Code'], "");
        break;
      }
    }
    return phone.trim();
  }

  static String getPhoneNumber(String mobileNumber) {
    var phoneNumber = mobileNumber;
    debugPrint("Phone number substring(0,2)= " + mobileNumber[0]);
    if (mobileNumber[0] == "+") {
      if (mobileNumber.substring(0, 3) == "+88") {
        phoneNumber = mobileNumber.substring(3);
      } else if (mobileNumber.substring(0, 3) == "+44") {
        phoneNumber = mobileNumber.substring(3);
      }
    } else {
      if (mobileNumber.substring(0, 2) == "88") {
        phoneNumber = mobileNumber.substring(2);
      } else if (mobileNumber.substring(0, 2) == "44") {
        phoneNumber = mobileNumber.substring(2);
      }
    }
    //remove double 00
    debugPrint("Phonenumber substring(0,2) 2= " + phoneNumber);
    if (phoneNumber.substring(0, 2) == "00") {
      phoneNumber = phoneNumber.substring(1);
    }
    return phoneNumber;
  }

  static num tryParse(input) {
    String source = input.trim();
    return int.tryParse(source) ?? double.tryParse(source);
  }

  static bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    // ignore: deprecated_member_use
    return double.parse(s, (e) => null) != null;
  }

  static String parseHtmlString(String htmlString) {
    return Bidi.stripHtmlIfNeeded(htmlString);
  }

  static findIndexFromList(List<dynamic> list, val) {
    for (int i = 0; i < list.length; i++) {
      if (list[i] == val) return i;
    }
    return 0;
  }

  static splitName(String name) {
    try {
      final fName = name.substring(0, name.indexOf(" "));
      final lName = name.substring(name.indexOf(" ") + 1);
      return {'fname': fName, 'lname': lName};
    } catch (e) {}
    return {'fname': name, 'lname': ''};
  }
}
