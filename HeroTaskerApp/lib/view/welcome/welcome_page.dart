import 'package:aitl/config/cfg/NotiCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/CommonData.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/services/PushNotificationService.dart';
import 'package:aitl/view/auth/auth_screen.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/splash/splash_page.dart';
import 'package:aitl/view/widgets/btn/MMArrowBtn.dart';
import 'package:aitl/view/widgets/images/ImgFade.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/case_alert_page.dart';
import 'package:aitl/view_model/helper/apns/InitializeAwesomeNotifications.dart';
import 'package:aitl/view_model/helper/apns/NotificationData.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> with Mixin {
  static final FirebaseMessaging _firebaseMessaging =
      FirebaseMessaging.instance;

  bool isDoneCookieCheck = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: MyTheme.bgColor));

// Create the initialization for your desired push service here
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);
      FirebaseMessaging.instance.getToken().then((value) async => {
            print("Token imaran = ${value.toString()}"),
            await PrefMgr.shared.setPrefStr("fcmTokenKey", value.toString())
          });

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        if (message.data["EntityName"] == "TestPushNotitification") {
          InitializeAwesomeNotifications.generateNotification(
              title: message.notification.title,
              body: message.notification.body);
        } else {
          notiCreator(message);
        }
        // debugPrint("Notification generation getting error ${message.data}");
      });

      //  apns
      //  https://console.firebase.google.com/project/_/notification
      //  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1
      final pushNotificationService =
          PushNotificationService(_firebaseMessaging);
      await pushNotificationService.initialise(
          callback: (enumFCM which, Map<String, dynamic> message) {
        switch (which) {
          case enumFCM.onMessage:
            fcmClickNoti(message);
            break;
          case enumFCM.onLaunch:
            break;
          case enumFCM.onResume:
            break;
          default:
        }
      });

      try {
        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();
          if (userData.userModel.isMobileNumberVerified) {
            Get.to(() => DashboardPage()).then((value) {
              setState(() {
                isDoneCookieCheck = true;
              });
            });
          } else {
            if (!Server.isOtp) {
              Get.to(() => DashboardPage()).then((value) {
                setState(() {
                  isDoneCookieCheck = true;
                });
              });
            } else {
              Get.off(() =>
                  AuthScreen()); //Sms2Page(mobile: userData.userModel.mobileNumber));
            }
          }
        } else {
          Future.delayed(const Duration(seconds: 2), () {
            setState(() {
              isDoneCookieCheck = true;
            });
          });
        }
      } catch (e) {
        log(e.toString());
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted && comData.isNotiTestPage != null) {
        if (comData.isNotiTestPage) {
          FlutterRingtonePlayer.play(
            android: AndroidSounds.notification,
            ios: IosSounds.glass,
            looping: false, // Android only - API >= 28
            volume: 0.1, // Android only - API >= 28
            asAlarm: false, // Android only - all APIs
          );

          Get.to(() => CaseAlertPage(
                message: message,
              )).then((value) async {
            await userData.setUserModel();
            Get.off(() => DashboardPage()).then((value) {
              setState(() {
                isDoneCookieCheck = true;
              });
            });
          });
        } else {
          final msg = message['data']['Message'] +
              '\n\n' +
              message['data']['Description'];
          showAlert(msg: msg);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (!isDoneCookieCheck)
                ? SplashScreen()
                : Container(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(height: 40),
                          drawHeaderImage(),
                          SizedBox(height: 20),
                          drawCenterView(),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, top: 40),
                            child: MMArrowBtn(
                                txt: "Let's Start",
                                height: getHP(context, MyTheme.btnHpa),
                                width: getW(context),
                                callback: () {
                                  Get.to(() => AuthScreen());
                                }),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    ),
                  ),
          )),
    );
  }

  drawHeaderImage() {
    return Container(
      //width: getWP(context, 70),
      //height: getHP(context, 30),
      child: SvgPicture.asset(
        'assets/images/welcome/welcome_theme.svg',
        //fit: BoxFit.fill,
      ),
    );
  }

  drawCenterView() {
    return Container(
        child: Column(
      children: [
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Txt(
            txt: "Welcome to the HeroTasker app",
            txtColor: MyTheme.timelineTitleColor,
            txtSize: MyTheme.txtSize + .2,
            txtAlign: TextAlign.center,
            isBold: true,
            //txtLineSpace: 1.5,
          ),
        ),
        Container(
          width: getW(context),
          height: getHP(context, 30),
          child: ImgFade(url: "assets/images/welcome/welcome_banner.png"),
        ),
        Padding(
          padding: EdgeInsets.only(left: 20, right: 20, top: 10),
          child: Txt(
              txt:
                  "By using the HeroTasker app you can get many different types of work done.",
              txtColor: MyTheme.blueColor,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false
              //txtLineSpace: 1.5,
              ),
        ),
      ],
    ));
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // Use this method to automatically convert the push data, in case you gonna use our data standard
  // AwesomeNotifications().createNotificationFromJsonData(message.data);

  notiCreator(message);
}

notiCreator(RemoteMessage message) {
  debugPrint("notification Message data get ${message.data}");

  try {
    NotificationData notiModel = new NotificationData.fromJson(message.data);

    Map<String, dynamic> notiMap =
        getNotiMapLocalNotification(model: notiModel);

    String txt = notiMap['txt'] != null ? notiMap['txt'].toString() : "";

    InitializeAwesomeNotifications.generateNotification(
        title: notiModel.initiatorName, body: txt);
  } catch (e) {
    debugPrint("Notification generation getting error $e");
  }
}

Map<String, dynamic> getNotiMapLocalNotification({NotificationData model}) {
  Map<String, dynamic> notiMap = {};
  try {
    for (var map in NotiCfg.EVENT_LIST) {
      int notificationEventId = map['notificationEventId'];

      if (model.entityName == map["entityName"] &&
          (model.notificationEventId.toString() ==
                  notificationEventId.toString() ||
              notificationEventId == 0)) {
        String txt = map["txt"];
        txt = txt.replaceAll("#InitiatorDisplayName#", model.initiatorName);
        txt = txt.replaceAll("#EventName#", model.message);
        txt = txt.replaceAll('[] ', '');
        notiMap['txt'] = txt ?? '';

        break;
      }
    }
  } catch (e) {
    print(e.toString());
  }
  return notiMap;
}
