import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APICouncilCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/model/auth/RegAPIModel.dart';
import 'package:aitl/data/model/dashboard/others/council/CouncilAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/auth/otp/sms_page2.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/datepicker/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view/widgets/radio/RadioButtonitem.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import 'auth_base.dart';

class RegView2 extends StatefulWidget {
  final String fname;
  final String lname;
  final String email;
  final String pwd;
  final String countryDialCode;
  final String mobile;
  final String dob2;
  final String regAddr2;
  final Location loc2;
  final genderEnum gender2;

  final int councilIndex2;
  final int userCompanyCouncilId2;
  final String optDD2;

  const RegView2({
    Key key,
    @required this.fname,
    @required this.lname,
    @required this.email,
    @required this.pwd,
    @required this.countryDialCode,
    @required this.mobile,
    this.dob2,
    this.regAddr2,
    this.loc2,
    this.gender2,
    this.councilIndex2,
    this.userCompanyCouncilId2,
    this.optDD2,
  }) : super(key: key);
  @override
  State createState() => _RegView2State();
}

enum genderEnum { male, female }

class _RegView2State extends BaseAuth<RegView2> with APIStateListener {
  //  reg2
  String dob = "";
  String regAddr = "";
  Location loc;
  genderEnum _gender = genderEnum.male;

  //` council init
  List<UserCompanyInfos> listUserCompanyInfos = [];
  int councilIndex = 1;
  int userCompanyCouncilId = 0;
  DropListModel ddCouncil = DropListModel([]);
  OptionItem optCouncil = OptionItem(id: null, title: "Select Council");

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.login && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await DBMgr.shared.setUserProfile(
                user: model.responseData.user,
                //otpID: "",
                //otpMobileNumber: model.responseData.otpMobileNumber,
              );
              await userData.setUserModel();
            } catch (e) {
              log(e.toString());
            }
            if (!Server.isOtp) {
              Get.off(() => DashboardPage());
            } else {
              if (model.responseData.user.isMobileNumberVerified) {
                Get.off(() => DashboardPage());
              } else {
                Get.to(
                  () => Sms2Page(
                    mobile: widget.mobile,
                  ),
                ).then((value) {
                  //callback(route);
                });
              }
            }
          } else {
            try {
              showToast(
                context: context,
                msg: "You have entered invalid credentials",
              );
            } catch (e) {
              log(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.reg1 &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              //  recall login to get cookie
              APIViewModel().req<LoginAPIModel>(
                context: context,
                apiState: APIState(APIType.login, this.runtimeType, null),
                url: APIAuthCfg.LOGIN_URL,
                param: LoginHelper().getParam(
                    email: widget.email.trim(), pwd: widget.pwd.trim()),
                reqType: ReqType.Post,
                isCookie: true,
              );
            } else {
              try {
                final err = model.errorMessages.register[0].toString();
                showToast(context: context, msg: err);
              } catch (e) {
                log(e.toString());
              }
            }
          } catch (e) {
            log(e.toString());
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    listUserCompanyInfos = null;
    ddCouncil = null;
    optCouncil = null;
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      if (widget.dob2 != null) {
        dob = widget.dob2;
      }
      if (widget.regAddr2 != null) {
        regAddr = widget.regAddr2;
      }
      if (widget.loc2 != null) {
        loc = widget.loc2;
      }
      if (widget.gender2 != null) {
        _gender = widget.gender2;
      }
      if (widget.councilIndex2 != null) {
        councilIndex = widget.councilIndex2;
      }
      if (widget.userCompanyCouncilId2 != null) {
        userCompanyCouncilId = widget.userCompanyCouncilId2;
      }
      if (widget.optDD2 != null) {
        optCouncil.title = widget.optDD2;
      }
    } catch (e) {}
    try {
      APIViewModel().req<CouncilGetAPIModel>(
          context: context,
          url: APICouncilCfg.COUNCIL_GET_URL,
          reqType: ReqType.Get,
          callback: (model) {
            if (model != null && mounted) {
              if (model.success) {
                listUserCompanyInfos = model.responseData.userCompanyInfos;
                if (listUserCompanyInfos != null) {
                  List<OptionItem> list = [];
                  for (var model2 in listUserCompanyInfos) {
                    list.add(
                        OptionItem(id: model2.id, title: model2.companyName));
                  }
                  ddCouncil = DropListModel(list);
                  setState(() {});
                }
              }
            }
          });
    } catch (e) {}
  }

  validate() {
    if (!UserProfileVal().isDOBOK(context, dob, MyTheme.redColor)) {
      return false;
    } else if (regAddr == "") {
      showToast(context: context, msg: "Please pick your address");
      return false;
    } else if (councilIndex == 1 && userCompanyCouncilId == 0) {
      showToast(
          context: context,
          msg: "Please choose your council from the list below");
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final DateTime dateNow = DateTime.now();
    return SafeArea(
      //  ******** On RegView2
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          leading: IconButton(
              onPressed: () {
                final map = {
                  'dob2': dob,
                  'regAddr2': regAddr,
                  'loc2': loc,
                  'gender2': _gender,
                  'councilIndex2': councilIndex,
                  'userCompanyCouncilId2': userCompanyCouncilId,
                  'optDD2': optCouncil.title,
                };
                Get.back(result: map);
              },
              icon: Icon(Icons.arrow_back_ios)),
          title: UIHelper().drawAppbarTitle(title: 'Create an account'),
          centerTitle: false,
        ),
        body: Container(
          width: getW(context),
          height: getH(context),
          color: Colors.white,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 20),
                  //drawCompSwitch(),
                  Txt(
                      txt: "Provide a date of birth",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 5),
                  Txt(
                      txt:
                          "To signup, you must be 18 or older. Other people won't see your birthday.",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .3,
                      txtAlign: TextAlign.start,
                      isBold: true),
                  SizedBox(height: 10),
                  DatePickerView(
                    cap: null,
                    capTxtColor: Colors.grey,
                    txtColor: Colors.black,
                    borderWidth: 1,
                    dt: (dob == '') ? 'Select a date' : dob,
                    initialDate:
                        DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                    firstDate: DateTime(
                        dateNow.year - 100, dateNow.month, dateNow.day),
                    lastDate:
                        DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                    callback: (value) {
                      if (mounted) {
                        setState(() {
                          try {
                            dob = DateFormat(DateFun.getDOBFormat())
                                .format(value)
                                .toString();
                          } catch (e) {
                            log(e.toString());
                          }
                        });
                      }
                    },
                  ),
                  SizedBox(height: 30),
                  drawGender(),
                  SizedBox(height: 10),
                  Txt(
                    txt: "Address",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  SizedBox(height: 10),
                  GPlacesView(
                      title: null,
                      address: regAddr,
                      callback: (String address, Location loc) {
                        regAddr = address;
                        this.loc = loc;
                        setState(() {});
                      }),
                  SizedBox(height: 30),
                  drawCouncilView(),
                  SizedBox(height: 60),
                  MMBtn(
                    txt: "Get Started",
                    width: getW(context),
                    height: getHP(context, MyTheme.btnHpa),
                    radius: 20,
                    callback: () {
                      if (validate()) {
                        APIViewModel().req<RegAPIModel>(
                          context: context,
                          apiState:
                              APIState(APIType.reg1, this.runtimeType, null),
                          url: APIAuthCfg.REG_URL,
                          reqType: ReqType.Post,
                          param: {
                            "Address": regAddr.toString().trim(),
                            "Agreement": true,
                            "BriefBio": "",
                            "Cohort": (_gender == genderEnum.male)
                                ? "Male"
                                : "Female",
                            "CommunityId": "",
                            "ConfirmPassword": widget.pwd.trim(),
                            "DateofBirth": dob,
                            "DeviceType": Common.getDeviceType(),
                            "Email": widget.email.trim(),
                            "FirstName": widget.fname.trim(),
                            "Headline": "",
                            "LastName": widget.lname.trim(),
                            "Latitude": loc.lat,
                            "Longitude": loc.lng,
                            "LinkedUrl": "",
                            "MobileNumber": widget.countryDialCode +
                                ' ' +
                                widget.mobile.trim(),
                            "Password": widget.pwd.trim(),
                            "ReferenceId": "",
                            "ReferenceType": "",
                            "ReferrerId": 0,
                            "Remarks": "",
                            "ReturnUrl": "",
                            "StanfordWorkplaceURL": "",
                            "TwitterUrl": "",
                            "Version": "28",
                            "IsCouncilTask": (councilIndex == 1) ? true : false,
                            "UserCompanyId": userCompanyCouncilId,
                          },
                        );
                      }
                    },
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Txt(
            txt: "Gender",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          Theme(
            data: Theme.of(context).copyWith(
              unselectedWidgetColor: Colors.black,
              disabledColor: Colors.black,
              selectedRowColor: Colors.black,
              indicatorColor: Colors.black,
              toggleableActiveColor: Colors.black,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.male;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.male,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Male",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.female;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.female,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Female",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawCouncilView() {
    return Container(
        child: Align(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
            txt: "Do you want to select council?",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            isBold: false,
          ),
          //SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                  child: radioButtonitem(
                      index: 1,
                      text: "Yes",
                      bgColor: (councilIndex == 1) ? '#252551' : '#FFF',
                      textColor:
                          (councilIndex == 1) ? Colors.white : Colors.black,
                      callback: (i) {
                        councilIndex = i;
                        setState(() {});
                      })),
              SizedBox(width: 10),
              Expanded(
                  child: radioButtonitem(
                      index: 0,
                      text: "No",
                      bgColor: (councilIndex == 0) ? '#252551' : '#FFF',
                      textColor:
                          (councilIndex == 0) ? Colors.white : Colors.black,
                      callback: (i) {
                        councilIndex = i;
                        userCompanyCouncilId = 0;
                        optCouncil =
                            OptionItem(id: null, title: "Select Council");
                        setState(() {});
                      })),
            ],
          ),
          (ddCouncil.listOptionItems.length > 0 && councilIndex == 1)
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: DropDownListDialog(
                    context: context,
                    title: optCouncil.title,
                    ddTitleList: ddCouncil,
                    callback: (optionItem) {
                      optCouncil = optionItem;
                      userCompanyCouncilId = optionItem.id;
                      setState(() {});
                    },
                  ),
                )
              : SizedBox(),
        ],
      ),
    ));
  }
}
