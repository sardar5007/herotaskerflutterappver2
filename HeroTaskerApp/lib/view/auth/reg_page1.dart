import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view/widgets/input/InputBoxHT.dart';
import 'package:aitl/view/widgets/input/InputMobFlagBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/TCView.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'auth_base.dart';
import 'reg_page2.dart';

class RegView extends StatefulWidget {
  @override
  State createState() => _RegViewState();
}

class _RegViewState extends BaseAuth<RegView> with APIStateListener {
  //  reg1
  final fname = TextEditingController();
  final lname = TextEditingController();
  final email = TextEditingController();
  final pwd = TextEditingController();

  final mobile = TextEditingController();
  String countryDialCode = AppDefine.COUNTRY_DIALCODE;

  //  reg page 2 instance init for back
  String dob2;
  String regAddr2;
  Location loc2;
  genderEnum gender2;
  int councilIndex2;
  int userCompanyCouncilId2;
  String optDD2;
  //

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    pwd.dispose();
    fname.dispose();
    lname.dispose();
    mobile.dispose();
    //_compName.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  bool validate() {
    if (!UserProfileVal().isFNameOK(context, fname, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, lname, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, email, "Invalid email address", MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, mobile, MyTheme.redColor)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, pwd, MyTheme.redColor)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: getW(context),
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: 50),
            AuthHelper().drawAuthHeaderImage(context),
            Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 40, right: 40),
                child: Column(
                  children: [
                    InputBox(
                      ctrl: fname,
                      lableTxt: "First name",
                      kbType: TextInputType.name,
                      len: 20,
                      ecap: eCap.Word,
                    ),
                    InputBox(
                      ctrl: lname,
                      lableTxt: "Last name",
                      kbType: TextInputType.name,
                      len: 20,
                      ecap: eCap.Word,
                    ),
                    InputBox(
                      ctrl: email,
                      lableTxt: "Email",
                      kbType: TextInputType.emailAddress,
                      len: 50,
                    ),
                    InputMobFlagBox(
                      ctrl: mobile,
                      lableTxt: "Mobile number",
                      len: 15,
                      getCountryCode: (code) {
                        countryDialCode = code.toString();
                        PrefMgr.shared.setPrefStr("countryName", code.code);
                        PrefMgr.shared
                            .setPrefStr("countryCode", code.toString());
                      },
                    ),
                    InputBox(
                      ctrl: pwd,
                      lableTxt: "Password",
                      kbType: TextInputType.text,
                      len: 20,
                      isPwd: true,
                    ),
                    SizedBox(height: 20),
                    MMBtn(
                      txt: "Create an account",
                      width: getW(context),
                      height: getHP(context, MyTheme.btnHpa),
                      radius: 20,
                      callback: () async {
                        if (validate()) {
                          Get.to(() => RegView2(
                                fname: fname.text.trim(),
                                lname: lname.text.trim(),
                                email: email.text.trim(),
                                pwd: pwd.text.trim(),
                                countryDialCode: countryDialCode,
                                mobile: mobile.text,
                                dob2: dob2,
                                regAddr2: regAddr2,
                                loc2: loc2,
                                gender2: gender2,
                                councilIndex2: councilIndex2,
                                userCompanyCouncilId2: userCompanyCouncilId2,
                                optDD2: optDD2,
                              )).then((map) {
                            dob2 = map['dob2'];
                            regAddr2 = map['regAddr2'];
                            loc2 = map['loc2'];
                            gender2 = map['gender2'];
                            councilIndex2 = map['councilIndex2'];
                            userCompanyCouncilId2 =
                                map['userCompanyCouncilId2'];
                            optDD2 = map['optDD2'];
                            setState(() {});
                          });
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Txt(
              txt: "Or continue with",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 40, right: 40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getW(context),
                    icon: "ic_fb",
                    txt: "Continue with Facebook",
                    callback: () {
                      loginWithFB(this.runtimeType);
                    },
                  ),
                  SizedBox(height: 10),
                  AuthHelper().drawGoogleFBLoginButtons(
                    context: context,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getW(context),
                    icon: "ic_google",
                    txt: "Continue with Google",
                    callback: () {
                      loginWithGoogle(this.runtimeType);
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 40, right: 40),
              child: Center(child: TCView(enum_tc: eTC.REG_TC)),
            ),
            SizedBox(height: 50),
          ],
        ),
      ),
    );
  }
}
