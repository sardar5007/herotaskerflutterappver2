import 'dart:io';

import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/otp/sms1/MobileUserOTPModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/data/model/auth/otp/sms2/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../auth_screen.dart';

class Sms3Screen extends StatefulWidget {
  final MobileUserOTPModel mobileUserOTPModel;
  final mobile;
  const Sms3Screen({
    Key key,
    @required this.mobileUserOTPModel,
    @required this.mobile,
  }) : super(key: key);
  @override
  State createState() => new _Sms3ScreenState();
}

class _Sms3ScreenState extends State<Sms3Screen>
    with APIStateListener, SingleTickerProviderStateMixin, Mixin {
  bool isLoading = false;

  // Variables
  static const int SMS_AUTH_CODE_LEN = 6;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fiveDigit;
  int _sixDigit;
  String otpCode;

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.otp_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            await APIViewModel().req<LoginRegOtpFBAPIModel>(
                context: context,
                url: APIAuthCfg.LIGIN_REG_FB_MOBILE,
                reqType: ReqType.Post,
                param: {
                  "MobileNumber": (model as MobileUserOtpPutAPIModel)
                      .responseData
                      .userOTP
                      .mobileNumber,
                  "OTPCode": otpCode,
                  "DeviceType": (Platform.isAndroid) ? 'Android' : 'iOS',
                  "Persist": true,
                },
                callback: (model2) async {
                  try {
                    await DBMgr.shared
                        .setUserProfile(user: model2.responseData.user);
                    await userData.setUserModel();
                  } catch (e) {}
                  Get.offAll(() => DashboardPage());
                });
          } else {
            try {
              final err = model.errorMessages.postUserotp[0].toString();
              showToast(context: context, msg: err);
            } catch (e) {}
          }
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  void clearOtp() {
    _sixDigit = null;
    _fiveDigit = null;
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    if (mounted) {
      setState(() {});
    }
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  // Return "OTP" input field
  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _otpTextField(_firstDigit),
        _otpTextField(_secondDigit),
        _otpTextField(_thirdDigit),
        _otpTextField(_fourthDigit),
        _otpTextField(_fiveDigit),
        _otpTextField(_sixDigit),
      ],
    );
  }

  // Returns "Resend" button
  get _getResendButton {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context, widget.mobile);
      },
      child: Center(
        child: Txt(
          txt: "I didn't get a code",
          txtColor: MyTheme.brandColor,
          txtSize: MyTheme.txtSize - .1,
          txtAlign: TextAlign.center,
          isBold: false,
        ),
      ),
    );
  }

  // Returns "Otp" keyboard
  get _getOtpKeyboard {
    return new Container(
        height: getW(context) - 80,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new SizedBox(
                    width: 80.0,
                  ),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Colors.black,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            if (_sixDigit != null) {
                              _sixDigit = null;
                            } else if (_fiveDigit != null) {
                              _fiveDigit = null;
                            } else if (_fourthDigit != null) {
                              _fourthDigit = null;
                            } else if (_thirdDigit != null) {
                              _thirdDigit = null;
                            } else if (_secondDigit != null) {
                              _secondDigit = null;
                            } else if (_firstDigit != null) {
                              _firstDigit = null;
                            }
                          });
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          //automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
        ),
        bottomNavigationBar: BottomAppBar(
          color: MyTheme.bgColor,
          child: _getOtpKeyboard,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getW(context),
      //height: getH(context),
//        padding: new EdgeInsets.only(bottom: 16.0),
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
                child: Center(
                  child: Txt(
                    txt: "Enter the code that was sent to\n" + widget.mobile,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ),
              ),
              SizedBox(height: 20),
              _getInputField,
              SizedBox(height: 20),
              _getResendButton,
              SizedBox(height: 20),
              Center(
                child: CupertinoButton(
                  onPressed: () =>
                      {Get.offAll(() => AuthScreen(initialIndex: 0))},
                  color: MyTheme.redColor,
                  borderRadius: new BorderRadius.circular(20.0),
                  child: new Text(
                    "Continue",
                    textAlign: TextAlign.center,
                    style: new TextStyle(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    int boxSpace = 2 * 6;
    double boxW = (getWP(context, 100) / SMS_AUTH_CODE_LEN) - boxSpace;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      child: Txt(
          txt: digit != null ? digit.toString() : "",
          txtColor: Colors.black,
          txtSize: MyTheme.txtSize + .5,
          txtAlign: TextAlign.start,
          isBold: false),
      decoration: BoxDecoration(
        border: Border.all(color: MyTheme.brandColor),
        //color: MyTheme.redColor,
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(40.0),
        child: new Container(
          height: 80.0,
          width: 80.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
          ),
          child: new Center(
            child: Txt(
                txt: label,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + 1.8,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) async {
    if (mounted) {
      var countryCode = await PrefMgr.shared.getPrefStr("countryCode");
      if (countryCode == null) countryCode = AppDefine.COUNTRY_DIALCODE;
      setState(() {
        _currentDigit = i;
        if (_firstDigit == null) {
          _firstDigit = _currentDigit;
        } else if (_secondDigit == null) {
          _secondDigit = _currentDigit;
        } else if (_thirdDigit == null) {
          _thirdDigit = _currentDigit;
        } else if (_fourthDigit == null) {
          _fourthDigit = _currentDigit;
        } else if (_fiveDigit == null) {
          _fiveDigit = _currentDigit;
        } else if (_sixDigit == null) {
          _sixDigit = _currentDigit;
          otpCode = _firstDigit.toString() +
              _secondDigit.toString() +
              _thirdDigit.toString() +
              _fourthDigit.toString() +
              _fiveDigit.toString() +
              _sixDigit.toString();
          if (otpCode.length == SMS_AUTH_CODE_LEN) {
            //signInWithPhoneNumber(otpCode);
            APIViewModel().req<MobileUserOtpPutAPIModel>(
              context: context,
              apiState: APIState(APIType.otp_put, this.runtimeType, null),
              url: APIAuthCfg.LOGIN_MOBILE_OTP_PUT_URL,
              reqType: ReqType.Put,
              param: {
                "MobileNumber": countryCode + widget.mobile,
                "OTPCode": otpCode,
                "UserId":
                    (userData.userModel != null) ? userData.userModel.id : 0,
                "DeviceType": (Platform.isAndroid) ? 'Android' : 'iOS',
                "Persist": true,
              },
            );
          }
        }
      });
    }
  }
}
