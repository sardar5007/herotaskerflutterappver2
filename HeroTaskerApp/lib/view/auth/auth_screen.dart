import 'dart:ui';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/auth/reg_page1.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

class AuthScreen extends StatefulWidget {
  bool isSwitch = true;
  String fname = '';
  String lname = '';
  String email = '';
  String mobile = '';
  String pwd = '';
  String compName = '';
  int initialIndex;
  AuthScreen({this.initialIndex = 1});
  @override
  State createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with Mixin {
  @override
  void initState() {
    super.initState();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: widget.initialIndex,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            backgroundColor: MyTheme.bgColor,
            iconTheme: MyTheme.themeData.iconTheme,
            elevation: MyTheme.appbarElevation,
            /*leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.back();
                }),*/
            title: UIHelper().drawAppbarTitle(title: 'Join HeroTasker'),
            centerTitle: false,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(getHP(context, 7)),
              child: Container(
                color: MyTheme.redColor,
                height: getHP(context, MyTheme.btnHpa),
                child: TabBar(
                  //labelColor: Colors.deepOrange,
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorWeight: 3,
                  tabs: [
                    Container(
                        //color: MyTheme.blueColor,
                        //height: getHP(context, MyTheme.btnHpa),
                        child: Txt(
                      txt: "CREATE AN ACCOUNT",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                    Container(
                        child: Txt(
                      txt: "LOG IN",
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                  ],
                ),
              ),
            ),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: TabBarView(
              children: [
                RegView(),
                LoginView(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
