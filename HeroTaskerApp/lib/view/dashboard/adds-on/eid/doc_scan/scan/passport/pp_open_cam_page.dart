import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'pp_open_cam_base.dart';

class OpenCamPPPage extends StatefulWidget {
  const OpenCamPPPage({Key key}) : super(key: key);
  @override
  State createState() {
    return _OpenCamPPPageState();
  }
}

class _OpenCamPPPageState extends OpenCamPPBase<OpenCamPPPage>
    with WidgetsBindingObserver, TickerProviderStateMixin, Mixin {
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController cameraController = controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
  }

  loadCam() async {
    final cameras = await availableCameras();
    controller = CameraController(cameras[0], ResolutionPreset.veryHigh);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    loadCam();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          key: scaffoldKey,
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          /*/appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Smart card completed",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize - .5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),*/
          body: drawLayout()),
    );
  }

  drawLayout() {
    if (controller == null) return drawEmpty();
    return controller.value.isInitialized
        ? Stack(children: [
            Positioned.fill(
                child: AspectRatio(
                    aspectRatio: controller.value.aspectRatio,
                    child: CameraPreview(controller))),
            new Positioned.fill(
              child: new Image.asset(
                'assets/images/doc_scan/cam_card_bg.png',
                fit: BoxFit.fill,
              ),
            ),
            Positioned(
                top: getHP(context, 1),
                right: 5,
                child: Container(
                  //width: getW(context),
                  child: IconButton(
                      onPressed: () {
                        Get.back();
                      },
                      icon: Icon(
                        Icons.close,
                        color: MyTheme.redColor,
                        size: 25,
                      )),
                )),
            Positioned(
                top: getHP(context, 20),
                child: Container(
                  width: getW(context),
                  child: Center(
                    child: Text(
                      "Scan your passport",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 17,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )),
            Positioned(
                top: getHP(context, 75),
                child: /*(isFront)
                    ?*/
                    Container(
                  width: getW(context),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Colors.green),
                  child: Center(
                    child: IconButton(
                        iconSize: 40,
                        icon: const Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                        ),
                        color: Colors.blue,
                        onPressed: onTakePictureButtonPressed),
                  ),
                )
                /*: Container(
                        width: getW(context),
                        child: IconButton(
                            iconSize: 40,
                            onPressed: onTakePictureButtonPressed,
                            icon: Icon(
                              Icons.refresh,
                              color: Colors.white,
                            ))),*/
                ),
          ])
        : drawEmpty();
  }

  drawEmpty() {
    return Center(child: CircularProgressIndicator());
  }
}
