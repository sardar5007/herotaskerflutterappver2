import 'dart:convert';

import 'package:aitl/config/server/APIEidVeriCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/ScanDocData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/ads-on/eid/doc_scan/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/scan_id_doc_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/selfie_open_cam_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/uploading_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:json_string/json_string.dart';
import 'package:mime_type/mime_type.dart';

class UploadingPage extends StatefulWidget {
  const UploadingPage({Key key}) : super(key: key);

  @override
  State createState() => _UploadingPageState();
}

class _UploadingPageState extends UploadingBase<UploadingPage> {
  bool isUploadingDone = true;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    //progController.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  initPage() async {
    try {
      String mimeType = mime(scanDocData.file_selfie.path);
      var fileName = (scanDocData.file_selfie.path.split('/').last);
      //String mimee = mimeType.split('/')[0];
      String type = mimeType.split('/')[1];
      List<int> fileInByte = scanDocData.file_selfie.readAsBytesSync();
      final imageData = base64Encode(fileInByte);
      print(imageData);
      final param = {
        "UserCompanyId": userData.userModel.userCompanyID,
        "ImageType": type,
        "FileName": fileName,
        "UserId": userData.userModel.id,
        "ContentData": imageData,
      };
      final jsonString = JsonString(json.encode(param));
      log(jsonString.source);
      await APIViewModel().req<PostSelfieByBase64DataAPIModel>(
          context: context,
          url: APIEidCfg.SELFIE_POST_URL,
          reqType: ReqType.Post,
          param: param,
          callback: (model) async {
            if (mounted) {
              if (model != null) {
                if (model.success) {
                  try {
                    scanDocData.requestId =
                        model.responseData.responsedata.requestid;
                    Get.off(() => ScanIDDocPage()).then((value) => Get.back());
                  } catch (e) {
                    //showAlert(msg: e.toString());
                    tryAgainAlert();
                  }
                } else {
                  tryAgainAlert();
                }
              } else {
                tryAgainAlert();
              }
            }
          });
    } catch (e) {}
  }

  tryAgainAlert() {
    progController.progress.value = 0;
    isUploadingDone = false;
    setState(() {});
    showConfirmDialog(
      context: context,
      title: "Uploading Alert",
      msg: "Something went wrong. Try again?",
      callback: () {
        initPage();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Take your selfie",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize - .5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          /*actions: [
            IconButton(
                onPressed: () {
                  Get.off(() => OpenCamPPPage());
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.greenColor,
                  size: 20,
                ))
          ],*/
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Center(
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.cloud_upload,
                color: MyTheme.redColor,
                size: 60,
              ),
              SizedBox(height: 5),
              Txt(
                  txt: "Uploading the documents",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Obx(() => Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20),
                    child: LinearProgressIndicator(
                      backgroundColor: Colors.grey,
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                      value: progController.progress.value,
                    ),
                  )),
              (!isUploadingDone)
                  ? Padding(
                      padding:
                          const EdgeInsets.only(top: 20, left: 20, right: 20),
                      child: MMBtn(
                          txt: "Try again",
                          width: getWP(context, 40),
                          height: getHP(context, 5),
                          radius: 0,
                          callback: () {
                            Get.off(() => OpenCamSelfiePage());
                          }))
                  : SizedBox()
            ],
          ),
        ),
      ),
    );
  }
}
