import 'dart:io';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/ScanDocData.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/utils/scan_mixin.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';

abstract class PPReviewBase<T extends StatefulWidget> extends State<T>
    with
        Mixin,
        WidgetsBindingObserver,
        SingleTickerProviderStateMixin,
        ScanMixin {
  bool isFrontAnim = false;

  AnimationController animationController;

  final ppType = TextEditingController();
  final ppCode = TextEditingController();
  final ppNo = TextEditingController();
  final ppSurName = TextEditingController();
  final ppGivenName = TextEditingController();
  final ppPlaceBirth = TextEditingController();
  final ppAuthority = TextEditingController();

  DropListModel ddMaritalNationalities;
  OptionItem optNationalities;

  var dob = "";
  var dtIssue = "";
  var dtExpiry = "";
  String last2lines = "";

  bool isAnim = true;

  drawLayout();

  drawPicBox() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        width: getW(context),
        height: getHP(context, 30),
        decoration: BoxDecoration(
            color: Colors.transparent,
            border: Border.all(color: MyTheme.redColor, width: 1)),
        child: Image.file(
          scanDocData.file_pp_front,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  drawIssues(List<String> list) {
    return Container(
      color: MyTheme.l3BlueColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: ListView.builder(
            shrinkWrap: true,
            primary: false,
            itemCount: list.length,
            itemBuilder: (context, index) {
              return Txt(
                  txt: list[index],
                  txtColor: Colors.blue,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false);
            }),
      ),
      //return Txt(txt: list[i], txtColor: Colors.blue, txtSize: MyTheme.txtSize, txtAlign: TextAlign.start, isBold: false)}),
    );
  }

  void animateScanAnimation(bool reverse) {
    if (reverse) {
      animationController.reverse(from: 1.0);
    } else {
      animationController.forward(from: 0.0);
    }
  }
}
