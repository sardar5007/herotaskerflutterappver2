import 'package:flutter/material.dart';

import '../../../../../../../mixin.dart';

abstract class DrvLic1Base<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();
}
