import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/ads-on/eid/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import '../result_base.dart';

class PPResultPage extends StatefulWidget {
  final PostDocVerifyAPIModel model;
  const PPResultPage({Key key, @required this.model}) : super(key: key);
  @override
  State createState() => _PPResultPageState();
}

class _PPResultPageState extends ResultBase<PPResultPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Identification of a person",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize - .5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
        ),
        body: drawLayout(eScanDocType.Passport, widget.model),
      ),
    );
  }
}
