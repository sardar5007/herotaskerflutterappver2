import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/utils/fun.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/utils/parser.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/utils/scan_mixin.dart';
import 'package:flutter/material.dart';

abstract class BaseCard<T extends StatefulWidget> extends State<T>
    with ScanMixin, Fun, ParserMixin {}
