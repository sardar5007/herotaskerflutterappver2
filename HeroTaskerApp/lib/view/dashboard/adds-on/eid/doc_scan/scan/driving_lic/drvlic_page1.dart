import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'drvlic_base1.dart';
import 'drvlic_page2.dart';

class DrvLicPage1 extends StatefulWidget {
  const DrvLicPage1({Key key}) : super(key: key);
  @override
  State createState() => _DrvLicPage1State();
}

class _DrvLicPage1State extends DrvLic1Base<DrvLicPage1> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Scan identity document",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize - .5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.redColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: getW(context),
              height: getHP(context, 50),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                      'assets/images/doc_scan/scan_driving_lic_bg.png'),
                  fit: BoxFit.fill,
                ),
                shape: BoxShape.rectangle,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "Capture the front side of the driving licence",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Txt(
                  txt:
                      "1- To prove your identity, you need to scan your driving licence. Make sure that front side of the card are clearly visible.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
              child: Txt(
                  txt:
                      "2- Display the driving license side/page with your photo in front of camera so that it fits into the frame.",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 20, right: 20, bottom: 20),
                child: MMBtn(
                    txt: "Continue",
                    //bgColor: MyTheme.redColor,
                    //txtColor: Colors.white,
                    width: getW(context),
                    height: getHP(context, 6),
                    radius: 0,
                    callback: () {
                      Get.off(() => DrvLicPage2());
                    })),
          ],
        ),
      ),
    );
  }
}
