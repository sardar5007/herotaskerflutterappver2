import 'dart:io';

import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/ImageLib.dart';
import 'package:aitl/data/app_data/ScanDocData.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/driving_lic/drvlic_open_cam_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/driving_lic/drvlic_page1.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/driving_lic/drvlic_review_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/passport/pp_page1.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/passport/pp_review_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/utils/ui_helper.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'driving_lic/drvlic_crop_image.dart';

abstract class ScanIDDocBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  drawLayout();

  drawRow() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Txt(
                  txt: "What identity document do you want to scan?",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
            ),
            SizedBox(height: 10),
            drawCell(
                0,
                Icon(
                  Icons.drive_eta_outlined,
                  color: Colors.green,
                  size: 20,
                ),
                "Driving licence",
                true),
            drawCell(
                1,
                Icon(
                  Icons.account_box,
                  color: Colors.green,
                  size: 20,
                ),
                "Passport",
                false),
          ],
        ),
      ),
    );
  }

  drawCell(i, ico, title, isLine) {
    return Container(
      color: MyTheme.greyColor,
      child: ListTile(
        onTap: () async {
          if (i == 0) {
            //  Driving Licence
            if (!Server.isOtp) {
              scanDocData.file_drvlic_front = await ImageLib()
                  .getImageFileFromAssets(
                      path: "eid_scan", img: "dada_driving.jpg");
              Get.to(() => DrvLicReviewPage()).then((value) {});
            } else {
              Get.to(() => DrvLicPage1());
            }
          } else if (i == 1) {
            //  Passport
            if (!Server.isOtp) {
              scanDocData.file_pp_front = await ImageLib()
                  .getImageFileFromAssets(path: "eid_scan", img: "pp.png");
              Get.to(() => PPReviewPage()).then((value) {});
            } else {
              Get.to(() => PPPage1());
            }
          }
        },
        minLeadingWidth: 0,
        leading: ico,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(top: 10),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
            (isLine)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: UIHelper().drawLine())
                : SizedBox(
                    height: 20,
                  )
          ],
        ),
      ),
    );
  }
}
