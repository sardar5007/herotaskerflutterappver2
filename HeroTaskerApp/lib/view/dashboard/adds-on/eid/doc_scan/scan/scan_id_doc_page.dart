import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/scan_id_doc_base.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ScanIDDocPage extends StatefulWidget {
  const ScanIDDocPage({Key key}) : super(key: key);

  @override
  _ScanIDDocPageState createState() => _ScanIDDocPageState();
}

class _ScanIDDocPageState extends ScanIDDocBase<ScanIDDocPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          iconTheme: IconThemeData(color: Colors.black //change your color here
              ),
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          title: Txt(
              txt: "Scan identity document",
              txtColor: Colors.black,
              txtSize: MyTheme.appbarTitleFontSize - .5,
              txtAlign: TextAlign.start,
              isBold: true),
          centerTitle: false,
          actions: [
            IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: Icon(
                  Icons.close,
                  color: MyTheme.redColor,
                  size: 20,
                ))
          ],
        ),
        body: drawLayout(),
      ),
    );
  }

  @override
  drawLayout() {
    return drawRow();
  }
}
