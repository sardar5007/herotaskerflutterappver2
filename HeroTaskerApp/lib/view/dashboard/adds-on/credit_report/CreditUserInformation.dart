import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditInfoParms.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditInfoPostResponse.dart';
import 'package:aitl/data/model/ads-on/credit_report/GetUserValidationOldUserResponse.dart';
import 'package:aitl/data/model/ads-on/credit_report/privacy/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/MultipleChoiceQuestions.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/datepicker/DatePicker.dart';
import 'package:aitl/view/widgets/dialog/NamePrefixAlertDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view_model/helper/adds-on/credit_report/CreditInfoPostAPIMgr.dart';
import 'package:aitl/view_model/helper/adds-on/credit_report/EditProfileHelper.dart';
import 'package:aitl/view_model/helper/adds-on/credit_report/privacy/PrivacyPolicyAPIMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

class CreditUserInformation extends StatefulWidget {
  final GetUserValidationOldUser responseData;
  CreditUserInformation({Key key, @required this.responseData})
      : super(key: key);

  @override
  _CreditUserInformationState createState() => _CreditUserInformationState();
}

class _CreditUserInformationState extends State<CreditUserInformation>
    with Mixin {
  EditProfileHelper editProfileHelper;

  //  personal info
  final _fname = TextEditingController();
  final _mname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _postCode = TextEditingController();
  final _town = TextEditingController();
  final _mobile = TextEditingController();

  String dob = "";
  bool isTermsAndConditionRead = false;

  @override
  void initState() {
    appInit();
    super.initState();
  }

  appInit() async {
    _fname.text = userData.userModel.name;
    //_mname.text = userData.userModel.middleName;
    _lname.text = userData.userModel.lastName;
    _email.text = userData.userModel.email;
    //_postCode.text = userData.userModel.postcode;
    //_town.text = userData.userModel.town;
    _mobile.text = userData.userModel.mobileNumber;
    dob = userData.userModel.dateofBirth;

    editProfileHelper = EditProfileHelper();
    editProfileHelper.optTitle = OptionItem(id: null, title: "Name Prefix");

    await editProfileHelper.getCountriesBirth(context: context, cap: "");
    await editProfileHelper.getCountriesResidential(context: context, cap: "");
    await editProfileHelper.getCountriesNationaity(context: context, cap: "");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      body: GestureDetector(
        /*  behavior: HitTestBehavior.opaque,
        onPanDown: (detail) {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },*/
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                //expandedHeight: 160,
                elevation: MyTheme.appbarElevation,
                toolbarHeight: getHP(context, 8),
                backgroundColor: Colors.white,
                iconTheme: IconThemeData(color: MyTheme.redColor),
                pinned: true,
                floating: false,
                snap: false,
                forceElevated: false,
                centerTitle: true,
                title: Txt(
                    txt: "Credit User Information",
                    txtColor: MyTheme.redColor,
                    txtSize: MyTheme.appbarTitleFontSize - .5,
                    txtAlign: TextAlign.center,
                    isBold: false),
                actions: <Widget>[],

                /*  bottom: PreferredSize(
                    preferredSize: new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
                    child: (isLoading || isBaseLoading || isCaseReviewLoading || isPrivacyPolicyFetch)
                        ? AppbarBotProgBar(
                            backgroundColor: MyTheme.appbarProgColor,
                          )
                        : SizedBox()),*/
              ),
            ];
          },
          body: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "Please fill in the details below to get started.",
                  txtColor: Colors.grey,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(
                height: 15.0,
              ),
              Container(
                  width: getW(context),
                  child: Txt(
                    txt: "Choose Title",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    isBold: true,
                    txtAlign: TextAlign.start,
                  )),
              SizedBox(
                height: 15.0,
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 9),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border(
                        left: BorderSide(color: Colors.grey, width: 1),
                        right: BorderSide(color: Colors.grey, width: 1),
                        top: BorderSide(color: Colors.grey, width: 1),
                        bottom: BorderSide(color: Colors.grey, width: 1)),
                    color: Colors.transparent),
                child: GestureDetector(
                  onTap: () {
                    showNamePrefixAlertDialog(
                        ddTitleList: editProfileHelper.ddTitle,
                        callback: (optionItem) {
                          setState(() {
                            editProfileHelper.optTitle = optionItem;
                          });

                          Navigator.of(context, rootNavigator: true).pop();
                        },
                        title: "Choose Title",
                        context: context);
                  },
                  child: new Row(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Txt(
                              txt: "${editProfileHelper.optTitle.title}",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                      ),
                      Align(
                        alignment: Alignment(1, 0),
                        child: Icon(
                          Icons.keyboard_arrow_down,
                          color: Colors.black,
                          size: 40,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              inputFieldWithText(
                  controller: _fname,
                  titleText: "First Name",
                  hintText: "First Name"),
              inputFieldWithText(
                  controller: _mname,
                  titleText: "Middle Name",
                  hintText: "Middle Name"),
              inputFieldWithText(
                  controller: _lname,
                  titleText: "Last Name",
                  hintText: "First Name"),
              SizedBox(
                height: 15,
              ),
              DatePicker(
                txtColor: Colors.black,
                cap: 'Select date of birth',
                dt: (dob == '') ? 'Select date of birth' : dob,
                initialDate: dateDOBlast,
                firstDate: dateDOBfirst,
                lastDate: dateDOBlast,
                callback: (value) {
                  if (mounted) {
                    setState(() {
                      try {
                        dob = DateFormat('dd-MM-yyyy').format(value).toString();
                      } catch (e) {
                        log(e.toString());
                      }
                    });
                  }
                },
              ),
              inputFieldWithText(
                  controller: _email,
                  titleText: "Email",
                  hintText: "Email Address",
                  keyBoardType: TextInputType.emailAddress),
              inputFieldWithText(
                  controller: _postCode,
                  titleText: "Post Code",
                  hintText: "Post Code"),
              inputFieldWithText(
                  controller: _town, titleText: "Town", hintText: "Town Name"),
              inputFieldWithText(
                  controller: _mobile,
                  titleText: "Mobile Number",
                  hintText: "Mobile Number",
                  keyBoardType: TextInputType.number),
              Txt(
                  txt:
                      "Your email address and mobile number will be used to login to your Client Portal.",
                  txtColor: Colors.grey,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              getTermsAndConditionPage(),
              SizedBox(
                height: 45,
              ),
              Btn(
                  txt: "Next",
                  txtColor: Colors.white,
                  bgColor: MyTheme.redColor,
                  //width: getW(context),
                  //height: 60,
                  callback: () {
                    //hide keyboard if is it focus able
                    if (FocusScope.of(context).hasFocus) {
                      FocusScope.of(context).unfocus();
                    }

                    AddressInfo addressInfoParams = AddressInfo(
                        abodeNumber: "1",
                        address1: "Bond Street",
                        address2: "",
                        address3: "",
                        postcode: "X9 4HJ",
                        town: "TEST TOWN");

                    Device deviceParams = Device(
                        blackBox:
                            "04003hQUMXGB0poNf94lis1ztmW5GgQ+xBQ+cvNMV/W52q6osBZcUljnE2vm03g5nuwyXNN+cc",
                        ip: "127.0.0.1");
                    IndividualDetails individualDetails = IndividualDetails(
                        dateOfBirth: "12/26/1990",
                        email: "cust30203@gmail.com",
                        forename: "Henry",
                        middleNames: "D",
                        phoneNumber: "601234569999",
                        surname: "Noble",
                        title: "Mr",
                        userCompanyId: 1003,
                        userId: 121907);

                    CreditInfoParms creditInfoParams = CreditInfoParms(
                        address: addressInfoParams,
                        device: deviceParams,
                        individualDetails: individualDetails,
                        riskLevel: "CardlessHigh");

                    print("credit information =  ${creditInfoParams.toJson()}");

                    CreditInfoPostApiMgr().creditUserInfoPost(
                        context: context,
                        creditInfoParms: creditInfoParams,
                        callback: (model) {
                          print("model.responseData =  ${model.success}");

                          if (model != null) {
                            CreditInfoPostResponseData creditInfoPostResponse =
                                model.responseData;

                            if (creditInfoPostResponse
                                    .validateNewUser
                                    .validateNewUserResult
                                    .identityValidationOutcome ==
                                10) {
                              Get.to(
                                () => MultipleChoiceQuestions(
                                    creditInfoPostResponse:
                                        creditInfoPostResponse,
                                    responseData: widget.responseData),
                              );
                            }
                          }
                        });
                  }),
              SizedBox(height: 45),
            ],
          ),
        ),
      ),
    );
  }

  inputFieldWithText(
      {TextEditingController controller,
      String titleText,
      String hintText,
      var keyBoardType}) {
    if (keyBoardType == null) {
      keyBoardType = TextInputType.text;
    }
    return Container(
      width: getW(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 10,
          ),
          Txt(
              txt: titleText,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: true),
          SizedBox(
            height: 10,
          ),
          Container(
            width: getW(context),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.white),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: new TextFormField(
                    controller: controller,
                    textInputAction: TextInputAction.next,
                    decoration: new InputDecoration(
                      hintText: hintText,
                      hintStyle: TextStyle(color: Colors.grey),
                      fillColor: Colors.black,
                    ),
                    validator: (val) {
                      if (val.length == 0) {
                        return "Name cannot be empty";
                      } else {
                        return null;
                      }
                    },
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: MyTheme.txtSize + 16,
                    ),
                    keyboardType: keyBoardType,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return MyTheme.redColor;
  }

  getTermsAndConditionPage() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 15,
          ),
          Txt(
              txt:
                  "Terms and Conditions and Privacy Notice - you need to read these.",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(
            height: 15,
          ),
          GestureDetector(
            onTap: () {
              //hide keyboard if is it focus able
              if (FocusScope.of(context).hasFocus) {
                FocusScope.of(context).unfocus();
              }

              setState(() {
                isTermsAndConditionRead
                    ? isTermsAndConditionRead = false
                    : isTermsAndConditionRead = true;
              });
            },
            child: Row(
              children: [
                Checkbox(
                    value: isTermsAndConditionRead,
                    fillColor: MaterialStateProperty.resolveWith(getColor),
                    focusColor: Colors.red,
                    checkColor: Colors.white,
                    onChanged: (value) {
                      //hide keyboard if is it focus able
                      if (FocusScope.of(context).hasFocus) {
                        FocusScope.of(context).unfocus();
                      }
                      setState(() {
                        isTermsAndConditionRead
                            ? isTermsAndConditionRead = false
                            : isTermsAndConditionRead = true;
                      });
                    }),
                Txt(
                    txt: "I confirm that I have",
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 68.0),
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: '* Read and agreed to the ',
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: Colors.black,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                      fontWeight: FontWeight.normal),
                ),
                TextSpan(
                    text: 'Terms and Conditions',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        height: MyTheme.txtLineSpace,
                        color: MyTheme.redColor,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        if (mounted) {
                          PrivacyPolicyAPIMgr().wsOnLoad(
                            context: context,
                            callback: (model) {
                              if (model != null) {
                                try {
                                  if (model.success) {
                                    debugPrint(
                                        "size = ${model.responseData.termsPrivacyNoticeSetupsList.length}");

                                    for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                                        in model.responseData
                                            .termsPrivacyNoticeSetupsList) {
                                      if (termsPrivacyPoliceSetups.type
                                              .toString() ==
                                          "Customer Privacy Notice") {
                                        Get.to(
                                          () => PDFDocumentPage(
                                            title: "Privacy",
                                            url:
                                                termsPrivacyPoliceSetups.webUrl,
                                          ),
                                        ).then((value) {
                                          //callback(route);
                                        });
                                        break;
                                      }
                                    }
                                  } else {
                                    //showToast(txtColor: Colors.white, bgColor: MyTheme.redColor,msg: model.errorMessages.toString());
                                    showToast(
                                        context: context,
                                        msg: "Sorry, something went wrong",
                                        which: 0);
                                  }
                                } catch (e) {
                                  log(e.toString());
                                }
                              } else {
                                log("dashboard screen not in");
                              }
                            },
                          );
                        }
                      })
              ]),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 68.0),
            child: RichText(
              textAlign: TextAlign.start,
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: '* Read the ',
                  style: TextStyle(
                      height: MyTheme.txtLineSpace,
                      color: Colors.black,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                      fontWeight: FontWeight.normal),
                ),
                TextSpan(
                    text: 'privacy policy.',
                    style: TextStyle(
                        decoration: TextDecoration.underline,
                        height: MyTheme.txtLineSpace,
                        color: MyTheme.redColor,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                        fontWeight: FontWeight.bold),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        // navigate to desired screen
                        Get.to(
                          () => PDFDocumentPage(
                            title: "Privacy",
                            url:
                                "https://mortgage-magic.co.uk/assets/img/privacy_policy.pdf",
                          ),
                        ).then((value) {
                          //callback(route);
                        });
                      })
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
