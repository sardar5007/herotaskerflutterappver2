import 'dart:ui';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/ads-on/credit_report/AnswerParams.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditInfoPostResponse.dart';
import 'package:aitl/data/model/ads-on/credit_report/GetUserValidationOldUserResponse.dart';
import 'package:aitl/data/model/ads-on/credit_report/KbaQuestionResponse.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/CreditDashboardTabController.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/adds-on/credit_report/GetKbaCreditQuestionAPIMgr.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MultipleChoiceQuestions extends StatefulWidget {
  final CreditInfoPostResponseData creditInfoPostResponse;
  final GetUserValidationOldUser responseData;
  MultipleChoiceQuestions({
    Key key,
    @required this.creditInfoPostResponse,
    @required this.responseData,
  }) : super(key: key);

  @override
  State createState() => _MultipleChoiceQuestionsState();
}

class _MultipleChoiceQuestionsState extends State<MultipleChoiceQuestions>
    with Mixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: Colors.white,
      body: GestureDetector(
        /*  behavior: HitTestBehavior.opaque,
        onPanDown: (detail) {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },*/
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                //expandedHeight: 160,
                elevation: MyTheme.appbarElevation,
                toolbarHeight: getHP(context, 2),
                backgroundColor: Colors.white,
                pinned: true,
                floating: false,
                snap: false,
                forceElevated: false,
                centerTitle: true,
                /*title: Txt(
                    txt: "Verify Account",
                    txtColor: MyTheme.redColor,
                    txtSize: MyTheme.appbarTitleFontSize - .5,
                    txtAlign: TextAlign.center,
                    isBold: false),
                actions: <Widget>[],*/

                /*  bottom: PreferredSize(
                    preferredSize: new Size(getW(context), getHP(context, (isLoading) ? 1 : 0)),
                    child: (isLoading || isBaseLoading || isCaseReviewLoading || isPrivacyPolicyFetch)
                        ? AppbarBotProgBar(
                            backgroundColor: MyTheme.appbarProgColor,
                          )
                        : SizedBox()),*/
              ),
            ];
          },
          body: _loading ? Container() : drawLayout(),
        ),
      ),
    );
  }

  var isCheckedList = [];
  KbaQuestionResponseData _kbaQuestionResponseData;
  bool _loading = true;

  @override
  void initState() {
    appInit();
    super.initState();
  }

  void appInit() {
    _loading = true;
    GetKbaCreditQuestionApiMgr().getKbaQuestionList(
        context: context,
        // validationIdentifier: widget.creditInfoPostResponse.validateNewUser.validateNewUserResult.validationIdentifier,
        validationIdentifier: widget.responseData.validateNewUser
            .validateNewUserResult.validationIdentifier,
        callback: (model) {
          print("response data = ${model.success}");
          if (model != null) {
            setState(() {
              _kbaQuestionResponseData = model.responseData;
              isCheckedList.length = _kbaQuestionResponseData.kbaQuestions
                  .getKbaQuestionsResult.questions.kbaQuestionItem.length;

              _loading = false;
            });
          } else {
            setState(() {
              _loading = false;
            });
          }
        });
  }

  drawLayout() {
    return Container(
        child: Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Container(
              width: MediaQuery.of(context).size.width,
              child: Txt(
                  txt: "Let us verify your account with TransUnion",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.left,
                  isBold: false)),
          SizedBox(height: 10),
          Container(
              width: MediaQuery.of(context).size.width,
              child: Txt(
                  txt:
                      "Please select the correct answers for the questions below. If we are not able to verify with the correct answers, we will not be able to provide access to your credit profile. If wrong answer selected three times, you will have to call our customer services to obtain access.",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.left,
                  isBold: false)),
          Expanded(
            flex: 13,
            child: ListView.builder(
              itemCount: _kbaQuestionResponseData.kbaQuestions
                  .getKbaQuestionsResult.questions.kbaQuestionItem.length,
              itemBuilder: (context, index) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Txt(
                          txt:
                              "${index + 1}. ${_kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult.questions.kbaQuestionItem[index].text}",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: _kbaQuestionResponseData
                            .kbaQuestions
                            .getKbaQuestionsResult
                            .questions
                            .kbaQuestionItem[index]
                            .answers
                            .kbaAnswerOption
                            .length,
                        itemBuilder: (context, subIndex) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                isCheckedList[subIndex] = true;
                                _kbaQuestionResponseData
                                        .kbaQuestions
                                        .getKbaQuestionsResult
                                        .questions
                                        .kbaQuestionItem[index]
                                        .selectedAnswer =
                                    _kbaQuestionResponseData
                                        .kbaQuestions
                                        .getKbaQuestionsResult
                                        .questions
                                        .kbaQuestionItem[index]
                                        .answers
                                        .kbaAnswerOption[subIndex]
                                        .id
                                        .toString();
                              });
                            },
                            child: radioButtonitem(
                                text:
                                    "${_kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult.questions.kbaQuestionItem[index].answers.kbaAnswerOption[subIndex].text}",
                                bgColor: _kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult.questions.kbaQuestionItem[index].selectedAnswer != null &&
                                        _kbaQuestionResponseData
                                                .kbaQuestions
                                                .getKbaQuestionsResult
                                                .questions
                                                .kbaQuestionItem[index]
                                                .selectedAnswer ==
                                            _kbaQuestionResponseData
                                                .kbaQuestions
                                                .getKbaQuestionsResult
                                                .questions
                                                .kbaQuestionItem[index]
                                                .answers
                                                .kbaAnswerOption[subIndex]
                                                .id
                                                .toString()
                                    ? '#252551'
                                    : '#FFF',
                                textColor: _kbaQuestionResponseData
                                                .kbaQuestions
                                                .getKbaQuestionsResult
                                                .questions
                                                .kbaQuestionItem[index]
                                                .selectedAnswer !=
                                            null &&
                                        _kbaQuestionResponseData
                                                .kbaQuestions
                                                .getKbaQuestionsResult
                                                .questions
                                                .kbaQuestionItem[index]
                                                .selectedAnswer ==
                                            _kbaQuestionResponseData
                                                .kbaQuestions
                                                .getKbaQuestionsResult
                                                .questions
                                                .kbaQuestionItem[index]
                                                .answers
                                                .kbaAnswerOption[subIndex]
                                                .id
                                                .toString()
                                    ? Colors.white
                                    : Colors.black),
                          );
                        }),
                    SizedBox(
                      height: 20,
                    ),
                    /*  GestureDetector(
                      onTap: () {
                        setState(() {
                          isCheckedList[index] = true;
                        });
                      },
                      child: radioButtonitem(
                          text: "Yes",
                          bgColor: isCheckedList[index] != null && isCheckedList[index] ? '#252551' : '#FFF',
                          textColor: isCheckedList[index] != null && isCheckedList[index] ? Colors.white : Colors.black),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          isCheckedList[index] = false;
                        });
                      },
                      child: radioButtonitem(
                          text: "No",
                          bgColor: isCheckedList[index] != null && !isCheckedList[index] ? '#252551' : '#FFF',
                          textColor: isCheckedList[index] != null && !isCheckedList[index] ? Colors.white : Colors.black),
                    ),
                    SizedBox(
                      height: 40,
                    ),*/
                  ],
                );
              },
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
            flex: 1,
            child: Btn(
                txt: "Next",
                txtColor: Colors.white,
                bgColor: MyTheme.redColor,
                //width: getW(context),
                //height: 70,
                callback: () {
                  bool selectedIsNull = false;
                  _kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult
                      .questions.kbaQuestionItem
                      .forEach((element) {
                    if (element.selectedAnswer == null) {
                      selectedIsNull = true;
                      return;
                    }
                  });
                  if (selectedIsNull) {
                    showToast(
                        context: context, msg: "Answer All Question Please.");
                  } else {
                    List<QuestionsAnswerList> questionsAnswerList = [];

                    _kbaQuestionResponseData.kbaQuestions.getKbaQuestionsResult
                        .questions.kbaQuestionItem
                        .forEach((element) {
                      debugPrint(
                          "selected anser id = ${element.selectedAnswer}");
                      AnswersItem anserItem = AnswersItem(
                          kbaAnswerOption: _kbaQuestionResponseData.kbaQuestions
                              .getKbaQuestionsResult.questions.kbaQuestionItem);
                      QuestionsAnswerList questionsAnswer =
                          new QuestionsAnswerList(
                              answers: anserItem,
                              id: element.id,
                              text: element.text,
                              selectedAnswer: element.selectedAnswer);
                      questionsAnswerList.add(questionsAnswer);
                    });

                    AnswerParams answerParams = AnswerParams(
                        kbaAnswerItem: [],
                        questionsAnswerList: questionsAnswerList,
                        validationIdentifier: widget
                            .creditInfoPostResponse
                            .validateNewUser
                            .validateNewUserResult
                            .validationIdentifier,
                        userCompanyId: 1003,
                        userId: 121907);

                    debugPrint("selected anser id = ${answerParams.toJson()}");

                    Get.to(
                      CreditDashBoardTabController(
                        responseData: widget.creditInfoPostResponse,
                      ),
                    );
                  }
                }),
          )
        ],
      ),
    ));
  }

  radioButtonitem({String text, String bgColor, Color textColor}) {
    return Container(
      width: getW(context),
      padding: EdgeInsets.only(left: 10),
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border(
              left: BorderSide(color: Colors.grey, width: 1),
              right: BorderSide(color: Colors.grey, width: 1),
              top: BorderSide(color: Colors.grey, width: 1),
              bottom: BorderSide(color: Colors.grey, width: 1)),
          color: HexColor.fromHex(bgColor)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 25,
            height: 25,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.white),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    border: Border(
                        left: BorderSide(color: Colors.grey, width: 1),
                        right: BorderSide(color: Colors.grey, width: 1),
                        top: BorderSide(color: Colors.grey, width: 1),
                        bottom: BorderSide(color: Colors.grey, width: 1)),
                    color: Colors.grey),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(12),
              child: Txt(
                  txt: text,
                  txtColor: textColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ),
          ),
        ],
      ),
    );
  }
}
