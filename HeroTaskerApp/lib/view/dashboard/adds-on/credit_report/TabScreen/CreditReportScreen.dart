import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditDashBoardReport.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/creditReportScreenWidget/AddressLinks.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/creditReportScreenWidget/CIFASWidget.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/creditReportScreenWidget/CreditReportRating.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/creditReportScreenWidget/CreditReportSearchHistory.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/creditReportScreenWidget/FinancialAccountInformation.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/creditReportScreenWidget/MODAWidget.dart';
import 'package:aitl/view/dashboard/adds-on/credit_report/widget/creditReportScreenWidget/PersonalInfomationWidget.dart';
import 'package:flutter/material.dart';

import '../CreditDashboardTabController.dart';

class CreditReportScreen extends StatefulWidget {
  @override
  State<CreditReportScreen> createState() => _CreditReportScreenState();
}

class _CreditReportScreenState extends State<CreditReportScreen> with Mixin {
  CreditDashBoardReport creditDashBoardReport;

  @override
  Widget build(BuildContext context) {
    creditDashBoardReport =
        CreditDashBoardTabControllerState.creditDashBoardReport;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              CreditReportRating(
                  cRUserManage: creditDashBoardReport.cRUserManage),
              PersonalInformation(creditDashBoardReport.cRAccountHolderDetail),
              FinancialAccountInformation(),
              MODA(),
              CreditReportSearchHistory(),
              AddressLinks(creditDashBoardReport.cRAddressLinkDetailList),
              CIFAS(),
            ],
          ),
        ),
      ),
    );
  }
}
