import 'package:aitl/Mixin.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APICreditReportCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditDashBoardReport.dart';
import 'package:aitl/data/model/ads-on/credit_report/GetSimulatedScoreUseridAPIModel.dart';
import 'package:aitl/data/model/ads-on/credit_report/getSummaryResponse.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dialog/NamePrefixAlertDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/DecimalTextInputFormatter.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/generic/enum_gen.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';

enum eOtp {
  ADD_NEW_CC,
  AMEND_EXISTING_CC,
  ADD_NEW_LOAN,
  AMEND_EXISTING_LOAN,
  ADD_NEW_MORTGAGE,
  AMEND_ELECTORAL_REG_STATUS,
  ADD_MISSED_PAYMENT,
}

enum eAccountType {
  CreditCards,
  PersonalLoans,
  Mortgages,
  OtherAccounts,
}

abstract class ScoreSimulatorBase<T extends StatefulWidget> extends State<T>
    with Mixin {
  GetSimulatedScoreUseridAPIModel modelSimulated;

  final scrollController = ScrollController();
  var limitCC = new TextEditingController();
  var balanceCC = new TextEditingController();
  var limitCCamend = new TextEditingController();
  var balanceCCamend = new TextEditingController();
  var balanceLoan = new TextEditingController();
  var balanceLoanAmend = new TextEditingController();

  var balanceMortgage = new TextEditingController();

  final btnColor = Colors.lightBlueAccent;
  int rbAmendElectoralRS = -1;

  CreditDashBoardReport creditDashBoardReport;
  GetSummaryResponseData getSummaryResponse;

  DropListModel ddCC;
  OptionItem optCC = OptionItem(id: null, title: "Select Lender Name");

  DropListModel ddLoan;
  OptionItem optLoan = OptionItem(id: null, title: "Select Lender Name");

  DropListModel ddAll;
  OptionItem optAll = OptionItem(id: null, title: "Select Lender Name");

  DropListModel ddMissedPayment;
  OptionItem optMissedPayment =
      OptionItem(id: null, title: "Select Missed Payment");

  //  ****************************  Widget Start

  addCCLenderName4DD() {
    final List<OptionItem> list = [];
    for (var bnk in creditDashBoardReport.cRBankAccountList) {
      if (bnk.accountType == EnumGen.getEnum2Str(eAccountType.CreditCards)) {
        list.add(OptionItem(
            id: int.parse(bnk.accountId),
            title: bnk.lenderName + bnk.accountNumber.toString()));
      }
    }
    ddCC = DropListModel(list);
  }

  addLoan4DD() {
    final List<OptionItem> list = [];
    for (var bnk in creditDashBoardReport.cRBankAccountList) {
      if (bnk.accountType == EnumGen.getEnum2Str(eAccountType.PersonalLoans)) {
        list.add(OptionItem(
            id: int.parse(bnk.accountId),
            title: bnk.lenderName + bnk.accountNumber.toString()));
      }
    }
    ddLoan = DropListModel(list);
  }

  addAll4DD() {
    final List<OptionItem> list = [];
    for (var bnk in creditDashBoardReport.cRBankAccountList) {
      list.add(OptionItem(
          id: int.parse(bnk.accountId),
          title: bnk.lenderName + bnk.accountNumber.toString()));
    }
    ddAll = DropListModel(list);
  }

  CRBankAccountList getBankAccount(int accountId) {
    for (var bnk in creditDashBoardReport.cRBankAccountList) {
      if (int.parse(bnk.accountId) == accountId) return bnk;
    }
    return null;
  }

  addMissedPayment4DD() {
    final List<OptionItem> list = [];
    list.add(OptionItem(id: 1, title: "Select Missed Payment"));
    for (int i = 2; i <= 7; i++) {
      list.add(OptionItem(id: i, title: (i).toString()));
    }
    ddMissedPayment = DropListModel(list);
  }

  _getTextFieldVal(TextEditingController input) {
    if (input.text.trim() == "")
      return 0.0;
    else
      return int.parse(input.text.trim());
  }

  _drawBtnRun({Function callback, Color clr, String txt, bool isArrow = true}) {
    if (clr == null) {
      clr = btnColor;
    }
    if (txt == null) {
      txt = "Run";
    }

    var missingPayment = 0;
    if (optMissedPayment.title != "Select Missed Payment") {
      try {
        missingPayment = int.parse(optMissedPayment.title);
      } catch (e) {}
    }

    return Align(
      alignment: Alignment.center,
      child: Container(
        height: getHP(context, 6),
        width: getWP(context, 30),
        child: MaterialButton(
          color: clr,
          textColor: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              (isArrow)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                      ),
                    )
                  : SizedBox(),
              Flexible(
                child: Text(
                  txt,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          onPressed: () async {
            FocusScope.of(context).unfocus();
            var list = [];
            if (balanceCC.text.trim() != "") {
              list.add({
                "AccountId": 0,
                "Balance": _getTextFieldVal(balanceCC),
                "ElectoralRollState": "",
                "Limit": _getTextFieldVal(limitCC),
                "MemberPortId": 0,
                "NumberOfMissedPayments": 0,
                "ScenarioType": "addCard"
              });
            }
            if (balanceCCamend.text.trim() != "" && optCC.id != null) {
              final bankAccount = getBankAccount(optCC.id);
              list.add({
                "AccountId": bankAccount != null ? bankAccount.accountId : 0,
                "Balance": _getTextFieldVal(balanceCCamend),
                "ElectoralRollState": "",
                "Limit": _getTextFieldVal(limitCCamend),
                "MemberPortId": 0,
                "NumberOfMissedPayments": 0,
                "ScenarioType": "alterCard"
              });
            }
            if (balanceLoan.text.trim() != "") {
              list.add({
                "AccountId": 0,
                "Balance": _getTextFieldVal(balanceLoan),
                "ElectoralRollState": "",
                "Limit": 0,
                "MemberPortId": 0,
                "NumberOfMissedPayments": 0,
                "ScenarioType": "addLoan"
              });
            }
            if (balanceLoanAmend.text.trim() != "" && optLoan.id != null) {
              final bankAccount = getBankAccount(optLoan.id);
              list.add({
                "AccountId": bankAccount != null ? bankAccount.accountId : 0,
                "Balance": _getTextFieldVal(balanceLoanAmend),
                "ElectoralRollState": "",
                "Limit": 0,
                "MemberPortId": 0,
                "NumberOfMissedPayments": 0,
                "ScenarioType": "alterLoan"
              });
            }

            if (balanceMortgage.text.trim() != "") {
              list.add({
                "AccountId": 0,
                "Balance": _getTextFieldVal(balanceMortgage),
                "ElectoralRollState": "",
                "Limit": 0,
                "MemberPortId": 0,
                "NumberOfMissedPayments": 0,
                "ScenarioType": "addMortgage"
              });
            }

            if (rbAmendElectoralRS != -1) {
              list.add({
                "AccountId": 0,
                "Balance": 0,
                "ElectoralRollState": (rbAmendElectoralRS == 1)
                    ? "registerNewAddress"
                    : "notRegisterNewAddress",
                "Limit": 0,
                "MemberPortId": 0,
                "NumberOfMissedPayments": 0,
                "ScenarioType": "erollState"
              });
            }

            if (missingPayment > 0 && optAll.id != null) {
              final bankAccount = getBankAccount(optAll.id);
              list.add({
                "AccountId": bankAccount != null ? bankAccount.accountId : 0,
                "Balance": 0,
                "ElectoralRollState": "",
                "Limit": 0,
                "MemberPortId": 0,
                "NumberOfMissedPayments": missingPayment,
                "ScenarioType": "missedPayments"
              });
            }

            final param = {
              "UserId": userData.userModel.id,
              "UserIdentifier": "",
              "Scenarios": list,
            };
            APIViewModel().req<GetSimulatedScoreUseridAPIModel>(
                context: context,
                url: APICreditReportCfg.GET_SIMSCOREWITHUSERID,
                param: param,
                reqType: ReqType.Post,
                callback: (model) {
                  if (mounted && model != null) {
                    if (model.success) {
                      final simulatedScore = model.responseData.simulatedScore;
                      if (simulatedScore.status == "false") {
                        modelSimulated = null;
                        showToast(
                            context: context, msg: simulatedScore.message);
                        scroll2Top();
                        setState(() {});
                      } else {
                        modelSimulated = model;
                        callback();
                        scroll2Top();
                        setState(() {});
                      }
                    }
                  }
                });
          },
          splashColor: Colors.white,
        ),
      ),
    );
  }

  drawBtnReset(
      {Function callback, Color clr, String txt, bool isArrow = true}) {
    if (clr == null) {
      clr = btnColor;
    }
    if (txt == null) {
      txt = "Reset";
    }

    return Align(
      alignment: Alignment.center,
      child: Container(
        height: getHP(context, 6),
        width: getWP(context, 30),
        child: MaterialButton(
          color: clr,
          textColor: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              (isArrow)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: Icon(
                        Icons.arrow_forward,
                        color: Colors.white,
                      ),
                    )
                  : SizedBox(),
              Flexible(
                child: Text(
                  txt,
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          ),
          onPressed: () async {
            FocusScope.of(context).unfocus();
            limitCC.clear();
            balanceCC.clear();
            limitCCamend.clear();
            balanceCCamend.clear();
            balanceLoan.clear();
            balanceLoanAmend.clear();
            balanceMortgage.clear();
            optCC = OptionItem(id: null, title: "Select Lender Name");
            optLoan = OptionItem(id: null, title: "Select Lender Name");
            optAll = OptionItem(id: null, title: "Select Lender Name");
            optMissedPayment =
                OptionItem(id: null, title: "Select Missed Payment");
            modelSimulated = null;
            setState(() {});
          },
          splashColor: Colors.red,
        ),
      ),
    );
  }

  _getRichText(String txt1, String txt2, String txt3) {
    return RichText(
      text: new TextSpan(
        // Note: Styles for TextSpans must be explicitly defined.
        // Child text spans will inherit styles from parent
        style: new TextStyle(
          fontSize: 14.0,
          color: Colors.black,
        ),
        children: <TextSpan>[
          new TextSpan(text: txt1),
          new TextSpan(
              text: txt2, style: new TextStyle(fontWeight: FontWeight.bold)),
          new TextSpan(text: txt3),
        ],
      ),
    );
  }

  Widget _myRadioButton(
      {String txt1, String txt2, String txt3, int value, Function onChanged}) {
    return Theme(
      data: Theme.of(context).copyWith(
          selectedRowColor: Colors.grey,
          unselectedWidgetColor: Colors.grey,
          disabledColor: Colors.grey),
      child: RadioListTile(
        activeColor: btnColor,
        value: value,
        groupValue: rbAmendElectoralRS,
        onChanged: onChanged,
        title: _getRichText(txt1, txt2, txt3),
      ),
    );
  }

  scroll2Top() {
    scrollController.jumpTo(scrollController.position.minScrollExtent);
  }

  drawInputCurrencyBox(
      TextEditingController tf, double hintTxt, String labelTxt, int len) {
    return Container(
      //color: Colors.amber,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Txt(
              txt: labelTxt,
              txtColor: Colors.black87,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(height: 5),
          Container(
            child: Row(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        bottomLeft: Radius.circular(10)),
                    color: Colors.grey.shade400,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Txt(
                        txt: AppDefine.CUR_SIGN,
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize + .3,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ),
                Expanded(
                  child: TextField(
                    textInputAction: TextInputAction.next,
                    controller: tf,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2)
                    ],
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    maxLength: len,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                      height: MyTheme.txtLineSpace,
                    ),
                    decoration: new InputDecoration(
                      counterText: "",
                      hintText: hintTxt.toStringAsFixed(0),
                      hintStyle: new TextStyle(
                        color: Colors.grey,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                        height: MyTheme.txtLineSpace,
                      ),
                      contentPadding: EdgeInsets.only(left: 20, right: 20),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        borderSide: BorderSide(width: .5, color: Colors.black),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                            topRight: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        borderSide: BorderSide(width: .5, color: Colors.grey),
                      ),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              bottomRight: Radius.circular(10)),
                          borderSide: BorderSide(
                            width: .5,
                          )),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawDropDown(String title, DropListModel ddList, OptionItem opt,
      Function(OptionItem) callback) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 5),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border(
                    left: BorderSide(color: Colors.grey, width: 1),
                    right: BorderSide(color: Colors.grey, width: 1),
                    top: BorderSide(color: Colors.grey, width: 1),
                    bottom: BorderSide(color: Colors.grey, width: 1)),
                color: Colors.transparent),
            child: GestureDetector(
              onTap: () {
                showNamePrefixAlertDialog(
                    ddTitleList: ddList,
                    callback: (optionItem) {
                      Navigator.of(context, rootNavigator: true).pop();
                      callback(optionItem);
                    },
                    title: title,
                    context: context);
                // showListAlertDialog(ddTitleList: editProfileHelper.ddTitle, callback: () {});
              },
              child: new Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Txt(
                          txt: "${opt.title}",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                  ),
                  Align(
                    alignment: Alignment(1, 0),
                    child: Icon(
                      Icons.keyboard_arrow_down,
                      color: Colors.black,
                      size: 40,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  //  ****************************  Widget End

  //  Add New Credit Card
  drawAddNewCreditCard() {
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(child: drawInputCurrencyBox(limitCC, 8900, "Limit", 8)),
              SizedBox(width: 10),
              Flexible(child: drawInputCurrencyBox(balanceCC, 0, "Balance", 8)),
            ],
          ),
          SizedBox(height: 20),
          _drawBtnRun(callback: () {}),
        ],
      ),
    );
  }

  //  Amend Existing Credit Card
  drawAmendCreditCard() {
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          drawDropDown("Choose Lender Name", ddCC, optCC, (opt) {
            optCC = opt;
            setState(() {});
          }),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                  child: drawInputCurrencyBox(limitCCamend, 0, "Limit", 8)),
              SizedBox(width: 10),
              Flexible(
                  child: drawInputCurrencyBox(balanceCCamend, 0, "Balance", 8)),
            ],
          ),
          SizedBox(height: 20),
          _drawBtnRun(callback: () {})
        ],
      ),
    );
  }

  //  Add New Loan
  drawAddNewLoan() {
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          drawInputCurrencyBox(balanceLoan, 0, "Balance", 8),
          SizedBox(height: 20),
          _drawBtnRun(callback: () {})
        ],
      ),
    );
  }

  //  Amend Existing Loan
  drawAmendLoan() {
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          drawDropDown("Choose Lender Name", ddLoan, optLoan, (opt) {
            optLoan = opt;
            setState(() {});
          }),
          drawInputCurrencyBox(balanceLoanAmend, 0, "Balance", 8),
          SizedBox(height: 20),
          _drawBtnRun(callback: () {})
        ],
      ),
    );
  }

  //  Add New Mortgage
  drawAddNewMortgage() {
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          drawInputCurrencyBox(balanceMortgage, 0, "Balance", 8),
          SizedBox(height: 20),
          _drawBtnRun(callback: () {})
        ],
      ),
    );
  }

  //  Amend Electoral Register Status
  drawAmendElectoralRegStatus() {
    return Container(
      child: Column(
        children: [
          _myRadioButton(
            txt1: "I am moving and ",
            txt2: "will",
            txt3: " be on the Electoral Register at my new address",
            value: 1,
            onChanged: (newValue) =>
                setState(() => rbAmendElectoralRS = newValue),
          ),
          _myRadioButton(
            txt1: "I am moving and ",
            txt2: "will not",
            txt3: " be on the Electoral Register at my new address",
            value: 0,
            onChanged: (newValue) =>
                setState(() => rbAmendElectoralRS = newValue),
          ),
          _drawBtnRun(callback: () {}),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  //  Add Missed Payment
  drawAddMissedPayment() {
    return Container(
      padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          drawDropDown("Choose Lender Name", ddAll, optAll, (opt) {
            optAll = opt;
            setState(() {});
          }),
          drawDropDown(
              "Select Missed Payment", ddMissedPayment, optMissedPayment,
              (opt) {
            optMissedPayment = opt;
            setState(() {});
          }),
          SizedBox(height: 10),
          _drawBtnRun(callback: () {})
        ],
      ),
    );
  }

  drawAbout() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Txt(
                  txt: "About the Credit Score Simulator",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
              UIHelper().drawLine(),
              SizedBox(height: 20),
              Txt(
                  txt:
                      "The Score Simulator is an educational tool. Select, adjust and ponder, but just remember these are theoretical outcomes and not predictions.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 30),
              Txt(
                  txt: "How does the Score Simulator work?",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(height: 10),
              Txt(
                  txt:
                      "The Score Simulator starts with the information in your current TransUnion credit report and explores how changing that information could affect your score. Of course, it’s all hypothetical. Simulating these changes won't actually affect your score or report.",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
