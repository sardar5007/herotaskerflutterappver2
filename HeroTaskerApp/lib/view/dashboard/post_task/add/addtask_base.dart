import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/post_task/add/task_attach_page.dart';
import 'package:aitl/view/widgets/txt/AppbarText..dart';
import 'package:aitl/view/widgets/stepbar/StepBar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/misc/FcmDeviceInfoHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';

abstract class BaseAddTaskStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final taskController = Get.put(TaskController());

  BaseAddTaskStatefull() {
    // Parent constructor
  }

  //  abstract methods
  onNextClicked();
  onDelTaskClicked();
  uploadTaskImages(int pageNo);

  drawAppbar({
    @required int pageNo,
    @required userModel,
    @required title,
    @required int pos,
    @required isBold1,
    @required isBold2,
    @required isBold3,
  }) {
    final totalDots = getW(context) / 20;
    return AppBar(
      elevation: MyTheme.appbarElevation,
      backgroundColor: MyTheme.bgColor,
      iconTheme: MyTheme.themeData.iconTheme,
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () async {
          if (pageNo == 1) {
            await uploadTaskImages(pageNo);
          }
          Get.back(result: pageNo);
        },
      ),
      title: UIHelper().drawAppbarTitle(title: 'Post Task'),
      centerTitle: false,
      actions: [
        taskController.getTaskModel().id != null
            ? IconButton(
                icon: Icon(Icons.attach_file),
                onPressed: () {
                  Get.to(() => TaskAttachmentScreen(
                        userModel: userModel,
                      ));
                },
              )
            : SizedBox(),
        SizedBox(width: 10),
        taskController.getTaskModel().id != null
            ? IconButton(
                icon: Icon(Icons.delete_forever_outlined),
                onPressed: () {
                  showConfirmDialog(
                    context: context,
                    title: null,
                    msg: "Are you sure, you want to delete this task?",
                    callback: () {
                      onDelTaskClicked();
                    },
                  );
                },
              )
            : SizedBox(),
        SizedBox(width: 20),
        drawAppbarText(
          txt: "NEXT",
          callback: () {
            onNextClicked();
          },
        ),
        SizedBox(width: 20),
      ],
      bottom: PreferredSize(
          child: Container(
            color: Colors.white,
            height: getHP(context, 10),
            width: getW(context),
            child: ListView(
              scrollDirection: Axis.horizontal,
              primary: false,
              padding: const EdgeInsets.only(left: 20, right: 20),
              children: [
                drawStepBar(
                  context: context,
                  isSelected: (pos == 0) ? true : false,
                  pos: 1,
                  txt: "DETAILS",
                  isBold: isBold1,
                  dotsLen: 0,
                ),
                drawStepBar(
                  context: context,
                  isSelected: (pos == 1) ? true : false,
                  pos: 2,
                  txt: "DUE DATE",
                  isDots: true,
                  isBold: isBold2,
                  dotsLen: totalDots,
                ),
                drawStepBar(
                  context: context,
                  isSelected: (pos == 2) ? true : false,
                  pos: 3,
                  txt: "BUDGET",
                  isDots: true,
                  isBold: isBold3,
                  dotsLen: totalDots,
                ),
              ],
            ),
          ),
          preferredSize: Size.fromHeight(getHP(context, 10))),
    );
  }
}
