import 'dart:io';

import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIYoutubeCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/ImageLib.dart';
import 'package:aitl/data/app_data/ScanDocData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/ads-on/action_req/DashBoardListType.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/selfie_page.dart';
import 'package:aitl/view/dashboard/adds-on/eid/doc_scan/scan/selfie/uploading_page.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/dashboard/post_task/all_cat/all_cat_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/btn/MMBtnLeftIconDashBoard.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/grid/SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight.dart';
import 'package:aitl/view_model/helper/action_req/action_req_helper.dart';
import 'package:aitl/view_model/helper/utils/PostTaskHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/ActionReqController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:launch_review/launch_review.dart';
import 'package:external_app_launcher/external_app_launcher.dart';

class PostTaskListPage extends StatefulWidget {
  @override
  State createState() => _PostTaskListPageState();
}

class _PostTaskListPageState extends State<PostTaskListPage>
    with Mixin, UIHelper, StateListener {
  final actionReqControler = Get.put(ActionReqController());

  dynamic widActionRequired;

  bool isLoading = true;

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB ||
          state == ObserverState.STATE_RELOAD_TAB_NEW_TASK) {
        refreshData();
      }
    } catch (e) {}
  }

  Future<void> refreshData() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        await ActionReqHelper().wsActionReqAPIcall(
          context: context,
          callback: (List<DashBoardListType> listUserList) {
            if (mounted && listUserList != null) {
              if (listUserList.length > 0) {
                widActionRequired = ActionReqHelper().drawActionRequiredItems(
                  context: context,
                  getColor: getColor,
                  listUserList: listUserList,
                  callbackReload: () {
                    refreshData();
                  },
                  callback: (Map<String, dynamic> map) {
                    if (map["email"] == true) {
                      final isOk = map["email"];
                      if (isOk) {
                        showToast(
                            context: context,
                            msg:
                                "We have sent an email for verification, please check your email",
                            which: 1);
                      } else {
                        showToast(
                            context: context,
                            msg: "Sorry, something went wrong",
                            which: 0);
                      }
                    } else if (map["eid"] == true) {
                      Get.to(() => SelfiePage()).then((value) {
                        scanDocData.file_selfie = null;
                        scanDocData.file_drvlic_front = null;
                        scanDocData.file_pp_front = null;
                        refreshData();
                      });
                    } else {
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    }
                  },
                );
              }
            }
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          },
        );
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: UIHelper().drawAppbarTitle(title: 'Post a Task'),
          centerTitle: false,
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.help),
                onPressed: () {
                  openUrl(context, APIYoutubeCfg.HELP_YOUTUBE_URL);
                }),
          ],
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(AppConfig.post_add_height),
            child: Container(
              color: MyTheme.appbarColor,
              width: getW(context),
              child: Padding(
                padding: const EdgeInsets.only(top: 15, bottom: 15),
                child: Txt(
                    txt: "Select the type of job you need done from below",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isOverflow: true,
                    isBold: false),
              ),
            ),
          ),
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            (widActionRequired != null) ? widActionRequired : SizedBox(),
            drawCreditReport(),
            drawTaskList(),
            Padding(
                padding: const EdgeInsets.only(top: 20, bottom: 30),
                child: drawHireWorkButtons()),
          ],
        ),
      ),
    );
  }

  drawTaskList() {
    final w = getW(context) / 4;
    return GridView.builder(
      shrinkWrap: true,
      primary: false,
      //padding: const EdgeInsets.only(top: 40, bottom: 40),
      scrollDirection: Axis.vertical,
      itemCount: PostTaskHelper.listTask.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
        crossAxisCount: 3,
        crossAxisSpacing: 0,
        mainAxisSpacing: 0,
        height: getHP(context, 22),
      ), //, childAspectRatio: getHP(context, .12)),
      itemBuilder: (context, index) {
        final map = PostTaskHelper.listTask[index];
        final icon = map["icon"];
        String title = map["title"];
        final isCat = map['isCat'];

        return GestureDetector(
          onTap: () async {
            if (!isCat) {
              final playstore_appId = map['playstore_appId'].toString();
              final appstore_appId = map['appstore_appId'].toString();
              if (playstore_appId.isNotEmpty || appstore_appId.isNotEmpty) {
                LaunchReview.launch(
                    androidAppId: playstore_appId, iOSAppId: appstore_appId);
              } else {
                Get.to(
                  () => AddTask1Screen(
                    index: index,
                    userModel: userData.userModel,
                  ),
                ).then((pageNo) {
                  obsUpdateTabs(pageNo);
                });
              }
            } else {
              Get.to(
                () => AllCatPage(),
              ).then((value) {
                if (value != null) {
                  Get.to(
                    () => AddTask1Screen(
                      index: null,
                      userModel: userData.userModel,
                      categoryPH: value,
                    ),
                  ).then((pageNo) {
                    obsUpdateTabs(pageNo);
                  });
                }
              });
            }
          },
          child: Container(
            //elevation: 0,
            color: MyTheme.bgColor,
            margin: new EdgeInsets.all(10),
            //color: Colors.black,
            //width: w,
            // height: w + 50,
            child: new Column(
              //mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: w - 15,
                  height: w - 15,
                  //color: Colors.black,
                  child: SvgPicture.asset(
                    icon,
                    //width: w,
                    //height: w,
                    //fit: BoxFit.cover,
                  ),
                ),
                SizedBox(height: 5),
                Expanded(
                  flex: 2,
                  child: Text(
                    title,
                    textAlign: TextAlign.center,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: MyTheme.gray5Color, //MyTheme.iconTextColor,
                      fontSize: (title.length > 25) ? 11 : 14,
                    ),
                  ),
                ),
                //just for testing, will fill with image later
              ],
            ),
          ),
        );
      },
    );
  }

  drawHireWorkButtons() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Flexible(
            child: MMBtn(
              txt: "I want to work",
              //txtColor: Colors.white,
              //bgColor: accentColor,
              width: getWP(context, 48),
              height: getHP(context, MyTheme.btnHpa + 1),
              radius: 20,
              callback: () {
                if (userData.communityId != 2) {
                  //Get.to(() => OffersCommentsPage(taskBiddingsModel: null));
                  StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
                      DashboardPage.TAB_FINDWORKS);
                } else {
                  StateProvider().notify(ObserverState.STATE_RELOAD_TAB,
                      DashboardPage.TAB_FINDWORKS);
                }
              },
            ),
          ),
          //SizedBox(width: 10),
          Flexible(
            child: MMBtn(
              txt: "I want to hire",
              //txtColor: Colors.white,
              //bgColor: accentColor,
              width: getWP(context, 48),
              height: getHP(context, MyTheme.btnHpa + 1),
              radius: 20,
              callback: () {
                Get.to(() => AddTask1Screen(
                      userModel: userData.userModel,
                      categoryPH: 'E.g. Suggest a name for my new company etc.',
                    ));
              },
            ),
          ),
        ],
      ),
    );
  }

  drawCreditReport() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        //width: getW(context),
        child: MMBtnLeftIconDashBoard(
          imageFile: 'assets/images/adds-on/my_credit_icon.png',
          bgColor: HexColor.fromHex("#E4E6EB"),
          txt: "My credit report",
          height: getHP(context, 6),
          width: null, //getW(context),
          radius: 0,
          callback: () async {
            if (!Server.isOtp) {
              scanDocData.file_selfie = await ImageLib()
                  .getImageFileFromAssets(path: "eid_scan", img: "dada.png");
              Get.to(() => UploadingPage());
            } else {
              final androidAppId = "uk.co.mortgagemagic.client";
              final appstoreAppId = "1574687745";
              final appstoreAppName =
                  "mortgagemagic.clients"; //  Mortgage Magic
              if (Platform.isAndroid) {
                await LaunchApp.openApp(
                  androidPackageName: androidAppId,
                  iosUrlScheme: appstoreAppName + "://",
                  appStoreLink:
                      'itms-apps://itunes.apple.com/us/app/$appstoreAppName/id' +
                          appstoreAppId,
                );
              } else if (Platform.isIOS) {
                final platform =
                    const MethodChannel('flutter.native/herotasker');
                final param = {
                  'appstoreAppId': appstoreAppId,
                  'appstoreAppName': appstoreAppName,
                  'appName': 'Mortgage Magic'
                };
                await platform.invokeMethod('openMM3App', param);
              }
            }
          },
        ),
      ),
    );
  }

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return MyTheme.redColor;
  }
}
