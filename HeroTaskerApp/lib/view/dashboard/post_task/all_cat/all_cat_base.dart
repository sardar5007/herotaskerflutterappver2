import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';

abstract class BaseAllCatStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  BaseAllCatStatefull() {
    // Parent constructor
  }

  drawLeftListView() {}

  drawRightListView() {}
}
