import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/stripe/StripeMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:stripe_payment/stripe_payment.dart';
import 'fund_payment_base.dart';

class FundPaymentPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  final bool isReleasePayment;
  const FundPaymentPage(
      {Key key, this.taskBidding, this.isReleasePayment = false})
      : super(key: key);
  @override
  State createState() => _FundPaymentPageState();
}

class _FundPaymentPageState extends BasePaymentStatefull<FundPaymentPage>
    with Mixin, UIHelper, APIStateListener {
  int switchIndex = 0;
  double price = 0;

  DropListModel ddTitle = DropListModel([
    OptionItem(id: 1, title: "Bkash Mobile Banking"),
    OptionItem(id: 2, title: "DBBL Mobile Banking"),
    OptionItem(id: 3, title: "BRAC VISA"),
    OptionItem(id: 4, title: "Dutch Bangla VISA"),
    OptionItem(id: 5, title: "City Bank Visa"),
    OptionItem(id: 6, title: "EBL Visa"),
    OptionItem(id: 7, title: "Southeast Bank Visa"),
    OptionItem(id: 8, title: "BRAC MASTER"),
    OptionItem(id: 9, title: "MASTER Dutch-Bangla"),
    OptionItem(id: 10, title: "City Master Card"),
    OptionItem(id: 11, title: "EBL Master Card"),
    OptionItem(id: 12, title: "Southeast Bank Master Card"),
    OptionItem(id: 13, title: "City Bank AMEX"),
    OptionItem(id: 14, title: "QCash"),
    OptionItem(id: 15, title: "DBBL Nexus"),
    OptionItem(id: 16, title: "Bank Asia IB"),
    OptionItem(id: 17, title: "AB Bank IB"),
    OptionItem(id: 18, title: "IBBL IB and Mobile Banking"),
    OptionItem(id: 19, title: "Mutual Trust Bank IB"),
    OptionItem(id: 20, title: "City Touch IB"),
  ]);

  OptionItem optTitle = OptionItem(id: null, title: "Select Payment Type");

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.stripe_payment_intent_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      }
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() {
    try {
      StripeMgr().initStripe();
    } catch (e) {}
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (widget.isReleasePayment) {
        /*await APIViewModel().req<GetTaskPaymentByTaskIdTaskBiddingIdAPIModel>(
            context: context,
            url: APIPaymentCfg
                .TASK_PAYMENTS_GET_TASK_PAYMENT_TASKID_TASKBIDDINGID_URL,
            isLoading: true,
            reqType: ReqType.Post,
            param: {
              "TaskId": myTaskController.getTaskModel().id,
            },
            callback: (model) {
              if (model != null && mounted) {
                if (model.success) {}
              }
            });*/
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(
              title: (widget.isReleasePayment)
                  ? "Release payment"
                  : "Offer accept"),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: (widget.isReleasePayment)
                ? "Release payment"
                : "Accept & add funds",
            callback: () async {
              StripeMgr().makePayment(
                  context: context,
                  description: "Task price",
                  amount: price,
                  callback: (PaymentMethod paymentMethod,
                      PaymentIntentResult paymentIntent,
                      PaymentIntentResult authPaymentIntent) {
                    //showAlert(context: context,
                    //msg: "Payment has been placed successfully.",
                    //isToast: true,
                    //which: 1);
                    showToast(
                      context: context,
                      msg: 'Payment accepted.',
                      which: 1,
                    );
                    /*await APIViewModel().req<PaymentIntentAPIModel>(
                      context: context,
                      apiState: APIState(APIType.stripe_payment_intent_post,
                          this.runtimeType, null),
                      url: APIPaymentCfg.STRIPE_PAYMENT_INTENT_POST_URL
                          .replaceAll(
                              "#taskId#", widget.taskBidding.taskId.toString()),
                      param: {
                        'paymentMethodId': paymentMethod.id,
                        'paymentIntentId': authPaymentIntent.paymentIntentId,
                        'status': authPaymentIntent.status,
                      },
                      reqType: ReqType.Get,
                    );*/
                    Get.back();
                  });
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          payOffer(),
        ],
      ),
    );
  }

  payOffer() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: MyTheme.picEmboseCircleDeco,
            alignment: Alignment.center,
            child: CircleAvatar(
              radius: 30,
              backgroundColor: Colors.transparent,
              backgroundImage: new CachedNetworkImageProvider(
                  MyNetworkImage.checkUrl(widget.taskBidding.ownerImageUrl)),
            ),
          ),
          SizedBox(height: 10),
          Center(
            child: Txt(
                txt: widget.taskBidding.ownerName,
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
          SizedBox(height: 20),
          Txt(
              txt: widget.taskBidding.coverLetter,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(height: 20),
          drawLine(colr: MyTheme.gray3Color),
          SizedBox(height: 10),
          drawPriceBox(),
          //SizedBox(height: 20),
          //drawPayBox(),
          (widget.isReleasePayment) ? drawReleaseFundBox() : SizedBox(),
        ],
      ),
    );
  }

  drawPriceBox() {
    price = widget.taskBidding.netTotalAmount;
    if (price < 1) {
      price = widget.taskBidding.fixedbiddigAmount;
    }
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Txt(
                txt: "Task price",
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          (widget.isReleasePayment)
              ? Flexible(
                  child: Row(
                    children: [
                      Icon(Icons.lock, size: 14, color: Colors.black),
                      SizedBox(width: 5),
                      Flexible(
                        child: Txt(
                            txt: "PAYMENT SECURED",
                            txtColor: MyTheme.airGreenColor,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      )
                    ],
                  ),
                )
              : SizedBox(),
          Flexible(
            child: Text(
              getCurSign() + price.toStringAsFixed(2),
              style: TextStyle(
                color: MyTheme.gray5Color,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawBankCard() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DropDownListDialog(
            context: context,
            title: optTitle.title,
            ddTitleList: ddTitle,
            callback: (optionItem) {
              optTitle = optionItem;
              setState(() {});
            },
          ),
          /*DropDownPicker(
            cap: null,
            itemSelected: optTitle,
            dropListModel: ddTitle,
            onOptionSelected: (optionItem) {
              optTitle = optionItem;
              setState(() {});
            },
          ),*/
          SizedBox(height: 20),
          Txt(
              txt:
                  "The funds for the task have now been secured. You're in control to release the funds once the task is completed. No further payment for the task is required.",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawCash() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Txt(
                txt: "Your payment will be paid cash directly to " +
                    widget.taskBidding.ownerName +
                    ". who will confirm the received payment",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }

  drawReleaseFundBox() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Txt(
          txt:
              "Please note that by releasing payment you're agreeing to our payment terms and conditions.",
          txtColor: MyTheme.gray4Color,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.center,
          isBold: false),
    );
  }
}
