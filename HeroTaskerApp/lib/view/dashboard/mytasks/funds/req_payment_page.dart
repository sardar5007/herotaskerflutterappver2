import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'fund_payment_base.dart';

class ReqPaymentPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const ReqPaymentPage({Key key, this.taskBidding}) : super(key: key);
  @override
  State createState() => _ReqPaymentPageState();
}

class _ReqPaymentPageState extends BasePaymentStatefull<ReqPaymentPage>
    with APIStateListener {
  final cmt = TextEditingController();
  String msg = "";

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.post_timeline &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final timelineId =
                (model as TimelinePostAPIModel).responseData.post.id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TIMELINE_EMAI_NOTI_GET_URL
                  .replaceAll("#timelineId#", timelineId.toString()),
              isLoading: true,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: true);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    cmt.dispose();
    super.dispose();
  }

  wsTimelinePost() async {
    try {
      await APIViewModel().req<TimelinePostAPIModel>(
        context: context,
        apiState: APIState(APIType.post_timeline, this.runtimeType, null),
        url: APITimelineCfg.TIMELINE_POST_URL,
        isLoading: true,
        reqType: ReqType.Post,
        param: {
          "AdditionalAttributeValue": '',
          "Checkin": '',
          "FromLat": myTaskController.getTaskModel().latitude,
          "FromLng": myTaskController.getTaskModel().longitude,
          "IsPrivate": true,
          "Message": msg,
          "OwnerId": userData.userModel.id,
          "PostTypeName": "status", //(url == null) ? "status" : "picture",
          "TaskId": myTaskController.getTaskModel().id,
        },
      );
    } catch (e) {}
  }

  appInit() {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: "Request payment"),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Request",
            callback: () async {
              wsTimelinePost();
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: MyTheme.gray1Color,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Txt(
                        txt:
                            "Please make sure you have completed your task properly before requesting payment. Otherwise it may bother your client.",
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 20),
                    Container(
                      //height: getHP(context, MyTheme.btnHpa),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border(
                              left: BorderSide(color: Colors.grey, width: 1),
                              right: BorderSide(color: Colors.grey, width: 1),
                              top: BorderSide(color: Colors.grey, width: 1),
                              bottom: BorderSide(color: Colors.grey, width: 1)),
                          color: Colors.white),
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10, right: 0),
                        child: ListTile(
                          dense: true,
                          contentPadding:
                              EdgeInsets.only(left: 0.0, right: 0.0),
                          leading: Txt(
                            txt: getCurSign(),
                            txtColor: MyTheme.gray5Color,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false,
                          ),
                          minLeadingWidth: 0,
                          title: Txt(
                            txt: widget.taskBidding.netTotalAmount
                                .round()
                                .toString(),
                            txtColor: MyTheme.gray5Color,
                            txtSize: MyTheme.txtSize + .5,
                            txtAlign: TextAlign.center,
                            isBold: false,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            Txt(
              txt: "Message for Payment",
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: Container(
                //padding: const EdgeInsets.only(bottom: 20),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: TextFormField(
                    controller: cmt,
                    minLines: 5,
                    maxLines: 10,
                    autocorrect: false,
                    onChanged: (v) {
                      try {
                        if (widget.taskBidding.paymentStatus == "" ||
                            widget.taskBidding.paymentStatus ==
                                TaskStatusCfg.STATUS_PAYMENT_METHOD_FUNDED ||
                            widget.taskBidding.paymentStatus ==
                                TaskStatusCfg
                                    .STATUS_PAYMENT_METHOD_FUNDED_REQUESTPAYMENT) {
                          msg =
                              "You are requested to Release Payment for the task since I have completed your task :" +
                                  " " +
                                  cmt.text.trim();
                        } else if (widget.taskBidding.paymentStatus ==
                            TaskStatusCfg
                                .STATUS_PAYMENT_METHOD_FUNDED_RELEASEPAYMENT) {
                        } else {
                          msg =
                              " You are requested to Make Payment for the task since I have completed your task :" +
                                  " " +
                                  cmt.text.trim();
                        }
                      } catch (e) {}
                    },
                    keyboardType: TextInputType.multiline,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                    ),
                    decoration: new InputDecoration(
                      counter: Offstage(),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      hintText: "",
                      hintStyle: new TextStyle(
                        color: Colors.grey,
                        fontSize: getTxtSize(
                            context: context, txtSize: MyTheme.txtSize),
                      ),
                      contentPadding: const EdgeInsets.symmetric(vertical: 0),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
