import 'dart:io';
import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/messages/chat_page.dart';
import 'package:aitl/view/dashboard/messages/comments/offers_comments_page.dart';
import 'package:aitl/view/dashboard/more/profile/profile_page.dart';
import 'package:aitl/view/dashboard/more/resolutions/res_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/fund_payment_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/rec_payment_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/req_payment_page.dart';
import 'package:aitl/view/dashboard/mytasks/funds/review_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/accept_offer_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/confirm_offer_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/update_offer_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/view_offer_page.dart';
import 'package:aitl/view/dashboard/post_task/add/addtask_page1.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/mapview/TaskLocationMap.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/generic/enum_gen.dart';
import 'package:aitl/view_model/helper/utils/ProfileHelper.dart';
import 'package:aitl/view_model/helper/utils/TaskDetailsHelper.dart';
import 'package:aitl/view_model/helper/utils/TimeLineHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:aitl/view_model/rx/TaskDetailsController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

enum enumMenu {
  Edit,
  Share_task,
  Copy,
  Report,
  Cancel_task,
}

abstract class BaseTaskDetailsStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> with TaskDetailsHelper, TimeLineHelper {
  final myTaskController = Get.put(MyTaskController());
  final taskDetController = Get.put(TaskDetailsController());
  bool isPoster = true;
  bool isLoading = false;
  bool isPaymentDue = false;
  ValueNotifier<int> indexTopBtn;

  var listTopBtn = [
    "Active",
    "Assigned",
    "Paid",
  ];

  getRefreshData();
  wsGetTimelineByApp();
  wsWithdrawTaskbidding();
  onDelTaskClicked();

  getPopupMenuButton() {
    if (!isPoster &&
        myTaskController.getStatusCode() != TaskStatusCfg.TASK_STATUS_ACTIVE) {
      return PopupMenuButton<String>(
        offset: Offset(-10.0, kToolbarHeight),
        onSelected: handleMenuClick,
        color: Colors.white,
        itemBuilder: (BuildContext context) {
          return {EnumGen.getEnum2Str(enumMenu.Copy)}.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(
                choice,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            );
          }).toList();
        },
      );
    } else if (!isPoster) {
      return PopupMenuButton<String>(
        offset: Offset(-10.0, kToolbarHeight),
        onSelected: handleMenuClick,
        color: Colors.white,
        itemBuilder: (BuildContext context) {
          return {
            EnumGen.getEnum2Str(enumMenu.Edit),
            EnumGen.getEnum2Str(enumMenu.Cancel_task),
            EnumGen.getEnum2Str(enumMenu.Copy),
          }.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(
                choice,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            );
          }).toList();
        },
      );
    } else {
      return PopupMenuButton<String>(
        offset: Offset(-10.0, kToolbarHeight),
        onSelected: handleMenuClick,
        color: Colors.white,
        itemBuilder: (BuildContext context) {
          return {
            EnumGen.getEnum2Str(enumMenu.Copy),
            EnumGen.getEnum2Str(enumMenu.Report),
          }.map((String choice) {
            return PopupMenuItem<String>(
              value: choice,
              child: Text(
                choice,
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            );
          }).toList();
        },
      );
    }
  }

  void handleMenuClick(String str) {
    str = str.replaceAll(" ", "_");
    enumMenu f =
        enumMenu.values.firstWhere((e) => e.toString().split('.').last == str);
    switch (f) {
      case enumMenu.Edit:
        log("edit");
        onEditTask(false);
        break;
      case enumMenu.Cancel_task:
        log("cancel task");
        showConfirmDialog(
          context: context,
          title: myTaskController.getTaskModel().title,
          msg: "Are you sure you want to cancel your task?",
          callback: () {
            onDelTaskClicked();
          },
        );
        break;
      case enumMenu.Copy:
        log("copy");
        onEditTask(true);
        break;
      case enumMenu.Report:
        log("report");
        Get.to(
          () => ResPage(
            from: ResCfg.RESOULUTION_TYPE_TASK_SPAMREPORT,
            userId: userData.userModel.id,
          ),
        );
        break;
      case enumMenu.Share_task:
        log("share_task");
        break;
      default:
        break;
    }
  }

  onEditTask(bool isCopyTask) {
    Get.to(
      () => AddTask1Screen(
        index: null,
        userModel: userData.userModel,
        taskModel: myTaskController.getTaskModel(),
        isCopyTask: isCopyTask,
      ),
    ).then(
      (pageNo) {
        //getRefreshData();
        //obsUpdateTabs(pageNo);
        if (pageNo != null) {
          if (pageNo == 4)
            Get.back();
          else {
            getRefreshData();
            obsUpdateTabs(pageNo);
          }
        } else {
          getRefreshData();
          obsUpdateTabs(pageNo);
        }
      },
    );
  }

  onReviewClicked(TaskBiddingModel taskBidding) {
    Get.to(() => ReviewPage(taskBidding: taskBidding)).then(
      (value) {
        if (value != null) getRefreshData();
      },
    );
  }

  onReleasePayment(TaskBiddingModel taskBidding) {
    Get.to(() =>
        FundPaymentPage(taskBidding: taskBidding, isReleasePayment: true)).then(
      (value) {
        if (value != null) getRefreshData();
      },
    );
  }

  drawTopBtn(int index) {
    final w = (getWP(context, 100) / 3) - 20;
    return ValueListenableBuilder(
      valueListenable: indexTopBtn,
      builder: (context, value, child) => Container(
        width: w,
        child: Btn(
            txt: listTopBtn[index],
            txtColor: (value == index)
                ? MyTheme.greenAlertWidgetText
                : MyTheme.gray3Color,
            bgColor: (value == index)
                ? MyTheme.greenAlertWidgetBackground
                : Colors.white,
            radius: 20,
            callback: () {
              indexTopBtn.value = index;
            }),
      ),
    );
  }

  drawTopStatus() {
    var stateSelected = 0;
    try {
      switch (myTaskController.getStatusCode()) {
        case TaskStatusCfg.TASK_STATUS_ACTIVE:
          stateSelected = 0;
          break;
        case TaskStatusCfg.TASK_STATUS_ACCEPTED:
          stateSelected = 1;
          break;
        case TaskStatusCfg.TASK_STATUS_PAYMENTED:
          stateSelected = 2;
          break;
        case TaskStatusCfg.TASK_STATUS_CANCELLED:
          stateSelected = 0;
          listTopBtn = ["Cancelled", "", ""];
          break;
        default:
      }
    } catch (e) {}

    return (stateSelected == 2)
        ? Container(
            decoration: BoxDecoration(
              color: MyTheme.greenAlertWidgetBackground,
              borderRadius: BorderRadius.circular(20),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                for (String statusStr in listTopBtn)
                  Container(
                    width: getW(context) / (listTopBtn.length + 1),
                    child: Txt(
                        txt: statusStr,
                        txtColor: MyTheme.greenAlertWidgetText,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
              ],
            ),
          )
        : (stateSelected == 1)
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    decoration: BoxDecoration(
                      color: MyTheme.greenAlertWidgetBackground,
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (int i = 0; i < listTopBtn.length - 1; i++)
                          Container(
                            width: getW(context) / (listTopBtn.length + 1),
                            child: Txt(
                                txt: listTopBtn[i],
                                txtColor: MyTheme.greenAlertWidgetText,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                      ],
                    ),
                  ),
                  Txt(
                      txt: listTopBtn[listTopBtn.length - 1],
                      txtColor: MyTheme.gray3Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.center,
                      isBold: false)
                ],
              )
            : Padding(
                padding: EdgeInsets.only(
                    left: (listTopBtn.length == 1) ? 40 : 20,
                    right: (listTopBtn.length == 1) ? 40 : 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyTheme.greenAlertWidgetBackground,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: Txt(
                            txt: listTopBtn[0],
                            txtColor: MyTheme.greenAlertWidgetText,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        for (int i = 1; i < listTopBtn.length; i++)
                          Container(
                            width: getW(context) / (listTopBtn.length + 1),
                            child: Txt(
                                txt: listTopBtn[i],
                                txtColor: MyTheme.gray3Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false),
                          ),
                      ],
                    ),
                  ],
                ),
              );
  }

  drawTitle() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Txt(
            txt: myTaskController.getTaskModel().title,
            txtColor: MyTheme.gray5Color,
            txtSize: MyTheme.txtSize + .5,
            txtAlign: TextAlign.start,
            isBold: false),
      ),
    );
  }

  drawPostedBy() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: getWP(context, 15),
              height: getWP(context, 15),
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: MyNetworkImage.loadProfileImage(
                      myTaskController.getTaskModel().ownerImageUrl),
                  fit: BoxFit.cover,
                ),
                shape: BoxShape.circle,
              ),
            ),
            SizedBox(width: 10),
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  Txt(
                      txt: "POSTED BY",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Get.to(() => ProfilePage()).then((value) {
                              if (value != null) {
                                getRefreshData();
                              }
                            });
                          },
                          child: Txt(
                              txt: myTaskController.getTaskModel().ownerName,
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        ),
                      ),
                      Txt(
                          txt: DateFun.getTimeAgoTxt(
                              myTaskController.getTaskModel().creationDate),
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Container(color: Colors.grey, height: .5),
                  ),
                ])),
          ],
        ),
      ),
    );
  }

  drawLocation() {
    var preferedLocation = myTaskController.getTaskModel().preferedLocation;
    preferedLocation = (preferedLocation.isEmpty) ? "Remote" : preferedLocation;

    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        child: ListTile(
          leading: Icon(
            Icons.location_on_outlined,
            color: MyTheme.gray3Color,
            size: 30,
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "LOCATION",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    child: Txt(
                        txt: preferedLocation,
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isOverflow: true,
                        isBold: false),
                  ),
                  (preferedLocation.isNotEmpty &&
                          !myTaskController.getTaskModel().isInPersonOrOnline)
                      ? GestureDetector(
                          onTap: () {
                            Get.to(() => TaskLocationMap());
                          },
                          child: Txt(
                              txt: "View map",
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )
                      : SizedBox()
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Container(color: Colors.grey, height: .5),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawDueDate() {
    bool isReshedule = false;
    if (!isPoster) {
      if (myTaskController.getStatusCode() ==
              TaskStatusCfg.TASK_STATUS_ACTIVE ||
          myTaskController.getStatusCode() == TaskStatusCfg.TASK_STATUS_DRAFT) {
        isReshedule = true;
      }
    }

    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        child: ListTile(
          leading: Icon(
            Icons.calendar_today_outlined,
            color: MyTheme.gray3Color,
            size: 30,
          ),
          title: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                  txt: "DUE DATE",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 10),
              Row(
                children: [
                  Expanded(
                    child: Txt(
                        txt: DateFun.getFormatedDate(
                            mDate: DateFormat("dd-MMM-yyyy").parse(
                                myTaskController.getTaskModel().deliveryDate),
                            format: "EEE MMM dd, yyyy"),
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  (isReshedule)
                      ? GestureDetector(
                          onTap: () {
                            onEditTask(false);
                          },
                          child: Txt(
                              txt: "Reschedule",
                              txtColor: MyTheme.brandColor,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.start,
                              isBold: false),
                        )
                      : SizedBox()
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawPriceBox(List<TaskBiddingModel> listTaskBidding) {
    if (listTaskBidding == null) return SizedBox();

    var priceApproxHrTxt = "";
    var priceTxt =
        myTaskController.getTaskModel().fixedBudgetAmount.round().toString();
    if (!myTaskController.getTaskModel().isFixedPrice) {
      priceApproxHrTxt = "Approx. " +
          myTaskController.getTaskModel().totalHours.toString() +
          " hrs";
      priceTxt = (myTaskController.getTaskModel().totalHours *
              myTaskController.getTaskModel().fixedBudgetAmount)
          .round()
          .toString();
    }

    var perTaskerTxt = "";
    if (myTaskController.getTaskModel().workerNumber > 1) {
      perTaskerTxt = " per Tasker";
    }
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            border: Border.all(color: Colors.grey.shade500, width: .5)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              decoration: new BoxDecoration(
                  color: MyTheme.gray2Color,
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(20.0),
                    topRight: const Radius.circular(20.0),
                  )),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/images/svg/ic_card.svg',
                    ),
                    SizedBox(width: 10),
                    Txt(
                        txt: "TASK PRICE",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .5,
                        txtAlign: TextAlign.start,
                        isBold: false)
                  ],
                ),
              ),
            ),
            SizedBox(height: 20),
            (priceApproxHrTxt != '')
                ? Txt(
                    txt: priceApproxHrTxt,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.start,
                    isBold: false)
                : SizedBox(),
            SizedBox(height: 20),
            drawPrice(
              price: priceTxt,
              txtSize: 45,
              txtColor: Colors.black,
            ),
            //SizedBox(height: 20),
            (perTaskerTxt != '')
                ? Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Txt(
                        txt: perTaskerTxt,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .3,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  )
                : SizedBox(),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Container(color: Colors.grey, height: .5),
            ),
            _drawTaskPriceBtn(listTaskBidding),
          ],
        ),
      ),
    );
  }

  _drawTaskPriceBtn(List<TaskBiddingModel> listTaskBidding) {
    int taskBiddingUserId = 0;
    try {
      taskBiddingUserId = listTaskBidding[0].userId;
    } catch (e) {}

    if (!isPoster) {
      return _drawTaskPriceBtn_User(listTaskBidding);
    } else if (taskBiddingUserId == userData.userModel.id) {
      return _drawTaskPriceBtn_Taskbidding(listTaskBidding);
    } else {
      //final radius = 20;
      taskDetController.isShowPrivateMessage.value = false;
      String btnTxt1 = "";
      bool isPaid = false;
      bool isAccepted = false;
      bool isCancelled = false;
      bool isMakeOffer = false;
      if (myTaskController.getStatusCode() ==
          TaskStatusCfg.TASK_STATUS_ACTIVE) {
        final totalBidsNumber = myTaskController.getTaskModel().totalBidsNumber;
        if (totalBidsNumber > 0) {
          btnTxt1 = "View all offers";
        } else {
          btnTxt1 = "Make an offer";
          isMakeOffer = true;
        }
      } else if (myTaskController.getStatusCode() ==
          TaskStatusCfg.TASK_STATUS_PAYMENTED) {
        btnTxt1 = "PAID";
        isPaid = true;
      } else if (myTaskController.getStatusCode() ==
          TaskStatusCfg.TASK_STATUS_ACCEPTED) {
        btnTxt1 = "ASSIGNED";
        isAccepted = true;
      } else if (myTaskController.getStatusCode() ==
          TaskStatusCfg.TASK_STATUS_CANCELLED) {
        btnTxt1 = "CANCELLED";
        isCancelled = true;
      }
      return Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            child: MMBtn(
              txt: btnTxt1,
              bgColor: (isPaid || isAccepted || isCancelled)
                  ? MyTheme.gray4Color
                  : null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 0,
              callback: () async {
                if (isMakeOffer) {
                  var taskBidding = TaskBiddingModel();
                  try {
                    taskBidding = listTaskBidding[0];
                  } catch (e) {}
                  Get.to(() => UpdateOfferPage(taskBidding: taskBidding)).then(
                    (value) {
                      if (value != null) {
                        getRefreshData();
                      }
                    },
                  );
                }
              },
            ),
          ),
        ],
      );
    }
  }

  _drawTaskPriceBtn_User(List<TaskBiddingModel> listTaskBidding) {
    if (isPaymentDue) {
      return SizedBox();
    }
    switch (myTaskController.getStatusCode()) {
      case TaskStatusCfg.TASK_STATUS_ACTIVE:
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: MMBtn(
                txt: "Review Offers",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                radius: 20,
                callback: () async {
                  Get.to(() => ViewOfferPage(
                      taskBidding: listTaskBidding.length > 0
                          ? listTaskBidding[0]
                          : null)).then(
                    (value) {
                      if (value != null) {
                        getRefreshData();
                      }
                    },
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10, left: 20, right: 20, bottom: 10),
              child: _drawTaskPriceInfo(
                  "You will be prompted to confirm the release of payment"),
            ),
          ],
        );
        break;
      case TaskStatusCfg.TASK_STATUS_ACCEPTED:
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: MMBtn(
                txt: "Release payment",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                radius: 20,
                callback: () async {
                  onReleasePayment(listTaskBidding[0]);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 10, left: 20, right: 20, bottom: 10),
              child: _drawTaskPriceInfo(
                  "You will be prompted to confirm the release of payment"),
            ),
          ],
        );
        break;
      case TaskStatusCfg.TASK_STATUS_PAYMENTED:
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 20, bottom: 20),
              child: MMBtn(
                txt: "PAID",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                bgColor: MyTheme.gray4Color,
                txtColor: Colors.white,
                radius: 5,
                callback: () async {},
              ),
            ),
          ],
        );
        break;
      case TaskStatusCfg.TASK_STATUS_CANCELLED:
        return Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
              child: MMBtn(
                txt: "CANCELLED",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                bgColor: MyTheme.gray4Color,
                txtColor: Colors.white,
                radius: 20,
                callback: () async {},
              ),
            ),
          ],
        );
        break;
      default:
        return SizedBox();
    }
  }

  _drawTaskPriceBtn_Taskbidding(List<TaskBiddingModel> listTaskBidding) {
    bool isTaskBiddingAccepted = false;
    if (TaskStatusCfg().getSatusCode(listTaskBidding[0].status) ==
        TaskStatusCfg.TASK_STATUS_ACCEPTED) {
      isTaskBiddingAccepted = true;
    }

    if (TaskStatusCfg().getSatusCode(listTaskBidding[0].status) ==
        TaskStatusCfg.TASK_STATUS_ACTIVE) {
      taskDetController.isShowPrivateMessage.value = false;
    } else {
      taskDetController.isShowPrivateMessage.value = true;
    }

    //
    if (myTaskController.getStatusCode() ==
            TaskStatusCfg.TASK_STATUS_ACCEPTED &&
        isTaskBiddingAccepted) {
      String btnTxt1 = "Make an offer";
      String btnTxt2 = "Receive payment";
      btnTxt1 = "Request payment";

      bool isFuneded = false;
      if (listTaskBidding[0].paymentStatus ==
          TaskStatusCfg.STATUS_PAYMENT_METHOD_FUNDED) {
        btnTxt1 = "Funded";
        btnTxt2 = "Request release payment";
        isFuneded = true;
      }
      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: MMBtn(
              txt: btnTxt1,
              bgColor: (isFuneded) ? MyTheme.gray4Color : null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                if (!isFuneded) {
                  if (btnTxt1 == "Request payment") {
                    Get.to(
                      () => ReqPaymentPage(
                        taskBidding: listTaskBidding[0],
                      ),
                    ).then(
                      (value) {
                        wsGetTimelineByApp();
                      },
                    );
                  }
                }
              },
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            child: MMBtn(
              txt: btnTxt2,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                if (btnTxt2 == "Receive payment") {
                  Get.to(
                    () => RecPaymentPage(
                      taskBidding: listTaskBidding[0],
                    ),
                  ).then(
                    (value) {
                      wsGetTimelineByApp();
                    },
                  );
                } else if (btnTxt2 == "Request release payment") {
                  Get.to(
                    () => ReqPaymentPage(
                      taskBidding: listTaskBidding[0],
                    ),
                  ).then(
                    (value) {
                      wsGetTimelineByApp();
                    },
                  );
                }
              },
            ),
          ),
        ],
      );
    } else if (myTaskController.getStatusCode() ==
        TaskStatusCfg.TASK_STATUS_PAYMENTED) {
      return Padding(
        padding:
            const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
        child: MMBtn(
          txt: "PAID",
          bgColor: MyTheme.gray4Color,
          width: getW(context),
          height: getHP(context, MyTheme.btnHpa),
          radius: 5,
          callback: () async {},
        ),
      );
    } else if (myTaskController.getStatusCode() ==
            TaskStatusCfg.TASK_STATUS_ACCEPTED &&
        listTaskBidding[0].status ==
            TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACTIVE)) {
      String btnTxt1 = "Make an offer";
      bool isPaid = false;
      bool isAccepted = false;
      if (myTaskController.getStatusCode() ==
          TaskStatusCfg.TASK_STATUS_PAYMENTED) {
        btnTxt1 = "PAID";
        isPaid = true;
      } else if (myTaskController.getStatusCode() ==
          TaskStatusCfg.TASK_STATUS_ACCEPTED) {
        btnTxt1 = "ASSIGNED";
        isAccepted = true;
      }

      return Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
            child: MMBtn(
              txt: btnTxt1,
              bgColor: (isPaid || isAccepted) ? MyTheme.gray4Color : null,
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                if (!isPaid && !isAccepted) {}
              },
            ),
          ),
          SizedBox(height: 10),
        ],
      );
    } else if (listTaskBidding[0].id > 0) {
      return Column(
        children: [
          Padding(
            padding:
                const EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 20),
            child: MMBtn(
              txt: "Update offer",
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                if (userData.communityId != 2) {
                  Get.to(() =>
                      ConfirmOfferPage(taskBidding: listTaskBidding[0])).then(
                    (value) {
                      if (value != null) {
                        getRefreshData();
                      }
                    },
                  );
                } else {
                  Get.to(() => UpdateOfferPage(taskBidding: listTaskBidding[0]))
                      .then((value) {
                    if (value != null) {
                      getRefreshData();
                    }
                  });
                }
              },
            ),
          ),
        ],
      );
    } else {
      return SizedBox();
    }
  }

  _drawIncreasePrice() {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RawMaterialButton(
            onPressed: () {},
            elevation: 2.0,
            fillColor: MyTheme.brandColor,
            child: Icon(
              Icons.add,
              size: 20,
              color: Colors.white,
            ),
            //padding: EdgeInsets.all(5.0),
            shape: CircleBorder(),
          ),
          //SizedBox(width: 10),
          Flexible(
            flex: 2,
            child: Txt(
                txt: "Increase price",
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          )
        ],
      ),
    );
  }

  _drawTaskPriceInfo(String txt) {
    return Padding(
      padding: const EdgeInsets.only(left: 5, right: 5, bottom: 10),
      child: Txt(
          txt: txt,
          txtColor: MyTheme.gray4Color,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.center,
          isBold: false),
    );
  }

  drawReviews(List<UserRatingsModel> listUserRatings) {
    if (listUserRatings.length == 0) return SizedBox();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: EdgeInsets.all(20),
            child: Txt(
                txt: "REVIEWS",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          ProfileHelper().drawTaskDetailsUserRatingView(listUserRatings),
          SizedBox(height: 20),
          drawLine(),
        ],
      ),
    );
  }

  drawPrivateMsg() {
    if (!taskDetController.isShowPrivateMessage.value ||
        (myTaskController.getStatusCode() == TaskStatusCfg.TASK_STATUS_ACTIVE ||
            myTaskController.getStatusCode() ==
                TaskStatusCfg.TASK_STATUS_DRAFT ||
            myTaskController.getStatusCode() ==
                TaskStatusCfg.TASK_STATUS_CANCELLED)) {
      return SizedBox();
    }

    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Txt(
                txt: "",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            MMBtn(
              txt: "Open private messages",
              width: getW(context),
              height: getHP(context, MyTheme.btnHpa),
              radius: 20,
              callback: () async {
                Get.to(() => ChatPage());
              },
            ),
          ],
        ),
      ),
    );
  }

  drawDetailsView(List<GetPicModel> listGetPicModel) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "DETAILS",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            Txt(
                txt: myTaskController.getTaskModel().description,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            (listGetPicModel != null)
                ? Container(
                    width: getW(context),
                    height:
                        getHP(context, (listGetPicModel.length > 3) ? 32 : 20),
                    child: GridView.count(
                      crossAxisCount: 3,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      primary: false,
                      padding: EdgeInsets.all(10.0),
                      children: List.generate(
                        listGetPicModel.length,
                        (index) {
                          var model = listGetPicModel[index];
                          return GestureDetector(
                            onTap: () {
                              Get.to(() => PicFullView(
                                    title: "Attachment",
                                    url: model.thumbnailPath,
                                  ));
                            },
                            child: Card(
                                child: Stack(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: MyNetworkImage.loadProfileImage(
                                          model.thumbnailPath),
                                      fit: BoxFit.cover,
                                    ),
                                    shape: BoxShape.rectangle,
                                  ),
                                ),
                                /*Positioned(
                              right: -5,
                              top: -5,
                              child: Container(
                                width: 30,
                                height: 30,
                                child: MaterialButton(
                                  onPressed: () {
                                    Get.dialog(
                                      ConfirmDialog(
                                          callback: () {
                                            TaskPicViewModel().fetchDataDelPic(
                                              context: context,
                                               apiState: APIState(APIType.del_pic, this.runtimeType, null),
                                              index: index,
                                              mediaId: model.id,
                                            );
                                          },
                                          title: "Confirmation",
                                          msg:
                                              "Are you sure, you want to remove this attachment?"),
                                    );
                                  },
                                  color: MyTheme.gray1Color,
                                  child: Text("X",
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.black)),
                                  padding: EdgeInsets.all(0),
                                  shape: CircleBorder(),
                                ),
                              ),
                            ),*/
                              ],
                            )),
                          );
                        },
                      ),
                    ),
                  )
                : SizedBox(),
            drawLine(),
          ],
        ),
      ),
    );
  }

  drawProjectSummary() {
    final workerNumber = myTaskController.getTaskModel().workerNumber;
    if (workerNumber <= 1) return SizedBox();

    final int project_layout_spots_count =
        workerNumber - myTaskController.getTaskModel().totalAcceptedNumber;
    final int project_layout_assigned_count =
        myTaskController.getTaskModel().totalAcceptedNumber;
    final int project_layout_offers_count =
        myTaskController.getTaskModel().totalBidsNumber;

    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: "TASK SUMMARY",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  _drawProjSumBox("Spots Left", project_layout_spots_count),
                  _drawProjSumBox("Assigned", project_layout_assigned_count),
                  _drawProjSumBox("Offers", project_layout_offers_count),
                ],
              ),
            ),
            /*Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: MMBtn(
                txt: "Finalise project",
                width: getW(context),
                height: getHP(context, MyTheme.btnHpa),
                radius: 20,
                callback: () async {},
              ),
            ),*/
          ],
        ),
      ),
    );
  }

  _drawProjSumBox(String title, int val) {
    return Flexible(
      child: Column(
        children: [
          Txt(
              txt: val.toString(),
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize + 2,
              txtAlign: TextAlign.center,
              isBold: false),
          Txt(
              txt: title,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize + .5,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawOffers(List<TaskBiddingModel> listTaskBidding, bool isHide) {
    if (listTaskBidding == null) return SizedBox();
    if (listTaskBidding.length == 0 &&
        myTaskController.getStatusCode() != TaskStatusCfg.TASK_STATUS_ACTIVE) {
      return SizedBox();
    } else {
      final totalBidsNumber =
          myTaskController.getTaskModel().totalBidsNumber ?? 0;
      String btnTxt1 = "Make an offer";
      if (totalBidsNumber > 0) {
        btnTxt1 = "View all offers";
      } else {}

      return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: (!isHide) ? 20 : 0),
            (!isHide)
                ? Padding(
                    padding: EdgeInsets.all((!isHide) ? 20 : 0),
                    child: Txt(
                        txt: "OFFERS(" + totalBidsNumber.toString() + ")",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  )
                : SizedBox(),
            (totalBidsNumber == 0 && !isHide)
                ? Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20, bottom: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            child: Image.asset(
                              "assets/images/icons/ic_waiting_offers.png",
                              fit: BoxFit.cover,
                              width: getW(context) / 3.5,
                              height: getW(context) / 5,
                            ),
                          ),
                          SizedBox(height: 5),
                          Txt(
                              txt: "Waiting for offers",
                              txtColor: MyTheme.gray4Color,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.center,
                              isBold: false),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
            (!isHide && listTaskBidding != null && listTaskBidding.length > 8)
                ? Container(
                    width: getW(context),
                    child: BtnOutline(
                      txt: btnTxt1,
                      width: getW(context),
                      height: getHP(context, MyTheme.btnHpa),
                      txtColor: MyTheme.brandColor,
                      borderColor: Colors.grey,
                      radius: 20,
                      callback: () async {},
                    ),
                  )
                : SizedBox(),
            _drawOfferItems(listTaskBidding, isHide),
            SizedBox(height: (!isHide) ? 20 : 0),
            drawLine(),
          ],
        ),
      );
    }
  }

  Widget _drawOfferItems(List<TaskBiddingModel> listTaskBidding, bool isHide) {
    List<Widget> list = [];

    for (var model in listTaskBidding) {
      final map = _getTextViewPriceOffered(model);
      list.add(
        Padding(
          padding: const EdgeInsets.only(top: 20),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ListTile(
                  leading: CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.transparent,
                    backgroundImage:
                        MyNetworkImage.loadProfileImage(model.ownerImageUrl),
                  ),
                  title: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        //flex: 3,
                        child: Container(
                          //width: getWP(context, 60),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: model.ownerName,
                                  txtColor: MyTheme.brandColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: true),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  UIHelper().getStarRatingView(
                                    rate: model.averageRating.round(),
                                    reviewTxt1: '(',
                                    reviews: model.ratingCount,
                                    reviewTxt2: ')',
                                    align: MainAxisAlignment.start,
                                  ),
                                  UIHelper().getCompletionText(
                                    pa: model.taskCompletionRate.round(),
                                    align: MainAxisAlignment.start,
                                    txtColor: MyTheme.gray4Color,
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      (map['btnTxt'] != '')
                          ? Flexible(
                              //flex: 2,
                              child: Container(
                                //width: getWP(context, 40),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Txt(
                                      txt: getCurSign() + map['price'],
                                      style: TextStyle(
                                          color: MyTheme.gray5Color,
                                          fontSize: 22),
                                      txtAlign: TextAlign.center,
                                      txtColor: null,
                                      txtSize: null,
                                      isBold: null,
                                    ),
                                    SizedBox(height: 10),
                                    GestureDetector(
                                      onTap: () {
                                        if (map['confirmTxt'] != null) {
                                          showConfirmDialog(
                                            context: context,
                                            title: myTaskController
                                                    .getTaskModel()
                                                    .title ??
                                                '',
                                            msg: map['confirmTxt'],
                                            callback: () {
                                              Function.apply(
                                                  map['callback'], []);
                                            },
                                          );
                                        } else {
                                          if (map['callback'] != null) {
                                            Function.apply(map['callback'], []);
                                          } else if (map['route'] != null) {
                                            Get.to(map['route']).then((value) {
                                              if (value != null) {
                                                getRefreshData();
                                              }
                                            });
                                          }
                                        }
                                      },
                                      child: Container(
                                        width:
                                            ((map['btnTxt']).toString().length *
                                                    15)
                                                .toDouble(),
                                        decoration: map['bgColor'] != null
                                            ? BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                color: map['bgColor'],
                                              )
                                            : BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                border: Border.all(
                                                    color: map['borderColor'])),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8),
                                          child: Text(
                                            map['btnTxt'],
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: map['txtColor'],
                                              fontWeight: FontWeight.bold,
                                              fontSize: 15,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : SizedBox()
                    ],
                  ),
                ),
                SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      (!isHide)
                          ? SizedBox(width: getWP(context, 15))
                          : SizedBox(),
                      Flexible(
                        child: Container(
                          color: MyTheme.gray1Color,
                          width: double.infinity,
                          //height: (!isHide) ? null : getHP(context, 5),
                          child: (model.coverLetter.length <
                                  (AppConfig.textExpandableSize + 100))
                              ? Txt(
                                  txt: model.coverLetter.toString().trim(),
                                  txtColor: MyTheme.gray5Color,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.start,
                                  isBold: false)
                              : UIHelper().expandableTxt(model.coverLetter,
                                  MyTheme.gray5Color, Colors.black),
                        ),
                      ),
                    ],
                  ),
                ),
                (!isHide)
                    ? Padding(
                        padding:
                            const EdgeInsets.only(left: 20, right: 20, top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(width: getWP(context, 15)),
                            Expanded(
                              child: Txt(
                                  txt:
                                      DateFun.getTimeAgoTxt(model.creationDate),
                                  txtColor: MyTheme.gray4Color,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  isBold: false),
                            ),
                            (TaskStatusCfg().getSatusCode(model.status) ==
                                    TaskStatusCfg.TASK_STATUS_ACTIVE)
                                ? TextButton.icon(
                                    icon: Icon(
                                      Icons.reply,
                                      color: Colors.grey,
                                    ),
                                    onPressed: () {
                                      Get.to(OffersCommentsPage(
                                        taskBiddingsModel: model,
                                      )).then((value) {
                                        if (value != null) {
                                          getRefreshData();
                                        }
                                      });
                                    },
                                    label: Text(
                                      "Reply",
                                      style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 17,
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            Expanded(child: SizedBox()),
                            GestureDetector(
                              onTapDown: (TapDownDetails details) {
                                onReportClicked(
                                    context,
                                    details.globalPosition,
                                    model.userId,
                                    ResCfg
                                        .RESOULUTION_TYPE_TASK_OFFERCOMMENT_SPAMREPORT);
                              },
                              child: Icon(
                                Icons.more_horiz,
                                color: MyTheme.gray4Color,
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox(),
                (!isHide)
                    ? Txt(
                        txt: "", // textViewReplyComments
                        txtColor: MyTheme.hotdipPink,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false)
                    : SizedBox(),
              ],
            ),
          ),
        ),
      );
    }
    return Column(children: list);
  }

  _getTextViewPriceOffered(TaskBiddingModel model) {
    //view\ViewOffersAdapter.java -> buttonViewOffer
    String price = "";
    String btnTxt = "";
    Color txtColor = MyTheme.gray5Color;
    Color bgColor;
    Color borderColor;
    bool isEnable = true;
    String confirmTxt;
    var route;
    var callback;
    if (model.userId == userData.userModel.id) {
      switch (model.status) {
        case "ACTIVE":
          price = model.fixedbiddigAmount.round().toString();
          btnTxt = "Withdraw";
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          confirmTxt = "Are you sure you want to withdraw your offer?";
          isEnable = true;
          callback = () => wsWithdrawTaskbidding();
          break;
        case "ASSIGNED":
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          btnTxt = "Assigned";
          /*if (model.isTaskOwner &&
              model.status ==
                  TaskStatusCfg().getSatus(TaskStatusCfg.TASK_STATUS_ACCEPTED))
            isEnable = true;
          else
            isEnable = false;
          if (isEnable) {
            txtColor = MyTheme.brandColor;
            borderColor = MyTheme.gray4Color;
            callback = () => onReleasePayment(model);
          } else {*/
          txtColor = Colors.white;
          bgColor = MyTheme.gray4Color;
          borderColor = MyTheme.gray4Color;
          //}
          break;
        case "PAID":
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          btnTxt = (!model.isReview) ? "Review" : "Paid";
          txtColor = (!model.isReview) ? MyTheme.brandColor : Colors.white;
          borderColor = (!model.isReview) ? MyTheme.gray4Color : null;
          bgColor = (!model.isReview) ? null : MyTheme.gray4Color;
          isEnable = (!model.isReview) ? true : false;
          callback = (!model.isReview) ? () => onReviewClicked(model) : null;
          break;
        default:
          if (model.discountAmount > 0) {
            price = model.fixedbiddigAmount.round().toString();
          } else {
            price = model.netTotalAmount.round().toString();
          }
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          break;
      }
    } else if (model.taskOwnerId == userData.userModel.id) {
      txtColor = MyTheme.brandColor;
      borderColor = MyTheme.gray4Color;
      if (model.discountAmount > 0) {
        price = model.fixedbiddigAmount.round().toString();
      } else {
        price = model.netTotalAmount.round().toString();
      }
      if (model.status == "ACTIVE") {
        btnTxt = "Accept";
        txtColor = MyTheme.brandColor;
        borderColor = MyTheme.gray4Color;
        route = () => AcceptOfferPage(taskBidding: model);
      } else if (model.status == "ASSIGNED" &&
          model.status == "FUNDED" &&
          model.status == "FUNDED_REQUESTPAYMENT") {
        btnTxt = "Release";
        txtColor = MyTheme.brandColor;
        borderColor = MyTheme.gray4Color;
      } else if (model.status == "ASSIGNED") {
        btnTxt = "Assigned";
        if (model.isTaskOwner)
          isEnable = true;
        else
          isEnable = false;
        if (isEnable) {
          txtColor = MyTheme.brandColor;
          borderColor = MyTheme.gray4Color;
          callback = () => onReleasePayment(model);
        } else {
          txtColor = Colors.white;
          bgColor = MyTheme.gray4Color;
          borderColor = MyTheme.gray4Color;
        }
      } else if (model.status == "PAID") {
        btnTxt = (!model.isReview) ? "Review" : "Paid";
        txtColor = (!model.isReview) ? MyTheme.brandColor : Colors.white;
        borderColor = (!model.isReview) ? MyTheme.gray4Color : null;
        bgColor = (!model.isReview) ? null : MyTheme.gray4Color;
        isEnable = (!model.isReview) ? true : false;
        callback = (!model.isReview) ? () => onReviewClicked(model) : null;
      } else {
        btnTxt = "";
        price = "";
      }
    }

    return {
      "btnTxt": btnTxt,
      'bgColor': bgColor,
      'txtColor': txtColor,
      'borderColor': borderColor,
      'isEnable': isEnable,
      "price": price,
      "confirmTxt": confirmTxt,
      "callback": callback,
      "route": route,
    };
  }

  drawQuestions(
      {List<TimelinePostModel> listTimelinePostsModel,
      TextEditingController textController,
      cls,
      Function(File, String) callbackCam}) {
    if (myTaskController.getStatusCode() ==
            TaskStatusCfg.TASK_STATUS_PAYMENTED ||
        myTaskController.getStatusCode() ==
            TaskStatusCfg.TASK_STATUS_ACCEPTED) {
      return SizedBox();
    }

    if (listTimelinePostsModel == null) return SizedBox();

    String picUrl = "";
    try {
      picUrl = myTaskController.getTaskModel().ownerImageUrl;
    } catch (e) {}
    int totalQ = listTimelinePostsModel.length;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, bottom: 20, top: 20),
            child: Txt(
                txt: "COMMENTS(" + totalQ.toString() + ")",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          (myTaskController.getStatusCode() !=
                  TaskStatusCfg.TASK_STATUS_CANCELLED)
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: drawTimelineHeading(),
                    ),
                    SizedBox(height: 10),
                    Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: drawLine()),
                    SizedBox(height: 20),
                    drawMessageBox(
                      context: context,
                      picUrl: picUrl,
                      hintText: 'Ask ' +
                          myTaskController.getTaskModel().ownerName +
                          " a question",
                      textController: textController,
                      callbackCam: (File file, String txt) {
                        callbackCam(file, txt);
                      },
                    ),
                    SizedBox(height: 10),
                    Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: drawLine()),
                  ],
                )
              : SizedBox(),
          drawQ(
            context: context,
            myTaskController: myTaskController,
            listTimelinePostsModel: listTimelinePostsModel,
            isReply: true,
            isPoster: isPoster,
            callback: () async {
              await APIViewModel().req<GetTimeLineByAppAPIModel>(
                context: context,
                apiState: APIState(
                    APIType.get_timeline_by_app, this.runtimeType, null),
                url: APITimelineCfg.GET_TIMELINE_URL,
                isLoading: false,
                reqType: ReqType.Get,
                param: {
                  "Count": 8,
                  "CustomerId": 0,
                  "IsPrivate": false,
                  "Page": 1,
                  "TaskId": myTaskController.getTaskModel().id,
                  "timeLineId": 0,
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
