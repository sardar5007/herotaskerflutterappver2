import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/GetTaskAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/offers/UpdateOfferTaskBiddingAPIModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/input/InputBoxHT.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../taskdetails_base.dart';

class UpdateOfferPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  const UpdateOfferPage({Key key, @required this.taskBidding})
      : super(key: key);
  @override
  State createState() => _UpdateOfferPageState();
}

class _UpdateOfferPageState extends BaseTaskDetailsStatefull<UpdateOfferPage>
    with APIStateListener {
  final offer = TextEditingController();
  final cmt = TextEditingController();
  String serviceFeeTxt = "";
  String youReceiveTxt = "";

  double offerAmount = 0;
  double serviceAmount = 0;
  double bidderAmount = 0;

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.task_bidding_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              await APIViewModel().req<GetTaskAPIModel>(
                context: context,
                url: APIMyTasksCfg.GET_TASK_URL.replaceAll(
                    "#taskId#", myTaskController.getTaskModel().id.toString()),
                reqType: ReqType.Get,
                callback: (model) async {
                  if (model != null && mounted) {
                    if (model.success) {
                      final taskModel = model.responseData.task;
                      if (taskModel != null)
                        myTaskController.setTaskModel(taskModel);
                    }
                  }
                },
              );
            } catch (e) {}

            final taskBiddingId = (model as UpdateOfferTaskBiddingAPIModel)
                .responseData
                .taskBidding
                .id;
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.TASKBIDDING_EMAI_NOTI_GET_URL
                  .replaceAll("#taskBiddingId#", taskBiddingId.toString()),
              isLoading: false,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              Get.back(result: true);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    offer.dispose();
    cmt.dispose();
    serviceFeeTxt = null;
    youReceiveTxt = null;
    super.dispose();
  }

  getRefreshData() {}
  wsGetTimelineByApp() {}
  wsWithdrawTaskbidding() {}
  onDelTaskClicked() {}

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      offer.text = widget.taskBidding.fixedbiddigAmount.round().toString();
      cmt.text = widget.taskBidding.coverLetter;
      calculateServiceCharge();
    } catch (e) {}
  }

  calculateServiceCharge() {
    double serviceCharge = 9.5;
    if (offer.text.length > 0) {
      offerAmount = double.parse(offer.text);
      serviceAmount = (offerAmount * serviceCharge) / 100;
      bidderAmount = offerAmount - serviceAmount;
      youReceiveTxt =
          "You'll receive: " + bidderAmount.toStringAsFixed(2).toString();
      serviceFeeTxt =
          "(Service fee: " + serviceAmount.toStringAsFixed(2).toString() + ")";
      setState(() {});
    } else {
      youReceiveTxt = "You'll receive: 0.0";
      serviceFeeTxt = "(Service fee: 0.0)";
      setState(() {});
    }
  }

  wsMakeOffer() async {
    try {
      final paymentStatus = (widget.taskBidding.paymentStatus != '')
          ? widget.taskBidding.paymentStatus
          : 0;
      APIViewModel().req<UpdateOfferTaskBiddingAPIModel>(
          context: context,
          apiState: APIState(APIType.task_bidding_put, this.runtimeType, null),
          url: APIMyTasksCfg.PUT_TASKBIDDING_URL,
          //isLoading: false,
          reqType: ReqType.Put,
          param: {
            "CoverLetter": cmt.text.trim(),
            "DeliveryDate":
                widget.taskBidding.deliveryDate ?? DateTime.now().toString(),
            "DeliveryTime":
                widget.taskBidding.deliveryTime ?? DateTime.now().toString(),
            "Description": widget.taskBidding.description ?? '',
            "DiscountAmount": widget.taskBidding.discountAmount ?? 0,
            "FixedbiddigAmount": double.parse(offer.text).round(),
            "HourlyRate": myTaskController.getTaskModel().hourlyRate ?? 0.0,
            "NetTotalAmount":
                myTaskController.getTaskModel().netTotalAmount ?? 0.0,
            "PaymentStatus": paymentStatus ?? 0,
            "ReferenceId": widget.taskBidding.referenceId ?? 0,
            "ReferenceType": widget.taskBidding.referenceType ?? 0,
            "ServiceFeeAmount": widget.taskBidding.serviceFeeAmount ?? 0.0,
            "ShohokariAmount": widget.taskBidding.shohokariAmount ?? 0.0,
            "Status": myTaskController.getStatusCode(),
            "Id": widget.taskBidding.id ?? 0,
            "TaskId": myTaskController.getTaskModel().id,
            "TotalHour": myTaskController.getTaskModel().totalHours,
            "TotalHourPerWeek": widget.taskBidding.totalHourPerWeek ?? 0.0,
            "UserId": userData.userModel.id,
            "UserPromotionId": widget.taskBidding.userPromotionId ?? 0,
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    var title = 'Make offer';
    if (widget.taskBidding != null) {
      title = 'Update make offer';
    }
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: title),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "Continue",
            callback: () async {
              if (cmt.text.trim().length < 25) {
                showToast(
                  context: context,
                  msg:
                      "Please enter a valid description (at least 25 characters)",
                );
                /*Get.dialog(
                  ConfirmDialog(
                      callback: () {
                        wsMakeOffer();
                      },
                      title: "Make offer",
                      msg:
                          "Are you sure you want to offer without description? HeroTasker who provide a description has a much higher chance of being accepted "),
                );*/
              } else {
                wsMakeOffer();
              }
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          (widget.taskBidding == null) ? drawNF() : makeOffers(),
        ],
      ),
    );
  }

  makeOffers() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Card(
          elevation: 0,
          color: MyTheme.gray1Color,
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Txt(
                    txt: "Your offer:",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: true),
                SizedBox(height: 10),
                InputBoxHT(
                  ctrl: offer,
                  lableTxt: null,
                  //widget.taskBidding.fixedbiddigAmount.round().toString(),
                  kbType: TextInputType.phone,
                  len: 8,
                  txtAlign: TextAlign.center,
                  txtSize: 3,
                  onChange: (txt) {
                    calculateServiceCharge();
                  },
                ),
                SizedBox(height: 10),
                Txt(
                    txt: youReceiveTxt,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
                SizedBox(height: 5),
                Txt(
                    txt: serviceFeeTxt,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false),
              ],
            ),
          ),
        ),
        drawCoverLetter()
      ],
    );
  }

  drawCoverLetter() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          children: [
            Txt(
                txt: "Why are you the best person for this task?",
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
            SizedBox(height: 10),
            Container(
              //padding: const EdgeInsets.only(bottom: 20),
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: TextFormField(
                  controller: cmt,
                  minLines: 7,
                  maxLines: 10,
                  autocorrect: false,
                  keyboardType: TextInputType.multiline,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: new InputDecoration(
                    counter: Offstage(),
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    hintText: "",
                    hintStyle: new TextStyle(
                      color: Colors.grey,
                      fontSize: getTxtSize(
                          context: context, txtSize: MyTheme.txtSize),
                    ),
                    contentPadding: const EdgeInsets.symmetric(vertical: 0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  drawNF() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Image.asset(
                  "assets/images/icons/ic_waiting_offers.png",
                  fit: BoxFit.cover,
                  width: getW(context) / 3.5,
                  height: getW(context) / 5,
                ),
              ),
              SizedBox(height: 10),
              Txt(
                  txt: "You have not yet received any Offer for this Task",
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          ),
        ),
      ),
    );
  }
}
