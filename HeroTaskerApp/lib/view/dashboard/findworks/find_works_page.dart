import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/cfg/FindWorksConfig.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APIYoutubeCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/LifecycleEventHandler.dart';
import 'package:aitl/data/model/dashboard/findworks/FiltersModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/findworks/filters/filters_page.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/mapview/FindWorksMap.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/APIHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/FiltersController.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'find_works_base.dart';

class FindWorksPage extends StatefulWidget {
  @override
  State createState() => _FindWorksPageState();
}

class _FindWorksPageState extends BaseFindWorksStatefull<FindWorksPage>
    with APIStateListener, StateListener, SingleTickerProviderStateMixin {
  List<TaskModel> listTaskModel = [];

  //  search stuff start
  //  0
  //  Sometime rebuilding whole screen might not be desirable with setState((){})
  //  for this situation you can wrap searchables with ValuelistenableBuilder widget.
  final ValueNotifier<List<TaskModel>> filtered =
      ValueNotifier<List<TaskModel>>([]);
  final FocusNode searchFocus = FocusNode();
  final searchText = TextEditingController();
  bool isSearchIconClicked = false;
  bool searching = false;
  bool isRefreshing = false;
  //  search stuff end

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int page = 1;
  int count = AppConfig.page_limit;

  //  tab stuff start here
  int status = TaskStatusCfg.TASK_STATUS_ALL;
  int totalTabs = 6;

  int tabNo = 2;
  int distance = FindWorksConfig.distance;
  int fromPrice = FindWorksConfig.minPrice;
  int toPrice = FindWorksConfig.maxPrice;
  double lat = 0.0;
  double lng = 0.0;
  String location = '';
  bool isHideAssignTask = false;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
      wsUserDevice(eventType: 'Browse Task End');
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
      wsUserDevice(eventType: 'Browse Task Start');
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB ||
          state == ObserverState.STATE_RELOAD_TAB_FIND_WORK) {
        refreshData();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.taskinfo_search &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          final List<dynamic> locations = model.responseData.locations;
          if (locations != null) {
            //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
            if (locations.length != count) {
              isPageDone = true;
            }
            try {
              if (page == 0) listTaskModel.clear();
              for (TaskModel task in locations) {
                listTaskModel.add(task);
              }
              setState(() {});
            } catch (e) {
              log(e.toString());
            }
          } else {}
        }
      }
    } catch (e) {
      log(e.toString());
    }
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      int inPersonOrOnline = 0; //  default=all
      if (tabNo == 2)
        inPersonOrOnline = 0; //  all
      else if (tabNo == 1)
        inPersonOrOnline = 2; //  remotely
      else if (tabNo == 0) inPersonOrOnline = 1; //  in person

      await APIViewModel().req<TaskInfoSearchAPIModel>(
        context: context,
        apiState: APIState(APIType.taskinfo_search, this.runtimeType, null),
        url: APIPostTaskCfg.TASKINFOBYSEARCH_POST_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": count,
          "Distance": distance ?? FindWorksConfig.distance,
          "FromPrice": fromPrice ?? FindWorksConfig.minPrice,
          "InPersonOrOnline": inPersonOrOnline,
          "IsHideAssignTask": isHideAssignTask,
          "Latitude": lat ?? 0.0,
          "Location": location ?? '',
          "Longitude": lng ?? 0.0,
          "Page": page,
          "SearchText": searchText.text ?? '',
          "Status": status,
          "ToPrice": toPrice ?? FindWorksConfig.maxPrice,
          "UserId": 0,
        },
      );
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> refreshData() async {
    if (mounted) {
      try {
        wsUserDevice(eventType: 'Browse Task Start');
      } catch (e) {}
      try {
        final filtersModel = await FiltersSharedPref().get();
        tabNo = filtersModel.tabNo;
        if (tabNo != 1) {
          //  NOT remotely
          distance = FiltersController().getDistance(filtersModel.distance);
          lat = filtersModel.lat;
          lng = filtersModel.lng;
          location = filtersModel.location;
        } else {
          distance = FindWorksConfig.distance;
          location = '';
          lat = 0.0;
          lng = 0.0;
        }
        fromPrice = FiltersController().getMinPrice(filtersModel.minPrice);
        toPrice = FiltersController().getMaxPrice(filtersModel.maxPrice);
        isHideAssignTask = filtersModel.isAvailableTasksOnly;

        //  update listTopBtn
        try {
          listTopBtn = [];

          //  btn ->  in person, remotely or all
          switch (tabNo) {
            case 0:
              listTopBtn.add("In person");
              break;
            case 1:
              listTopBtn.add("Remotely");
              break;
            case 2:
              listTopBtn.add("In person & remotely");
              break;
            default:
          }

          //  btn ->  Location
          if (tabNo != 1) {
            if (location != '') {
              listTopBtn.add((distance.toString() + 'km, ' + location));
            } else {
              listTopBtn.add((distance.toString() + 'km, London'));
            }
          }

          //  btn ->  price
          if (fromPrice == FindWorksConfig.minPrice &&
              toPrice == FindWorksConfig.maxPrice) {
            listTopBtn.add("Any price");
          } else {
            listTopBtn.add(getCurSign() +
                fromPrice.toString() +
                '-' +
                getCurSign() +
                toPrice.toString());
          }

          //  btn ->  isHideAssignTask
          if (!isHideAssignTask) {
            listTopBtn.add("Open tasks");
          }
        } catch (e) {}
      } catch (e) {}

      //setState(() {
      isSearchIconClicked = false;
      searchText.clear();
      searching = false;
      filtered.value = [];
      if (searchFocus.hasFocus) searchFocus.unfocus();
      //
      page = 0;
      isPageDone = false;
      isLoading = true;
      listTaskModel.clear();
      //});
      onLazyLoadAPI();
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      listTaskModel = null;
    } catch (e) {}
    try {
      searchText.dispose();
    } catch (e) {}
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      log(e.toString());
    }

    try {
      wsUserDevice(eventType: 'Browse Task End');
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    //onLazyLoadAPI();
    return SafeArea(
      child: DefaultTabController(
        length: totalTabs,
        child: Scaffold(
          backgroundColor: MyTheme.bgColor,
          appBar: AppBar(
            backgroundColor: MyTheme.bgColor,
            iconTheme: MyTheme.themeData.iconTheme,
            elevation: MyTheme.appbarElevation,
            //automaticallyImplyLeading: !isSearchIconClicked,
            leadingWidth: (!isSearchIconClicked) ? 0 : 56,
            leading: (!isSearchIconClicked)
                ? SizedBox()
                : IconButton(
                    onPressed: () {
                      FocusScope.of(context).requestFocus(FocusNode());
                      isSearchIconClicked = !isSearchIconClicked;
                      searchText.clear();
                      searching = false;
                      filtered.value = [];
                      if (searchFocus.hasFocus) searchFocus.unfocus();
                      setState(() {});
                    },
                    icon: Icon(Icons.arrow_back)),
            title: (!isSearchIconClicked)
                ? UIHelper().drawAppbarTitle(title: 'Find Work')
                : drawSearchbar(searchText, (text) {
                    if (text.length > 0) {
                      searching = true;
                      filtered.value = [];
                      listTaskModel.forEach((locModel) {
                        if (locModel.title
                                .toString()
                                .toLowerCase()
                                .contains(text.toLowerCase()) ||
                            locModel.ownerName
                                .toString()
                                .toLowerCase()
                                .contains(text.toLowerCase())) {
                          filtered.value.add(locModel);
                        }
                      });
                    } else {
                      searchText.clear();
                      searching = false;
                      filtered.value = [];
                      if (searchFocus.hasFocus) searchFocus.unfocus();
                    }
                  }),
            centerTitle: false,
            actions: <Widget>[
              (!isSearchIconClicked)
                  ? Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: IconButton(
                          icon: Icon(
                            Icons.search,
                            size: 30,
                          ),
                          onPressed: () {
                            isSearchIconClicked = !isSearchIconClicked;
                            setState(() {});
                          }),
                    )
                  : SizedBox(),
              SizedBox(width: (!isSearchIconClicked) ? 10 : 0),
              (!isSearchIconClicked)
                  ? IconButton(
                      icon: Icon(
                        Icons.map_outlined,
                      ),
                      onPressed: () {
                        Get.to(() => FindWorksMap(listTaskModel: listTaskModel))
                            .then((value) {
                          if (value != null) {
                            refreshData();
                          }
                        });
                      })
                  : SizedBox(),
              SizedBox(width: (!isSearchIconClicked) ? 10 : 0),
              (!isSearchIconClicked)
                  ? IconButton(
                      icon: Icon(Icons.filter_alt_outlined),
                      onPressed: () {
                        Get.to(() => FilterPage()).then((value) async {
                          if (value != null) {
                            refreshData();
                          }
                        });
                      })
                  : SizedBox(),
              (!isSearchIconClicked)
                  ? IconButton(
                      icon: Icon(Icons.help),
                      onPressed: () {
                        openUrl(context, APIYoutubeCfg.HELP_YOUTUBE_URL);
                      })
                  : SizedBox(),
            ],
            bottom: drawAppbarNavBar((index) {
              if (!isLoading) {}
            }),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout(),
          ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      color: MyTheme.gray1Color,
      child: listTaskModel.length > 0
          ? ValueListenableBuilder<List>(
              valueListenable: filtered,
              builder: (context, value, _) {
                return RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.brandColor,
                  onRefresh: () {
                    isRefreshing = true;
                    refreshData();
                    return;
                  },
                  notificationPredicate: (scrollNotification) {
                    if (scrollNotification is ScrollStartNotification) {
                      //print('Widget has started scrolling');
                    } else if (scrollNotification is ScrollEndNotification) {
                      Future.delayed(Duration(seconds: 1), () {
                        if (!isRefreshing && !isPageDone) {
                          page++;
                          onLazyLoadAPI();
                        }
                      });
                    }
                    return true;
                  },
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: searching
                        ? filtered.value.length
                        : listTaskModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return drawItem(
                          taskModel: searching
                              ? filtered.value[index]
                              : listTaskModel[index]);
                    },
                  ),
                );
              },
            )
          : (!isLoading)
              ? drawNF()
              : Container(),
    );
  }
}
