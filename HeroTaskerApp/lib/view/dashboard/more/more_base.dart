import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/db/db_page.dart';
import 'package:aitl/view/dashboard/more/help/help_page.dart';
import 'package:aitl/view/dashboard/more/noti/noti_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_history_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/payment_methods_page.dart';
import 'package:aitl/view/dashboard/more/profile/profile_page.dart';
import 'package:aitl/view/dashboard/more/reviews/reviews_page.dart';
import 'package:aitl/view/dashboard/more/settings/settings_page.dart';
import 'package:aitl/view/dashboard/more/task_settings/task_alert_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

abstract class BaseMoreStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final List<Map<String, dynamic>> listMore = [
    {"title": "Dashboard", "route": () => DashboardMore()},
    {"title": "Profile", "route": () => ProfilePage()},
    {"title": "Payment History", "route": () => PaymentHistoryPage()},
    {"title": "Payment Methods", "route": () => PaymentMethodsPage()},
    {"title": "Reviews", "route": () => ReviewsPage()},
    {"title": "Notifications", "route": () => NotiPage()},
    {"title": "Task Alerts Setting", "route": () => TaskAlertPage()},
    {"title": "Settings", "route": () => SettingsPage()},
    {"title": "Help", "route": () => HelpPage()},
    {"title": "Logout", "route": null},
  ];

  drawNotiBadge() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Container(
        decoration: BoxDecoration(
          color: MyTheme.redColor,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Padding(
          padding: const EdgeInsets.all(5),
          child: Txt(
              txt: userData.unreadNotificationCount.toString(),
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ),
      ),
    );
  }
}
