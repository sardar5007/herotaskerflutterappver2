import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseTut2Statefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  double currentPage = 0;
  PageController controller = PageController(
    initialPage: 0,
  );
  int pageCount = 4;

  onGetStarted();

  drawPage1() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: 'Hi ' +
                        userData.userModel.firstName +
                        ", let's get you earning!",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo1.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt:
                        "Set up your profile with a photo & description so Job Posters know who you are. Take the time to fill it so they choose you!",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage2() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "Find a task",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo2.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt:
                        "Browse tasks to find work you can complete. Check what needs to be done, where and when and make an offer.",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage3() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "Make an offer",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo3.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt:
                        "When making an offer, say why you're great for the task, suggest a fair price and answer any questions.",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage4() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "Get the task done, get paid",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo4.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt:
                        "Now your offer's accepted and payment is secured with HeroTasker Pay, get it done then ask for payment release!",
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawBottomBar() {
    return BottomAppBar(
      color: MyTheme.bgColor,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DotsIndicator(
              dotsCount: pageCount,
              position: currentPage,
              decorator: DotsDecorator(
                color: Colors.black87, // Inactive color
                activeColor: Colors.redAccent,
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MMBtn(
                    txt: (currentPage == 0) ? "Back" : "Previous",
                    height: getHP(context, MyTheme.btnHpa),
                    width: getWP(context, 42),
                    radius: 20,
                    callback: () {
                      if (currentPage == 0)
                        Get.back();
                      else {
                        controller.previousPage(
                            duration: Duration(milliseconds: 100),
                            curve: Curves.decelerate);
                      }
                    }),
                MMBtn(
                    txt:
                        (currentPage == pageCount - 1) ? "Get Started" : "Next",
                    height: getHP(context, MyTheme.btnHpa),
                    width: getWP(context, 42),
                    radius: 20,
                    callback: () {
                      if ((currentPage == pageCount - 1)) {
                        onGetStarted();
                      } else {
                        controller.nextPage(
                            duration: Duration(milliseconds: 100),
                            curve: Curves.decelerate);
                      }
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
