import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'more_base.dart';

enum eMoreEvent {
  app_tut1,
  signout,
}

class MorePage extends StatefulWidget {
  @override
  State createState() => _MorePageState();
}

class _MorePageState extends BaseMoreStatefull<MorePage> with StateListener {
  var unreadNotiCount = 0;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB_MORE &&
          data == this.runtimeType) {
        unreadNotiCount =
            await PrefMgr.shared.getPrefInt("unreadNotificationCount");
        StateProvider().notify(ObserverState.STATE_BOTNAV, null);
        setState(() {});
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    super.dispose();
  }

  appInit() async {
    try {
      try {
        _stateProvider = new StateProvider();
        _stateProvider.subscribe(this);
      } catch (e) {}
      unreadNotiCount =
          await PrefMgr.shared.getPrefInt("unreadNotificationCount");
    } catch (e) {}
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          automaticallyImplyLeading: false,
          title: UIHelper().drawAppbarTitle(title: 'More'),
          centerTitle: false,
        ),
        body: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return Padding(
      padding: const EdgeInsets.only(top: 10),
      child: Container(
        child: ListView.builder(
          itemCount: listMore.length,
          itemBuilder: (context, index) {
            Map<String, dynamic> mapMore = listMore[index];
            return GestureDetector(
              onTap: () async {
                if (mapMore['route'] != null) {
                  Get.to(mapMore['route']).then((value) {
                    if (value != null) {
                      if (value['moreEvent'] == eMoreEvent.app_tut1) {
                        StateProvider()
                            .notify(ObserverState.STATE_OPEN_HELP_DIALOG, null);
                      }
                    } else {
                      if (mapMore['title'] == 'Notifications') {
                        unreadNotiCount =
                            userData.userModel.unreadNotificationCount;
                        StateProvider()
                            .notify(ObserverState.STATE_BOTNAV, null);
                        setState(() {});
                      }
                    }
                  });
                } else {
                  showConfirmDialog(
                    context: context,
                    title: "Log out",
                    msg: "Are you sure you want to log out?",
                    callback: () {
                      StateProvider().notify(ObserverState.STATE_LOGOUT, null);
                    },
                  );
                }
              },
              child: Container(
                color: Colors.transparent,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Padding(
                      padding: const EdgeInsets.only(left: 20, right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: Txt(
                                txt: mapMore['title'].toString(),
                                txtColor: MyTheme.gray5Color,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: false),
                          ),
                          (mapMore['title'] == 'Notifications' &&
                                  userData.unreadNotificationCount > 0)
                              ? drawNotiBadge()
                              : SizedBox(),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: MyTheme.gray3Color,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    drawLine(),
                    SizedBox(height: 10),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
