import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/UserCommentPublicModelList.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../mytasks/taskdetails_base.dart';

abstract class BaseOffersStatefull<T extends StatefulWidget>
    extends BaseTaskDetailsStatefull<T> {
  int index2 = 0;
  String msgTmp = '';
  String lastMsgDate2;

  Widget buildSingleMessage(
      List<UserCommentPublicModelList> listCommentsModel, int index) {
    try {
      UserCommentPublicModelList commentsModel = listCommentsModel[index];
      bool fromMe =
          (commentsModel.user.id == userData.userModel.id) ? false : true;

      //if (!fromMe) index2 = index;

      /* String lastMsgDate;
      DateTime date1;
      try {
        date1 = Jiffy(commentsModel.dateCreatedUtc).dateTime;
        DateTime date2 = DateTime.now();
        if (index < listCommentsModel.length - 1) {
          final UserCommentPublicModelList timelineModel2 =
              listCommentsModel[(index + 1)];
          date2 = Jiffy(timelineModel2.dateCreatedUtc).dateTime;
        }
        if (date1.day == date2.day || date2 == DateTime.now()) {
          // log(days.toString());
          var inputDate = DateTime.parse(date1.toString());
          var outputFormat = DateFormat('dd-MMMM-yyyy');
          lastMsgDate = outputFormat.format(inputDate);
        }
      } catch (e) {
        //log(e.toString());
      }*/

      //bool isSameDate = (lastMsgDate2 == lastMsgDate) ? true : false;
      //lastMsgDate2 = lastMsgDate;

      Alignment alignment = Alignment.topLeft;
      //Alignment chatArrowAlignment = Alignment.topLeft;

      //Color chatBgColor = fromMe ? MyTheme.dGreenColor : MyTheme.brandColor;

      //Color chatBgColor;
      //Color txtColor;
      //if (fromMe) {
      //chatBgColor = MyTheme.dGreenColor;
      //txtColor = Colors.white;
      //} else {
      //chatBgColor = MyTheme.ivory_dark;
      //txtColor = Colors.black;
      //}

      EdgeInsets edgeInsets = EdgeInsets.fromLTRB(15, 5, 5, 5);
      EdgeInsets margins = EdgeInsets.fromLTRB(10, 5, 80, 5);

      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(width: getWP(context, 15)),
                Flexible(
                  flex: 6,
                  child: Txt(
                      txt: commentsModel.user.name ?? userData.userModel.name,
                      txtColor: MyTheme.gray4Color,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                SizedBox(width: 20),
                (fromMe)
                    ? Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: MyTheme.gray5Color.withAlpha(120),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.only(
                              left: 5, right: 5, top: 3, bottom: 3),
                          child: Text(
                            "POSTER",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                            ),
                          ),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
            (commentsModel.commentText != null &&
                    commentsModel.commentText != '')
                ? Row(
                    children: [
                      getImage(commentsModel.user.profileImageUrl),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.all(10),
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: edgeInsets,
                                child: (commentsModel.commentText.length <
                                        AppConfig.textExpandableSize)
                                    ? Txt(
                                        txt: commentsModel.commentText
                                            .toString()
                                            .trim(),
                                        txtColor: MyTheme.gray5Color,
                                        txtSize: MyTheme.txtSize - .3,
                                        txtAlign: TextAlign.start,
                                        isBold: false)
                                    : UIHelper().expandableTxt(
                                        commentsModel.commentText,
                                        MyTheme.gray5Color,
                                        Colors.white,
                                      ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                : SizedBox(),
            (commentsModel.additionalData != '')
                ? Row(
                    children: [
                      getImage(commentsModel.user.profileImageUrl),
                      Expanded(
                        child: Container(
                          //color: Colors.black,
                          child: Container(
                            margin: margins,
                            child: Align(
                              alignment: alignment,
                              child: GestureDetector(
                                onTap: () {
                                  Get.to(() => PicFullView(
                                        title: "Attachment",
                                        url: commentsModel.additionalData,
                                      ));
                                },
                                child: Container(
                                  width: getWP(context, 20),
                                  height: getWP(context, 20),
                                  decoration: BoxDecoration(
                                    //color: Colors.black,
                                    image: DecorationImage(
                                      image: MyNetworkImage.loadProfileImage(
                                          commentsModel.additionalData),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : SizedBox(),
            Align(
              alignment: Alignment.bottomLeft,
              child: Padding(
                padding: EdgeInsets.only(left: getWP(context, 15)),
                child: Txt(
                    txt: DateFun.getTimeAgoTxt(commentsModel.dateCreated),
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      );
    } catch (e) {
      return Container();
    }
  }

  getImage(url) {
    return Container(
      decoration: MyTheme.picEmboseCircleDeco,
      width: getWP(context, 10),
      height: getWP(context, 10),
      child: CircleAvatar(
        radius: 25.0,
        backgroundColor: Colors.transparent,
        backgroundImage:
            new CachedNetworkImageProvider(MyNetworkImage.checkUrl(url)),
      ),
    );
  }
}
