import 'dart:io';

import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/CommentAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/GetTimeLineByAppAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'taskdetails_comments_base.dart';

// ignore: must_be_immutable
class TaskDetailsCommentsPage extends StatefulWidget {
  int timelineId;
  TaskDetailsCommentsPage({Key key, @required this.timelineId})
      : super(key: key);
  @override
  State createState() => _TaskDetailsCommentsState();
}

class _TaskDetailsCommentsState
    extends BaseTaskDetailsCommentsStatefull<TaskDetailsCommentsPage>
    with APIStateListener {
  final TextEditingController textController = TextEditingController();
  final ScrollController scrollController = new ScrollController();

  List<TimelinePostModel> listTimelinePostModel;
  String comments = '';

//  **************  app states start

  @override
  void onDetached() {
    try {
      log("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      log("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      log("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      log("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  void onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_timeline_by_app &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listTimelinePostModel = [];
            if (widget.timelineId == null) {
              listTimelinePostModel = (model as GetTimeLineByAppAPIModel)
                  .responseData
                  .timelinePosts;
            } else {
              final listTimelinePostsModel2 =
                  (model as GetTimeLineByAppAPIModel)
                      .responseData
                      .timelinePosts;
              for (var item in listTimelinePostsModel2) {
                if (item.id == widget.timelineId) {
                  listTimelinePostModel.add(item);
                  break;
                }
              }
            }
            setState(() {
              Future.delayed(const Duration(seconds: 1), () {
                scrollDown(scrollController,
                    getH(context) * AppConfig.chatScrollHeight);
              });
            });
          }
        }
      } else if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsPostComments(
                (model as MediaUploadFilesAPIModel).responseData.images[0].url);
          }
        }
      } else if (apiState.type == APIType.post_comment &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetTimelineByAPI();
            await APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.COMMENTS_EMAI_NOTI_GET_URL.replaceAll(
                  "#commentId#",
                  (model as CommentAPIModel).comment.id.toString()),
              isLoading: false,
              reqType: ReqType.Get,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              StateProvider().notify(
                  ObserverState.STATE_RELOAD_TASKDETAILS_GET_TIMELINE, null);
            } catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  wsPostComments(String url) async {
    try {
      await APIViewModel().req<CommentAPIModel>(
        context: context,
        apiState: APIState(APIType.post_comment, this.runtimeType, null),
        url: APITimelineCfg.COMMENTS_POST_URL,
        param: {
          "AdditionalData": url,
          "CanDelete": true,
          "CommentText": comments,
          "EntityId": widget.timelineId,
          "EntityName": "TimelinePost",
          "UserId": userData.userModel.id,
        },
        reqType: ReqType.Post,
      );
      comments = '';
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      log(e.toString());
    }
    textController.dispose();
    scrollController.dispose();
    listTimelinePostModel = null;
    comments = null;
    super.dispose();
  }

  Future<void> wsGetTimelineByAPI() async {
    try {
      await APIViewModel().req<GetTimeLineByAppAPIModel>(
        context: context,
        apiState: APIState(APIType.get_timeline_by_app, this.runtimeType, null),
        url: APITimelineCfg.GET_TIMELINE_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": 8,
          "CustomerId": 0,
          "IsPrivate": false,
          "Page": 1,
          "TaskId": myTaskController.getTaskModel().id,
          "timeLineId": 0,
        },
      );
    } catch (e) {}
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    if (myTaskController.getTaskModel().userId == userData.userModel.id) {
      isPoster = false;
    }

    if (widget.timelineId == null) {
      widget.timelineId = listTimelinePostModel.length - 1;
    }

    wsGetTimelineByAPI();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: 'Chat'),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    var ownerName = "";
    try {
      ownerName = listTimelinePostModel[0].ownerName;
    } catch (e) {}

    return Container(
      //width: double.infinity,
      height: double.maxFinite,
      //color: Colors.amber,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: RefreshIndicator(
              color: Colors.white,
              backgroundColor: MyTheme.brandColor,
              onRefresh: wsGetTimelineByAPI,
              child: ListView(
                controller: scrollController,
                //shrinkWrap: true,
                reverse: false,
                //primary: true,
                children: [
                  drawTimelineHeading(),
                  drawLine(),
                  Container(
                    child: drawMessageList(
                        listTimelinePostsModel: listTimelinePostModel),
                  ),
                  //Spacer(),
                ],
              ),
            ),
          ),
          Positioned(
            child: new Align(
              alignment: FractionalOffset.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: drawMessageBox(
                  context: context,
                  hintText: 'Reply to ' + ownerName,
                  textController: textController,
                  callbackCam: (File file, String txt) async {
                    comments = txt.trim();
                    textController.clear();
                    if (file != null) {
                      await APIViewModel().upload(
                        context: context,
                        apiState: APIState(
                            APIType.media_upload_file, this.runtimeType, null),
                        file: file,
                      );
                    } else {
                      wsPostComments("");
                    }
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
