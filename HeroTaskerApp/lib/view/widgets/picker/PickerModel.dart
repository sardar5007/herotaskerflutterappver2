import 'package:flutter/material.dart';

class PickerModel {
  void show(
      {@required BuildContext context,
      @required String title,
      @required List<MaterialButton> listBtn}) {
    showDialog<int>(
      context: context,
      builder: (context) => AlertDialog(content: Text(title), actions: listBtn),
    );
  }
}
