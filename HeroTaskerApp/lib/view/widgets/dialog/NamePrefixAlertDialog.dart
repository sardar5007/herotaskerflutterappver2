import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';

showNamePrefixAlertDialog(
    {DropListModel ddTitleList,
    Function(OptionItem optionItem) callback,
    String title,
    var context}) {
  return showGeneralDialog(
      barrierColor: Colors.black.withOpacity(0.2),
      transitionBuilder: (context, a1, a2, widget) {
        return Transform.scale(
          scale: a1.value,
          child: Opacity(
            opacity: a1.value,
            child: Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              backgroundColor: Colors.white,
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: ddTitleList.listOptionItems.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      callback(ddTitleList.listOptionItems[index]);
                    },
                    child: ListTile(
                      title: Padding(
                        padding: const EdgeInsets.all(5),
                        child: Card(
                            child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Txt(
                                    txt:
                                        "${ddTitleList.listOptionItems[index].title}",
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize,
                                    txtAlign: TextAlign.center,
                                    isBold: false))),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        );
      },
      transitionDuration: Duration(milliseconds: 300),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      pageBuilder: (context, animation1, animation2) {});
}
