import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

// ignore: non_constant_identifier_names
Future<Widget> TaskDetailsAlertDialog(
    {@required var caseAssignedBy,
    @required var caseDescription,
    @required var caseTitle,
    @required var caseID,
    @required var alertBody,
    @required var alertTitle,
    @required var deleteTxt,
    @required var cancelTxt,
    @required Function deleteClick,
    @required Function cancelClick,
    @required var context}) {
  return showDialog(
    context: context,
    builder: (ctx) => Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      backgroundColor: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Txt(
                    txt: alertTitle,
                    txtColor: MyTheme.brandColor,
                    txtSize: MyTheme.txtSize + 1,
                    isBold: true,
                    txtAlign: TextAlign.center,
                  ),
                ),
                Container(height: 1, color: Colors.black),
                SizedBox(height: 10.0),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Case No: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseID,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Title: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseTitle,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Description: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseDescription,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Assigned by: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseAssignedBy,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Txt(
                    txtAlign: TextAlign.center,
                    txt: alertBody,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    isBold: false,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15.0),
          Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: deleteClick,
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: MyTheme.brandColor,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(15.0),
                      ),
                    ),
                    child: Center(
                      child: Txt(
                        txtAlign: TextAlign.center,
                        txt: deleteTxt,
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        isBold: false,
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: cancelClick,
                  child: Container(
                    height: 60,
                    decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(15.0),
                      ),
                    ),
                    child: Center(
                      child: Txt(
                        txtAlign: TextAlign.center,
                        txt: cancelTxt,
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        isBold: false,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    ),
  );
}
