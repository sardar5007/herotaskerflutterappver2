import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:popup_menu/popup_menu.dart';

drawPopopMenu({
  @required context,
  @required List<String> list,
  @required Offset offset,
  @required Function(String) callback,
}) {
  PopupMenu.context = context;
  List<MenuItem> items = [];
  for (var str in list) items.add(MenuItem(title: str));
  PopupMenu menu = PopupMenu(
      //backgroundColor: Colors.white,
      //highlightColor: Colors.black,

      // lineColor: Colors.tealAccent,
      maxColumn: items.length,
      items: items,
      onClickMenu: (MenuItemProvider item) {
        //print('Click menu -> ${item.menuTitle}');
        callback(item.menuTitle);
      },
      stateChanged: (bool isShow) {
        //print('menu is ${isShow ? 'showing' : 'closed'}');
      },
      onDismiss: () {});
  double left = offset.dx;
  double top = offset.dy;
  menu.show(rect: Rect.fromLTWH(left, top, 0, 0));
}
