import 'package:aitl/Mixin.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/data/network/CookieMgr.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;
import 'WebBase.dart';

class WebScreen extends StatefulWidget {
  final String url;
  final String title;
  final String caseID;

  const WebScreen({
    Key key,
    this.caseID,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _WebScreenState();
}

class _WebScreenState extends WebBase<WebScreen> with Mixin {
  //final GlobalKey webViewKey = GlobalKey();
  //InAppWebViewController webView;

  // here we checked the url state if it loaded or start Load or abort Load

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    cookieStr = null;
    flutterWebviewPlugin.close();
    flutterWebviewPlugin.dispose();
    //webView = null;
    super.dispose();
  }

  appInit() async {
    try {
      /*CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();*/

      CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      log(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Uri.parse(Server.BASE_URL),
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );

        /*flutterWebviewPlugin.onUrlChanged.listen((String url) {
          log("onUrlChanged::" + url);
        });*/

        flutterWebviewPlugin.onStateChanged.listen((viewState) {
          if (mounted) {
            if (viewState.type == WebViewState.startLoad) {
              EasyLoading.show(status: "Loading...");
              //setState(() {
              //isLoading = true;
              //});
            } else if (viewState.type == WebViewState.finishLoad) {
              //Future.delayed(Duration(seconds: 5), () {
              EasyLoading.dismiss();
              //});

              //setState(() {
              //isLoading = false;
              //  });
            }
          }
        });

        /*flutterWebviewPlugin.onScrollYChanged.listen((double y) {
          if (mounted) {
            /*setState(() {
              _shouldShowBottomBar = y < 50;
            });*/
          }
        });*/

        setState(() {});
      }
    } catch (e) {
      cookieStr = "";
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    final FlutterWebviewPlugin webviewPlugin = new FlutterWebviewPlugin();

    return Scaffold(
      body: (cookieStr == null)
          ? SizedBox()
          : drawWebView(
              context: context,
              title: widget.title,
              url: widget.url,
              cookieStr: cookieStr,
              callback: (eDocFilesView e, message) {
                final extension = p.extension(message);
                print(extension);
                if (extension.toLowerCase() == ".pdf") {
                  Get.to(
                    () => PDFDocumentPage(
                      title: "Document View",
                      url: message,
                    ),
                  );
                } else if (extension.toLowerCase() == ".doc" ||
                    extension.toLowerCase() == ".docx" ||
                    extension.toLowerCase() == ".xls") {
                  flutterWebviewPlugin.reloadUrl(
                      "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                          message);
                } else {
                  flutterWebviewPlugin.hide();
                  Get.to(() => PicFullView(
                        url: message,
                        title: "Document View",
                      )).then((value) => flutterWebviewPlugin.show());
                }
              }),
    );
  }
}
