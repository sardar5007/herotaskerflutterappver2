import 'package:aitl/Mixin.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/LocalNotiAlert.dart';
import 'package:aitl/data/model/ads-on/credit_report/privacy/PrivacyPolicyAPIModel.dart';
import 'package:aitl/data/model/ads-on/credit_report/privacy/PrivacyPolicyHelper.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;

enum eDocFilesView {
  FullViewFileHandler,
}

abstract class WebBase<T extends StatefulWidget> extends State<T> with Mixin {
  InAppWebViewController wController;
  final flutterWebviewPlugin = new FlutterWebviewPlugin();
  final CookieManager cookieManager = CookieManager.instance();

  double progress = 0;
  bool isLoading = false;
  bool isTermsBusiness = false;
  String cookieStr;

  drawWebView({
    BuildContext context,
    String title,
    String url,
    String cookieStr,
    Function(eDocFilesView e, String message) callback,
  }) {
    //var fontSize = 17;
    //double width = MediaQuery.of(context).size.width * .8;
    //if (title.length)
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: WebviewScaffold(
        //resizeToAvoidBottomInset: false,
        resizeToAvoidBottomInset: true,
        scrollBar: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: Colors.white,
          automaticallyImplyLeading: false,
          title: FittedBox(
            fit: BoxFit.fitWidth,
            child:
                AutoSizeText((!isTermsBusiness) ? title : "Terms of Business",
                    style: TextStyle(
                        color: MyTheme.redColor,
                        //fontSize: 17,
                        fontWeight: FontWeight.bold)),
          ),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(!isTermsBusiness ? Icons.arrow_back : Icons.close,
                color: MyTheme.redColor),
            onPressed: () async {
              if (!isTermsBusiness)
                Navigator.pop(context);
              else {
                flutterWebviewPlugin.goBack();
                isTermsBusiness = false;
                setState(() {});
              }
            },
          ),
        ),
        allowFileURLs: true,
        url: url,
        headers: {'Cookie': cookieStr},
        withLocalStorage: true,
        //enableAppScheme: true,
        hidden: true,
        withZoom: true,
        appCacheEnabled: false,
        debuggingEnabled: true,
        clearCookies: false,
        mediaPlaybackRequiresUserGesture: false,
        ignoreSSLErrors: true,
        //useWideViewPort: false,
        javascriptChannels: [
          /*  JavascriptChannel(
            name: 'Back',
            onMessageReceived: (JavascriptMessage message) {
              print(message.message);
              Navigator.of(context).pop();
            }),*/

          JavascriptChannel(
            name:
                'DownloadFileModule', //  remove HYPHEN in js handler and then check
            onMessageReceived: (JavascriptMessage message) async {
              if (message.message != "") {
                print(message.message);
                flutterWebviewPlugin.hide();
                await NetworkMgr.shared.downloadFile(
                  context: context,
                  url: message.message,
                  callback: (f) {
                    LocalNotiAlert().play(
                        'Documents Download', 'File downloaded successfully');
                  },
                );
                flutterWebviewPlugin.show();
              }
            },
          ),
          JavascriptChannel(
              name: 'FullViewFileHandler',
              onMessageReceived: (JavascriptMessage message) {
                if (message.message != "") {
                  callback(eDocFilesView.FullViewFileHandler, message.message);
                }
              }),
          JavascriptChannel(
              name: 'onClickTermsOfBusiness',
              onMessageReceived: (JavascriptMessage message) async {
                if (message.message != "") {
                  print(message.message);
                  final url = PrivacyPolicyHelper().getUrl();
                  log("PrivacyPolicyHelper= " + url);
                  flutterWebviewPlugin.hide();
                  await NetworkMgr()
                      .req<PrivacyPolicyAPIModel, Null>(
                    context: context,
                    reqType: ReqType.Get,
                    url: url,
                    isLoading: true,
                  )
                      .then((model) async {
                    flutterWebviewPlugin.show();
                    for (var m
                        in model.responseData.termsPrivacyNoticeSetupsList) {
                      if (m.type == "Terms of Business") {
                        final extension = p.extension(m.webUrl);
                        print(extension);
                        if (extension.toLowerCase() == ".pdf") {
                          flutterWebviewPlugin.reloadUrl(
                              "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                                  m.webUrl);
                        } else {
                          flutterWebviewPlugin.reloadUrl(m.webUrl);
                        }
                        isTermsBusiness = true;
                        setState(() {});
                        break;
                      }
                    }
                  });
                }
                //Get.offAll(MainCustomerScreenNew());
              }),

          /*JavascriptChannel(
            name: 'markidwidget-ident-button',
            onMessageReceived: (JavascriptMessage message) {
              print(message.message);
              return {'bar': 'bar_value', 'baz': 'baz_value'};
            }),*/
        ].toSet(),
        withJavascript: true,
        initialChild: Container(
          color: Colors.white,
          child: Center(
              /*child: CircularProgressIndicator(
              color: MyTheme.redColor,
            ),*/
              ),
        ),
      ),
    );
  }
}
