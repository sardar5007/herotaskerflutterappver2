import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class PDFDocumentPage extends StatefulWidget {
  final String url;
  final String title;

  const PDFDocumentPage({
    Key key,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _PDFDocumentPageState();
}

class _PDFDocumentPageState extends State<PDFDocumentPage> with Mixin {
  final GlobalKey webViewKey = GlobalKey();

  double progress = 0;
  bool isLoading = true;
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    try {} catch (e) {}

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: MyTheme.appbarElevation,
        backgroundColor: Colors.white,
        iconTheme: MyTheme.themeData.iconTheme,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: MyTheme.redColor,
          ),
          onPressed: () {
            Get.back();
          },
        ),
        title: Text(
          '${widget.title}',
          style: TextStyle(
            color: MyTheme.redColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.bookmark,
              color: Colors.white,
            ),
            onPressed: () {
              _pdfViewerKey.currentState?.openBookmarkView();
            },
          ),
        ],
      ),
      body: Container(
        width: getW(context),
        height: getH(context),
        color: Colors.white,
        child: SfPdfViewer.network(
          '${widget.url}',
          key: _pdfViewerKey,
        ),
      ),
    );
  }
  /*@override
  Widget build(BuildContext context) {
    print("url Privacy ====== " + widget.url);
    return isLoading
            ? Container(
                child: Center(
                    child: CircularProgressIndicator(
                  backgroundColor: MyTheme.redColor,
                  strokeWidth: 4,
                )),
              )
            : Container(
      width: getW(context),
              height: getH(context),
              child: PDFViewer(
                  document: document,*/ /*
                  indicatorBackground: Colors.white,
                  indicatorText: Colors.black,
                  tooltip: const PDFViewerTooltip(),
                  enableSwipeNavigation: true,
                  scrollDirection: Axis.vertical,
                  pickerButtonColor: MyTheme.redColor,
                  pickerIconColor: Colors.white,
                  showNavigation: true,
                  zoomSteps: 1,
                  minScale: getH(context),*/ /*
                ),
            );
  }*/
}
