import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class CustomCheckbox extends StatelessWidget {
  final String txt;
  ThemeData themeData;
  final bool isSelected;
  final Function(bool) callback;

  CustomCheckbox({
    Key key,
    @required this.txt,
    this.themeData,
    @required this.callback,
    @required this.isSelected,
  }) {
    if (this.themeData == null) {
      themeData = MyTheme.radioThemeData;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        callback(!isSelected);
      },
      child: Container(
        color: Colors.transparent,
        child: Theme(
          data: themeData,
          child: Transform.translate(
            offset: Offset(-10, 0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Transform.scale(
                  scale: 1.2,
                  child: Checkbox(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: isSelected,
                    onChanged:
                        null, /*(newValue) {
                      callback(newValue);
                    },*/
                  ),
                ),
                SizedBox(width: 10),
                Txt(
                    txt: txt,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
