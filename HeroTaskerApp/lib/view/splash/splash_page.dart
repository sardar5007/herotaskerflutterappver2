import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SplashScreen extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SvgPicture.asset(
        'assets/images/logo/logo.svg',
      ),
    );
  }
}
