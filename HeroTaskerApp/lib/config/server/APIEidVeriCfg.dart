import 'package:aitl/config/server/Server.dart';

class APIEidCfg {
  static const String EID_URL =
      Server.BASE_URL + "/mrg/mark-eid-mobile-verification";

  static const SELFIE_POST_URL =
      Server.BASE_URL + "/api/camcapture/postselfieBydase64data";
  static const DOC_POST_URL =
      Server.BASE_URL + "/api/camcapture/postdocumentbybase64bitdata";
  static const VERIFY_POST_URL =
      Server.BASE_URL + "/api/camcapture/postdocumentverify";
}
