class Server {
  static const bool isTest = true;
  static const bool isOtp = false;

  //  BaseUrl
  static const String BASE_URL =
      (isTest) ? "https://herotasker.com" : "https://herotasker.com";

  static const String WSAPIKEY = "ABCXYZ123TEST";
}
