import 'package:aitl/config/server/Server.dart';

class APIBadgeCfg {
  //  More::Badge
  static const String BADGE_USER_GET_URL =
      Server.BASE_URL + "/api/userBadge/get?UserId=#userId#";
  static const String BADGE_EMAIL_URL =
      Server.BASE_URL + "/api/userBadge/postemailbadge";
  static const String BADGE_PHOTOID_URL =
      Server.BASE_URL + "/api/userBadge/post";
  static const String BADGE_PHOTOID_DEL_URL =
      Server.BASE_URL + "/api/userBadge/delete/#badgeId#";
}
