import 'Server.dart';

class ResCfg {
  static const RESOULUTION_TYPE_TASK = "Task";
  static const RESOULUTION_TYPE_USER = "User";
  static const RESOULUTION_TYPE_COMMENT = "Comment";
  static const RESOULUTION_TYPE_OTHER = "Other";

  static const RESOULUTION_TYPE_TASK_SPAMREPORT = "Task Spam Report";
  static const RESOULUTION_TYPE_TASK_OFFERCOMMENT_SPAMREPORT =
      "Offer Comment Spam Report";
  static const RESOULUTION_TYPE_TASK_COMMENT_SPAMREPORT = "Comment Spam Report";

  static const RES_POST_URL = Server.BASE_URL + "/api/resolution/post";
}
