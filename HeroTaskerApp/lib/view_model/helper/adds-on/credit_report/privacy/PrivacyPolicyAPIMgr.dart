import 'dart:developer';
import 'package:aitl/data/model/ads-on/credit_report/privacy/PrivacyPolicyAPIModel.dart';
import 'package:aitl/data/model/ads-on/credit_report/privacy/PrivacyPolicyHelper.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/material.dart';

class PrivacyPolicyAPIMgr {
  static final PrivacyPolicyAPIMgr _shared = PrivacyPolicyAPIMgr._internal();

  factory PrivacyPolicyAPIMgr() {
    return _shared;
  }

  PrivacyPolicyAPIMgr._internal();

  wsOnLoad({
    BuildContext context,
    Function(PrivacyPolicyAPIModel) callback,
  }) async {
    try {
      final url = PrivacyPolicyHelper().getUrl();
      log("PrivacyPolicyHelper= " + url);
      await NetworkMgr()
          .req<PrivacyPolicyAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
