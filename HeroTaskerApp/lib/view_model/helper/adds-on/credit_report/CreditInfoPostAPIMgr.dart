import 'dart:convert';
import 'package:aitl/config/server/APICreditReportCfg.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditInfoParms.dart';
import 'package:aitl/data/model/ads-on/credit_report/CreditInfoPostAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

class CreditInfoPostApiMgr with Mixin {
  static final CreditInfoPostApiMgr _shared = CreditInfoPostApiMgr._internal();

  factory CreditInfoPostApiMgr() {
    return _shared;
  }

  CreditInfoPostApiMgr._internal();

  creditUserInfoPost({
    BuildContext context,
    CreditInfoParms creditInfoParms,
    Function(CreditInfoPostAPIModel) callback,
  }) async {
    final jsonString = JsonString(json.encode(creditInfoParms.toJson()));
    log(jsonString.source);

    try {
      await NetworkMgr()
          .req<CreditInfoPostAPIModel, Null>(
        context: context,
        reqType: ReqType.Post,
        param: creditInfoParms.toJson(),
        url: APICreditReportCfg.CREDIT_USER_INFO_POST_URL,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log("CREDIT_USER_INFO_POST_URL  ERROR = " + e.toString());
    }
  }
}
