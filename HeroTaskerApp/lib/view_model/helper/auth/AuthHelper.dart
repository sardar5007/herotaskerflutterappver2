import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

class AuthHelper {
  drawAuthHeaderImage(context) {
    return Container(
      //width: getWP(context, 70),
      //height: getHP(context, 30),
      child: Column(
        children: [
          SvgPicture.asset(
            'assets/images/welcome/welcome_theme.svg',
            //fit: BoxFit.fill,
          ),
          RichText(
            text: new TextSpan(
              // Note: Styles for TextSpans must be explicitly defined.
              // Child text spans will inherit styles from parent
              style: new TextStyle(
                fontSize: ResponsiveFlutter.of(context)
                    .fontSize(MyTheme.txtSize + .5),
                color: Colors.black,
              ),
              children: <TextSpan>[
                new TextSpan(
                  text: 'Bring ',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize + .3)),
                ),
                new TextSpan(
                  text: 'HEROS',
                  style: TextStyle(
                      color: MyTheme.redColor,
                      fontSize: ResponsiveFlutter.of(context)
                          .fontSize(MyTheme.txtSize + .3)),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 5, left: 20, right: 20, bottom: 20),
            child: Txt(
              txt: "to do your to-dos",
              txtColor: MyTheme.timelineTitleColor,
              txtSize: MyTheme.txtSize + .3,
              txtAlign: TextAlign.center,
              isBold: false,
              //txtLineSpace: 1.5,
            ),
          ),
        ],
      ),
    );
  }

  drawGoogleFBLoginButtons(
      {BuildContext context,
      String icon,
      String txt,
      double height,
      double width,
      Function callback}) {
    return Container(
      height: height,
      width: width,
      child: OutlinedButton(
        onPressed: () {
          callback();
        },
        style: ButtonStyle(
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          ),
          side: MaterialStateProperty.resolveWith((states) {
            return BorderSide(color: MyTheme.gray4Color, width: 1);
          }),
        ),
        child: Row(children: [
          SvgPicture.asset(
            'assets/images/svg/' + icon + '.svg',
            //fit: BoxFit.fill,
          ),
          Expanded(
            child: Txt(
                txt: txt,
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
          //trailing: SizedBox(),
        ]),
      ),
    );
  }
}
