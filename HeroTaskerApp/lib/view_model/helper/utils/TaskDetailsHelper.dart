import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';

class TaskDetailsHelper {
  isTaskBiddingStatusOK(
      MyTaskController myTaskController, TaskBiddingModel model2) {
    if (myTaskController.getTaskModel().status == model2.status ||
        ((myTaskController.getTaskModel().workerNumber -
                    myTaskController.getTaskModel().totalAcceptedNumber >
                0) &&
            myTaskController.getTaskModel().workerNumber > 1 &&
            TaskStatusCfg().getSatusCode(model2.status) ==
                TaskStatusCfg.TASK_STATUS_ACTIVE) ||
        (myTaskController.getTaskModel().workerNumber > 1 &&
                myTaskController.getStatus() ==
                    TaskStatusCfg.TASK_STATUS_PAYMENTED) &&
            TaskStatusCfg().getSatusCode(model2.status) ==
                TaskStatusCfg.TASK_STATUS_ACCEPTED) {
      return true;
    } else
      return false;
  }
}
