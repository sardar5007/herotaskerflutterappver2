import 'package:aitl/config/server/APIBadgeCfg.dart';
import 'package:aitl/data/model/ads-on/action_req/BadgeAPIModel.dart';
import 'package:aitl/data/model/ads-on/action_req/BadgeEmailAPIModel.dart';
import 'package:aitl/data/model/ads-on/action_req/BadgePhotoIDAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

import 'BadgeEmailHelper.dart';

class BadgeAPIMgr with Mixin {
  static final BadgeAPIMgr _shared = BadgeAPIMgr._internal();

  factory BadgeAPIMgr() {
    return _shared;
  }

  BadgeAPIMgr._internal();

  wsGetUserBadge(
      {BuildContext context,
      int userId,
      Function(BadgeAPIModel) callback}) async {
    try {
      debugPrint(
          "badge api = ${APIBadgeCfg.BADGE_USER_GET_URL.replaceAll("#userId#", userId.toString())}");
      await NetworkMgr()
          .req<BadgeAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: APIBadgeCfg.BADGE_USER_GET_URL
            .replaceAll("#userId#", userId.toString()),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsPostEmailBadge(
      {BuildContext context,
      Function(BadgeEmailAPIModel model) callback}) async {
    try {
      await NetworkMgr()
          .req<BadgeEmailAPIModel, Null>(
        context: context,
        url: APIBadgeCfg.BADGE_EMAIL_URL,
        param: BadgeEmailHelper().getParam(),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsPostPhotoIDBadge(
      {BuildContext context,
      var params,
      Function(BadgePhotoIDAPIModel model) callback}) async {
    try {
      await NetworkMgr()
          .req<BadgePhotoIDAPIModel, Null>(
        context: context,
        url: APIBadgeCfg.BADGE_PHOTOID_URL,
        param: params,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }

  wsDelPhotoIDBadge(
      {BuildContext context,
      int id,
      Function(BadgePhotoIDAPIModel model) callback}) async {
    try {
      await NetworkMgr().req<BadgePhotoIDAPIModel, Null>(
        context: context,
        url: APIBadgeCfg.BADGE_PHOTOID_DEL_URL
            .replaceAll("#badgeId#", id.toString()),
        reqType: ReqType.Delete,
        param: {},
      ).then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
