import 'package:aitl/config/server/APIActionReqCfg.dart';

class CaseDigitalSignByCustomerURLHelper {
  static getUrl({String caseID}) {
    var url = APIActionReqCfg.CaseDigitalSignByCustomerURL;
    url = url.replaceAll("#CaseID#", caseID.toString());

    return url;
  }
}
