import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/UserNotesCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/ads-on/action_req/BadgeEmailAPIModel.dart';
import 'package:aitl/data/model/ads-on/action_req/DashBoardListType.dart';
import 'package:aitl/data/model/ads-on/action_req/UserBadgeModel.dart';
import 'package:aitl/data/model/ads-on/action_req/UserNotesModel.dart';
import 'package:aitl/data/model/ads-on/credit_report/privacy/TermsPrivacyNoticeSetups.dart';
import 'package:aitl/data/model/dashboard/action_req/MortgageCaseInfos.dart';
import 'package:aitl/data/model/dashboard/action_req/MortgageCaseRecomendationInfos.dart';
import 'package:aitl/view/widgets/dialog/PrivacyPoliceAcceptDialog.dart';
import 'package:aitl/view/widgets/dialog/TaskDetailsAlertDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dialog/DeleteConfirmationAlertDialog.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:aitl/view_model/helper/adds-on/credit_report/privacy/PrivacyPolicyAPIMgr.dart';
import 'package:aitl/view_model/rx/ActionReqController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';

import 'BadgeAPIMgr.dart';
import 'CaseDigitalSignByCustomerURLhelper.dart';
import 'CaseReviewAPIMgr.dart';
import 'NewCaseHelper.dart';
import 'UserCaseAPIMgr.dart';

class ActionReqHelper {
  wsActionReqAPIcall(
      {context,
      Function(List<DashBoardListType>) callback,
      Function(List<UserNotesModel>) callbackListUserNotes}) async {
    final actionReqControler = Get.put(ActionReqController());
    actionReqControler.isCaseChecked = List<bool>().obs;
    final List<DashBoardListType> list = [];
    /*await privacyPolicyFetchAPI(context);
    await userNotesAPI(context, (lst) {
      for (var l in lst) list.add(l);
    }, callbackListUserNotes);
    await caseReviewAPI(context, (lst) {
      for (var l in lst) list.add(l);
    });
    print(list);*/
    await updateUserBadgesAPI(context, (lst) {
      for (var l in lst) list.add(l);
    });
    print(list);
    callback(list);
  }

  privacyPolicyFetchAPI(context) async {
    var value = await PrefMgr.shared.getPrefStr("Accepted");
    if (value.toString() == "1") {
    } else {
      if (userData.userModel.isCustomerPrivacy == 0 ||
          userData.userModel.isCustomerPrivacy == 907) {
        await PrivacyPolicyAPIMgr().wsOnLoad(
          context: context,
          callback: (model) {
            if (model != null) {
              try {
                if (model.success) {
                  final termsPrivacyNoticeSetupsList =
                      model.responseData.termsPrivacyNoticeSetupsList;
                  for (TermsPrivacyNoticeSetups termsPrivacyPoliceSetups
                      in termsPrivacyNoticeSetupsList) {
                    if (termsPrivacyPoliceSetups.type.toString() ==
                        "Customer Privacy Notice") {
                      Get.dialog(
                        PrivacyPolicyDialog(termsPrivacyPoliceSetups.webUrl),
                        barrierDismissible: false,
                        barrierColor: Colors.transparent,
                        transitionDuration: Duration(milliseconds: 400),
                      );
                    }
                  }
                } else {}
              } catch (e) {}
            }
          },
        );
      }
    }
  }

  updateUserBadgesAPI(
      context, Function(List<DashBoardListType>) callback) async {
    await BadgeAPIMgr().wsGetUserBadge(
      context: context,
      userId: userData.userModel.id,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final List<UserBadgeModel> listUserBadgeModel =
                  model.responseData.userBadges;
              bool emailExitStatus;
              bool eIDExitStatus;
              for (UserBadgeModel userBaseModel in listUserBadgeModel) {
                if (userBaseModel.type == "Email" &&
                    userBaseModel.isVerified == true) {
                  emailExitStatus = true;
                }
                if (userBaseModel.type == "Email" &&
                    userBaseModel.isVerified == false) {
                  emailExitStatus = false;
                }
                if (userBaseModel.type == "ElectricId" &&
                    userBaseModel.isVerified == true) {
                  eIDExitStatus = true;
                }
                if (userBaseModel.type == "ElectricId" &&
                    userBaseModel.isVerified == false) {
                  eIDExitStatus = false;
                }
              }
              if (emailExitStatus == null || emailExitStatus == false) {
                //listUserList.add(new DashBoardListType(type: "email"));
              }
              if (eIDExitStatus == null || eIDExitStatus == false) {
                listUserList.add(new DashBoardListType(type: "eid"));
              }
              callback(listUserList);
            } else {}
          } catch (e) {}
        } else {}
      },
    );
  }

  caseReviewAPI(context, Function(List<DashBoardListType>) callback) async {
    await CaseReviewAPIMgr().wsOnLoad(
      context: context,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final caseReviewModelList =
                  model.responseData.caseDocumentReviewModelList;
              final caseMortgageAgreementReviewInfosList =
                  model.responseData.caseMortgageAgreementReviewInfosList;

              for (MortgageCaseRecomendationInfos info in caseReviewModelList) {
                listUserList.add(new DashBoardListType(
                    type: "caseReviewModelList",
                    taskID: info.taskId.toString()));
              }

              for (MortgageCaseInfos info2
                  in caseMortgageAgreementReviewInfosList) {
                listUserList.add(new DashBoardListType(
                    type: "caseMortgageAgreementReviewInfosList",
                    taskID: info2.taskId.toString()));
              }

              callback(listUserList);
            }
          } catch (e) {}
        }
      },
    );
  }

  userNotesAPI(context, Function(List<DashBoardListType>) callback,
      Function(List<UserNotesModel>) callbackListUserNotes) async {
    await UserCaseAPIMgr().wsUserNotesAPI(
      context: context,
      status: UserNotesCfg.PENDING,
      callback: (model) {
        if (model != null) {
          try {
            if (model.success) {
              final List<DashBoardListType> listUserList = [];
              final listUserNotesModel = model.responseData.userNotes;
              if (listUserNotesModel.length > 0) {
                for (UserNotesModel userNotesModel in listUserNotesModel) {
                  listUserList.add(new DashBoardListType(
                      type: "UserNotes",
                      taskID: userNotesModel.entityId.toString(),
                      title: userNotesModel.title,
                      initiatorName: userNotesModel.initiatorName,
                      comments: userNotesModel.comments));
                }
                callback(listUserList);
              }
            }
          } catch (e) {}
        }
      },
    );
  }

  drawActionRequiredItems({
    BuildContext context,
    getColor,
    List<DashBoardListType> listUserList,
    Function(Map<String, dynamic>) callback,
    Function(List<UserNotesModel>) callbackListUserNotes,
    Function callbackReload,
  }) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;
    final actionReqControler = Get.put(ActionReqController());

    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: Container(
          width: width,
          child: Column(
            children: [
              Container(
                width: width,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Txt(
                        txt: "Action Required",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .2,
                        txtAlign: TextAlign.left,
                        isBold: false,
                      ),
                    ),
                    IconButton(
                        onPressed: () {
                          callbackReload();
                        },
                        icon: Icon(
                          Icons.refresh,
                          color: Colors.black,
                        )),
                  ],
                ),
              ),
              ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount:
                    listUserList.length > 3 && !actionReqControler.expanad.value
                        ? 3
                        : listUserList.length,
                itemBuilder: (context, index) {
                  actionReqControler.isCaseChecked.obs.value.add(false);
                  if (listUserList[index].type == "email") {
                    return Obx(() => Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: GestureDetector(
                            onTap: () {
                              actionReqControler
                                      .isCaseChecked.obs.value[index] =
                                  !actionReqControler
                                      .isCaseChecked.obs.value[index];
                              if (actionReqControler
                                  .isCaseChecked.obs.value[index]) {
                                showEmailConfirmDialog(context, index,
                                    actionReqControler, callback);
                              }
                            },
                            child: Container(
                              color: Colors.transparent,
                              width: width,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 24,
                                    height: 24,
                                    child: Checkbox(
                                      fillColor:
                                          MaterialStateProperty.resolveWith(
                                              getColor),
                                      focusColor: Colors.red,
                                      checkColor: Colors.white,
                                      value: actionReqControler
                                          .isCaseChecked.obs.value[index],
                                      onChanged: (value) {
                                        actionReqControler.isCaseChecked.obs
                                            .value[index] = value;
                                        if (value) {
                                          showEmailConfirmDialog(context, index,
                                              actionReqControler, callback);
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Txt(
                                    txt: "Email verification",
                                    txtColor:
                                        MyTheme.timelinePostCallerNameColor,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: false,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ));
                  } else if (listUserList[index].type == "eid") {
                    return Obx(() => Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: GestureDetector(
                            onTap: () {
                              actionReqControler
                                      .isCaseChecked.obs.value[index] =
                                  !actionReqControler
                                      .isCaseChecked.obs.value[index];
                              if (actionReqControler
                                  .isCaseChecked.obs.value[index]) {
                                showEIDConfirmDialog(context, index,
                                    actionReqControler, callback);
                              }
                            },
                            child: Container(
                              color: Colors.transparent,
                              width: width,
                              child: Row(
                                children: [
                                  SizedBox(
                                    height: 24,
                                    width: 24,
                                    child: Checkbox(
                                      fillColor:
                                          MaterialStateProperty.resolveWith(
                                              getColor),
                                      focusColor: Colors.red,
                                      checkColor: Colors.white,
                                      value: actionReqControler
                                          .isCaseChecked.obs.value[index],
                                      onChanged: (value) {
                                        actionReqControler.isCaseChecked.obs
                                            .value[index] = value;
                                        if (value) {
                                          showEIDConfirmDialog(context, index,
                                              actionReqControler, callback);
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Txt(
                                    txt: "E-ID verification",
                                    txtColor:
                                        MyTheme.timelinePostCallerNameColor,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: false,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ));
                  } else if (listUserList[index].type ==
                      "caseReviewModelList") {
                    return Obx(
                      () => Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: GestureDetector(
                          onTap: () {
                            actionReqControler.isCaseChecked.obs.value[index] =
                                !actionReqControler
                                    .isCaseChecked.obs.value[index];
                            if (actionReqControler
                                .isCaseChecked.obs.value[index]) {
                              deleteCaseReview(context, listUserList, index);
                            }
                          },
                          child: Container(
                            color: Colors.transparent,
                            width: width,
                            child: Row(
                              children: [
                                SizedBox(
                                  height: 24,
                                  width: 24,
                                  child: Checkbox(
                                    fillColor:
                                        MaterialStateProperty.resolveWith(
                                            getColor),
                                    focusColor: Colors.red,
                                    checkColor: Colors.white,
                                    value: actionReqControler
                                        .isCaseChecked.obs.value[index],
                                    onChanged: (value) {
                                      actionReqControler.isCaseChecked.obs
                                          .value[index] = value;
                                      if (value) {
                                        deleteCaseReview(
                                            context, listUserList, index);
                                      }
                                    },
                                  ),
                                ),
                                SizedBox(width: 8),
                                Expanded(
                                  child: new RichText(
                                    text: new TextSpan(
                                      style: new TextStyle(
                                        fontSize: MyTheme.txtSize - .1,
                                        color: Colors.black,
                                      ),
                                      children: <TextSpan>[
                                        new TextSpan(
                                            text: 'Case no:',
                                            style: new TextStyle(
                                                fontSize: ResponsiveFlutter.of(
                                                        context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                        new TextSpan(
                                            text:
                                                ' ${listUserList[index].taskID},',
                                            style: new TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.blueAccent,
                                                fontSize: ResponsiveFlutter.of(
                                                        context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                        new TextSpan(
                                            text:
                                                ' You have NOT Signed the document yet.',
                                            style: new TextStyle(
                                                fontSize: ResponsiveFlutter.of(
                                                        context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                        new TextSpan(
                                            text: 'Click here ',
                                            style: new TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.blueAccent,
                                                fontSize: ResponsiveFlutter.of(
                                                        context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                        new TextSpan(
                                            text: 'to review',
                                            style: new TextStyle(
                                                fontWeight: FontWeight.normal,
                                                fontSize: ResponsiveFlutter.of(
                                                        context)
                                                    .fontSize(
                                                        MyTheme.txtSize - .1))),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    );
                  } else if (listUserList[index].type == "UserNotes") {
                    return Obx(() => Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: GestureDetector(
                            onTap: () {
                              actionReqControler
                                      .isCaseChecked.obs.value[index] =
                                  !actionReqControler
                                      .isCaseChecked.obs.value[index];
                              if (actionReqControler
                                  .isCaseChecked.obs.value[index]) {
                                taskDetailsDialogUserNotes(context,
                                    listUserList, index, callbackListUserNotes);
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.only(bottom: 8.0),
                              child: Row(
                                children: [
                                  SizedBox(
                                    height: 24,
                                    width: 24,
                                    child: Checkbox(
                                      fillColor:
                                          MaterialStateProperty.resolveWith(
                                              getColor),
                                      focusColor: Colors.red,
                                      checkColor: Colors.white,
                                      value: actionReqControler
                                          .isCaseChecked.obs.value[index],
                                      onChanged: (value) {
                                        actionReqControler.isCaseChecked.obs
                                            .value[index] = value;
                                        if (value) {
                                          taskDetailsDialogUserNotes(
                                              context,
                                              listUserList,
                                              index,
                                              callbackListUserNotes);
                                        }
                                      },
                                    ),
                                  ),
                                  SizedBox(
                                    width: 8,
                                  ),
                                  Expanded(
                                    child: new RichText(
                                      text: new TextSpan(
                                        style: new TextStyle(
                                          fontSize: MyTheme.txtSize - .1,
                                          color: Colors.black,
                                        ),
                                        children: <TextSpan>[
                                          new TextSpan(
                                              text: 'Assign Task: ',
                                              style: new TextStyle(
                                                  fontSize:
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .fontSize(
                                                              MyTheme.txtSize -
                                                                  .1))),
                                          new TextSpan(
                                              text:
                                                  '${listUserList[index].title}',
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize:
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .fontSize(
                                                              MyTheme.txtSize -
                                                                  .1))),
                                          new TextSpan(
                                              text: ' by ',
                                              style: new TextStyle(
                                                  fontSize:
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .fontSize(
                                                              MyTheme.txtSize -
                                                                  .1))),
                                          new TextSpan(
                                              text:
                                                  '${listUserList[index].initiatorName}.',
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize:
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .fontSize(
                                                              MyTheme.txtSize -
                                                                  .1))),
                                          new TextSpan(
                                              text: 'Click here',
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.blueAccent,
                                                  fontSize:
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .fontSize(
                                                              MyTheme.txtSize -
                                                                  .1))),
                                          new TextSpan(
                                              text: ' to complete it.',
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.normal,
                                                  fontSize:
                                                      ResponsiveFlutter.of(
                                                              context)
                                                          .fontSize(
                                                              MyTheme.txtSize -
                                                                  .1))),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ));
                  } else {
                    return Container(height: 20);
                  }
                },
              ),
              listUserList.length > 3
                  ? GestureDetector(
                      onTap: () {
                        if (actionReqControler.expanad.value) {
                          actionReqControler.expanad.value = false;
                        } else {
                          actionReqControler.expanad.value = true;
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Txt(
                              txt: actionReqControler.expanad.value
                                  ? "Show less"
                                  : "Show more",
                              txtColor: MyTheme.redColor,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.right,
                              isBold: false),
                          Icon(
                            actionReqControler.expanad.value
                                ? Icons.arrow_drop_up
                                : Icons.arrow_drop_down,
                            color: MyTheme.redColor,
                          )
                        ],
                      ))
                  : SizedBox()
            ],
          )),
    );
  }

  void callSendEmailAPi({BuildContext context, Function(bool) callback}) {
    BadgeAPIMgr().wsPostEmailBadge(
      context: context,
      callback: (BadgeEmailAPIModel model) {
        if (model.success) {
          callback(true);
        } else {
          callback(false);
        }
        Navigator.of(context, rootNavigator: true).pop();
      },
    );
  }

  void showEmailConfirmDialog(
      BuildContext context,
      int index,
      ActionReqController actionReqControler,
      Function(Map<String, dynamic>) callback) {
    DeleteConfirmationAlertDialog(
        alertBody: "Press \'Yes\' to verify your email.",
        alertTitle: "Email Verification !",
        deleteTxt: "Yes",
        cancelTxt: "No",
        deleteClick: () {
          actionReqControler.isCaseChecked.obs.value[index] = true;
          callSendEmailAPi(callback: (val) {
            callback({"email": val});
          });
          Navigator.of(context, rootNavigator: true).pop();
        },
        cancelClick: () {
          actionReqControler.isCaseChecked.obs.value[index] = false;
          callback({"email": null});
          Navigator.of(context, rootNavigator: true).pop();
        },
        context: context);
  }

  void showEIDConfirmDialog(
      BuildContext context,
      int index,
      ActionReqController actionReqControler,
      Function(Map<String, dynamic>) callback) {
    DeleteConfirmationAlertDialog(
        alertBody: "Press \'Yes\' to verify E-ID.",
        alertTitle: "E-ID Verification !",
        deleteTxt: "Yes",
        cancelTxt: "No",
        deleteClick: () {
          Navigator.of(context, rootNavigator: true).pop();
          actionReqControler.isCaseChecked.obs.value[index] = true;
          callback({"eid": true});
        },
        cancelClick: () {
          Navigator.of(context, rootNavigator: true).pop();
          actionReqControler.isCaseChecked.obs.value[index] = false;
          callback({"eid": false});
        },
        context: context);
  }

  deleteCaseReview(context, listUserList, index) {
    final actionReqControler = Get.put(ActionReqController());
    DeleteConfirmationAlertDialog(
        deleteClick: () {
          Navigator.of(context, rootNavigator: true).pop();

          String url = CaseDigitalSignByCustomerURLHelper.getUrl(
              caseID: listUserList[index].taskID.toString());

          Get.to(
            () => WebScreen(
              caseID: listUserList[index].taskID.toString(),
              title: "Case ${listUserList[index].taskID} Signature",
              // url: Server.CaseDigitalSignByCustomer + caseReviewModelList[index].taskId.toString(),
              url: url,
            ),
          );

          actionReqControler.isCaseChecked.obs.value[index] = false;
        },
        cancelClick: () {
          Navigator.of(context, rootNavigator: true).pop();
          actionReqControler.isCaseChecked.obs.value[index] = false;
        },
        deleteTxt: "Yes",
        cancelTxt: "No",
        context: context,
        alertBody: "Do you want to signature now ?",
        alertTitle: "Case ${listUserList[index].taskID}");
  }

  taskDetailsDialogUserNotes(context, listUserList, index,
      Function(List<UserNotesModel>) callbackListUserNotes) {
    final actionReqControler = Get.put(ActionReqController());
    TaskDetailsAlertDialog(
      caseID: listUserList[index].taskID.toString(),
      caseTitle: listUserList[index].title,
      caseDescription: listUserList[index].comments,
      caseAssignedBy: listUserList[index].initiatorName,
      deleteClick: () {
        Navigator.of(context, rootNavigator: true).pop();
        final userNotesModel = NewCaseHelper().getUserNotesModelByTitle(
            listUserNotesModel: callbackListUserNotes,
            title: listUserList[index].title);
        if (userNotesModel != null) {
          //  api call
          UserCaseAPIMgr().wsCaseNoteDone(
            context: context,
            userNoteModel: userNotesModel,
            callback: (model) {
              if (model != null) {
                if (model.success) {}
              }
            },
          );
        }
      },
      cancelClick: () {
        Navigator.of(context, rootNavigator: true).pop();
        actionReqControler.isCaseChecked.obs.value[index] = false;
      },
      deleteTxt: "Yes",
      cancelTxt: "No",
      context: context,
      alertBody:
          "For Completing the task, please click the \'Yes\' button otherwise click the \'No\' button",
      alertTitle: "Task Details",
    );
  }
}
