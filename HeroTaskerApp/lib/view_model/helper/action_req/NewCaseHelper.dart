import 'package:aitl/config/cfg/NewCaseCfg.dart';
import 'package:aitl/config/server/APIActionReqCfg.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/ads-on/action_req/UserNotesModel.dart';

class NewCaseHelper {
  getUrl({pageStart, pageCount, status}) {
    var url = APIActionReqCfg.NEWCASE_URL;
    url = url.replaceAll("#userId#", userData.userModel.id.toString());
    url = url.replaceAll("#page#", pageStart.toString());
    url = url.replaceAll("#count#", pageCount.toString());
    url = url.replaceAll("#status#", status);
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());
    return url;
  }

  getCreateCaseIconByTitle(String title) {
    try {
      for (var map in NewCaseCfg.listCreateNewCase) {
        final title2 = map['title'];
        if (title2.toLowerCase() == title.toLowerCase()) {
          return map["url"];
        }
      }
    } catch (e) {}
    return null;
  }

  getCaseByTitle(String title) {
    try {
      for (var map in NewCaseCfg.listCreateNewCase) {
        final title2 = map['title'];
        if (title2.toLowerCase() == title.toLowerCase()) {
          return map;
        }
      }
    } catch (e) {}
    return null;
  }

  getUserNotesModelByTitle({listUserNotesModel, String title}) {
    for (UserNotesModel userNotesModel in listUserNotesModel) {
      if (userNotesModel.title == title) {
        return userNotesModel;
      }
    }
    return null;
  }
}
