import 'package:get/get.dart';

class RadioSelController extends GetxController {
  var radioIndex = 0.obs;
  setSelectedIndex({var selectValue}) {
    radioIndex.value = selectValue;
  }
}
