import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/view/dashboard/dashboard_page.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';

import 'view/widgets/dialog/AlrtDialog.dart';
import 'view/widgets/dialog/ConfirmDialog.dart';

mixin Mixin {
  getW(context) {
    return ResponsiveFlutter.of(context).wp(100);
  }

  getH(context) {
    return ResponsiveFlutter.of(context).hp(100);
  }

  getWP(context, p) {
    return ResponsiveFlutter.of(context).wp(p);
  }

  getHP(context, p) {
    return ResponsiveFlutter.of(context).hp(p);
  }

  getTxtSize({BuildContext context, double txtSize = 0}) {
    if (txtSize == 0) txtSize = 2;
    return ResponsiveFlutter.of(context).fontSize(txtSize);
  }

  obsUpdateTabs(int pageNo) async {
    switch (pageNo) {
      case 0:
        StateProvider()
            .notify(ObserverState.STATE_RELOAD_TAB, DashboardPage.TAB_MYTASK);
        break;
      case 4:
        StateProvider()
            .notify(ObserverState.STATE_RELOAD_TAB, DashboardPage.TAB_MYTASK);
        StateProvider().notify(
            ObserverState.STATE_RELOAD_TAB, DashboardPage.TAB_FINDWORKS);
        break;
      default:
        break;
    }
  }

  /*navTo({
    BuildContext context,
    Widget Function() page,
    isRep = false,
    isFullscreenDialog = false,
  }) {
    if (!isRep) {
      return Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) {
            return page();
          },
          fullscreenDialog: isFullscreenDialog));
    } else {
      return Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) {
            return page();
          },
          fullscreenDialog: isFullscreenDialog));
    }
  }*/

  openUrl(context, url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      Alert(context: context, title: "Alert", desc: 'Could not launch $url')
          .show();
    }
  }

  openMap(context, double latitude, double longitude, String title) async {
    String googleUrl =
        'https://www.google.com/maps/search/?api=1&query=$latitude,$longitude';
    if (await canLaunch(googleUrl)) {
      await launch(googleUrl);
    } else {
      Alert(
              context: context,
              title: "Alert",
              desc: 'Could not launch $googleUrl')
          .show();
    }
  }

  Iterable<E> mapIndexed<E, T>(
      Iterable<T> items, E Function(int index, T item) f) sync* {
    var index = 0;
    for (final item in items) {
      yield f(index, item);
      index = index + 1;
    }
  }

  void startLoading() {
    EasyLoading.show(status: "Loading...");
  }

  void stopLoading() {
    EasyLoading.dismiss();
  }

  void showAlert({String msg}) {
    Get.dialog(AlrtDialog(
      which: 2,
      msg: msg,
      txtColor: Colors.black,
      bgColor: MyTheme.gray1Color,
    ));
  }

  void showSnake(String title, String msg) {
    Get.snackbar(title, msg);
  }

  void showToast(
      {@required BuildContext context, @required String msg, int which = 2}) {
    FToast fToast;
    fToast = FToast();
    fToast.init(context);
    Widget toast = Card(
        elevation: 50,
        color: Colors.transparent,
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 12.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: MyTheme.redColor),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                (which == 1)
                    ? Icon(Icons.check, color: Colors.green, size: 50)
                    : (which == 0)
                        ? Icon(Icons.error, color: Colors.red, size: 50)
                        : Icon(Icons.info, color: Colors.orange, size: 50),
                SizedBox(height: 10),
                Text(msg,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.normal)),
              ],
            ),
          ),
        ));
    fToast.showToast(
      child: toast,
      gravity: ToastGravity.CENTER,
      toastDuration: Duration(seconds: 3),
    );
  }

  showToolTips({BuildContext context, String txt = "", Widget w}) {
    return showDialog(
      context: context,
      builder: (context) => AlertDialog(
        content: (w == null)
            ? Text(
                txt,
                softWrap: true,
                style: TextStyle(color: Colors.white),
              )
            : w,
      ),
    );
    //key.currentState.ensureTooltipVisible();
  }

  showConfirmDialog(
      {BuildContext context,
      String title,
      String msg = "",
      int which = 2,
      Function callback}) {
    if (title == null) {
      if (which == 0) {
        title = "Error";
      } else if (which == 1) {
        title = "Success";
      } else if (which == 2) {
        title = "Confirmation";
      }
    }
    Get.dialog(
      confirmDialog(
        context: context,
        title: title,
        msg: msg,
        which: which,
        callback: () {
          callback();
        },
      ),
    );
  }

  log(str) {
    if (const String.fromEnvironment('DEBUG') != null) {
      print(str);
      //final pattern = new RegExp('.{1,800}'); // 800 is the size of each chunk
      //pattern.allMatches(str).forEach((match) => print(match.group(0)));
    }
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

extension CapExtension on String {
  String get inCaps =>
      this.length > 0 ? '${this[0].toUpperCase()}${this.substring(1)}' : '';
  String get allInCaps => this.toUpperCase();
  String get capitalizeFirstofEach => this.toLowerCase().split(' ').map((word) {
        String leftText =
            (word.length > 1) ? word.substring(1, word.length) : '';
        return word[0].toUpperCase() + leftText;
      }).join(' ');
}
