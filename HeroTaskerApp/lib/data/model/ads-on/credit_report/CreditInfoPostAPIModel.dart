import 'CreditInfoPostResponse.dart';

class CreditInfoPostAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  CreditInfoPostResponseData responseData;

  CreditInfoPostAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory CreditInfoPostAPIModel.fromJson(Map<String, dynamic> j) {
    return CreditInfoPostAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? CreditInfoPostResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}
