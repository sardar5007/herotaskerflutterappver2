class GetSimulatedScoreUseridAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetSimulatedScoreUseridAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetSimulatedScoreUseridAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  SimulatedScore simulatedScore;
  String simulatedScoreUpdateDate;

  ResponseData({this.simulatedScore, this.simulatedScoreUpdateDate});

  ResponseData.fromJson(Map<String, dynamic> json) {
    simulatedScore = json['SimulatedScore'] != null
        ? new SimulatedScore.fromJson(json['SimulatedScore'])
        : null;
    simulatedScoreUpdateDate = json['SimulatedScoreUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.simulatedScore != null) {
      data['SimulatedScore'] = this.simulatedScore.toJson();
    }
    data['SimulatedScoreUpdateDate'] = this.simulatedScoreUpdateDate;
    return data;
  }
}

class SimulatedScore {
  String message;
  String status;
  int simulatedScore;

  SimulatedScore({this.message, this.status, this.simulatedScore});

  SimulatedScore.fromJson(Map<String, dynamic> json) {
    message = json['Message'];
    status = json['Status'];
    simulatedScore = json['SimulatedScore'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Message'] = this.message;
    data['Status'] = this.status;
    data['SimulatedScore'] = this.simulatedScore;
    return data;
  }
}
