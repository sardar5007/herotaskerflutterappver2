import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/server/ServerUrls.dart';
import 'package:aitl/data/app_data/UserData.dart';

class PrivacyPolicyHelper {
  getUrl() {
    var url = ServerUrls.TERMS_POLICY;
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());
    return url;
  }
}
