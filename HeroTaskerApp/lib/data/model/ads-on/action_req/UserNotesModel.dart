class UserNotesModel {
  int userId;
  String user;
  dynamic status;
  String creationDate;
  String title;
  String comments;
  String serviceDate;
  bool isTag;
  int initiatorId;
  String initiatorName;
  String ownerName;
  String ownerEmail;
  String profileImageUrl;
  String type;
  int leadId;
  String priority;
  String category;
  String complainMode;
  String noteCategory;
  String serviceEndDate;
  int adviserId;
  String adviserName;
  String directionType;
  String resolutionsUrl;
  int entityId;
  String entityName;
  int id;

  UserNotesModel(
      {this.userId,
      this.user,
      this.status,
      this.creationDate,
      this.title,
      this.comments,
      this.serviceDate,
      this.isTag,
      this.initiatorId,
      this.initiatorName,
      this.ownerName,
      this.ownerEmail,
      this.profileImageUrl,
      this.type,
      this.leadId,
      this.priority,
      this.category,
      this.complainMode,
      this.noteCategory,
      this.serviceEndDate,
      this.adviserId,
      this.adviserName,
      this.directionType,
      this.resolutionsUrl,
      this.entityId,
      this.entityName,
      this.id});

  UserNotesModel.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'] ?? '';
    status = json['Status'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    title = json['Title'] ?? '';
    comments = json['Comments'] ?? '';
    serviceDate = json['ServiceDate'] ?? '';
    isTag = json['IsTag'] ?? false;
    initiatorId = json['InitiatorId'] ?? 0;
    initiatorName = json['InitiatorName'] ?? '';
    ownerName = json['OwnerName'] ?? '';
    ownerEmail = json['OwnerEmail'] ?? '';
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    type = json['Type'] ?? '';
    leadId = json['LeadId'] ?? 0;
    priority = json['Priority'] ?? '';
    category = json['Category'] ?? '';
    complainMode = json['ComplainMode'] ?? '';
    noteCategory = json['NoteCategory'] ?? '';
    serviceEndDate = json['ServiceEndDate'] ?? '';
    adviserId = json['AdviserId'] ?? 0;
    adviserName = json['AdviserName'] ?? '';
    directionType = json['DirectionType'] ?? '';
    resolutionsUrl = json['ResolutionsUrl'] ?? '';
    entityId = json['EntityId'] ?? 0;
    entityName = json['EntityName'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['Title'] = this.title;
    data['Comments'] = this.comments;
    data['ServiceDate'] = this.serviceDate;
    data['IsTag'] = this.isTag;
    data['InitiatorId'] = this.initiatorId;
    data['InitiatorName'] = this.initiatorName;
    data['OwnerName'] = this.ownerName;
    data['OwnerEmail'] = this.ownerEmail;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['Type'] = this.type;
    data['LeadId'] = this.leadId;
    data['Priority'] = this.priority;
    data['Category'] = this.category;
    data['ComplainMode'] = this.complainMode;
    data['NoteCategory'] = this.noteCategory;
    data['ServiceEndDate'] = this.serviceEndDate;
    data['AdviserId'] = this.adviserId;
    data['AdviserName'] = this.adviserName;
    data['DirectionType'] = this.directionType;
    data['ResolutionsUrl'] = this.resolutionsUrl;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['Id'] = this.id;
    return data;
  }
}
