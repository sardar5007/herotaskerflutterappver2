class TaskUserRatingModel {
  String creationDate;
  int employeeId;
  String initiatorName;
  bool isPoster;
  double rating;
  int status;
  int taskBiddingId;
  String taskTitle;
  int userId;
  int id;

  TaskUserRatingModel(
      {this.creationDate,
      this.employeeId,
      this.initiatorName,
      this.isPoster,
      this.rating,
      this.status,
      this.taskBiddingId,
      this.taskTitle,
      this.userId,
      this.id});

  TaskUserRatingModel.fromJson(Map<String, dynamic> json) {
    creationDate = json['CreationDate'] ?? '';
    employeeId = json['EmployeeId'] ?? 0;
    initiatorName = json['InitiatorName'] ?? '';
    isPoster = json['IsPoster'] ?? false;
    rating = json['Rating'] ?? 0.0;
    status = json['Status'] ?? 0;
    taskBiddingId = json['TaskBiddingId'] ?? 0;
    taskTitle = json['TaskTitle'] ?? '';
    userId = json['UserId'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['CreationDate'] = this.creationDate;
    data['EmployeeId'] = this.employeeId;
    data['InitiatorName'] = this.initiatorName;
    data['IsPoster'] = this.isPoster;
    data['Rating'] = this.rating;
    data['Status'] = this.status;
    data['TaskBiddingId'] = this.taskBiddingId;
    data['TaskTitle'] = this.taskTitle;
    data['UserId'] = this.userId;
    data['Id'] = this.id;
    return data;
  }
}
