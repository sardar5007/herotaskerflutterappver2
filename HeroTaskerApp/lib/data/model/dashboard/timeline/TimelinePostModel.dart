import 'comments/UserCommentPublicModelList.dart';

class TimelinePostModel {
  String additionalAttributeValue;
  bool canDelete;
  String dateCreated;
  String dateCreatedUtc;
  String dateUpdated;
  String dateUpdatedUtc;
  double fromLat;
  double fromLng;
  bool isOwner;
  bool isPrivate;
  bool isSponsored;
  int likeStatus;
  String ownerEntityType;
  int ownerId;
  String ownerImageUrl;
  String ownerName;
  String ownerProfileUrl;
  String postTypeName;
  String publishDate;
  String publishDateUtc;
  int id;
  int totalComments;
  int totalLikes;
  List<UserCommentPublicModelList> userCommentPublicModelList;
  String message;
  String checkin;
  int receiverId;

  TimelinePostModel({
    this.additionalAttributeValue,
    this.canDelete,
    this.dateCreated,
    this.dateCreatedUtc,
    this.dateUpdated,
    this.dateUpdatedUtc,
    this.fromLat,
    this.fromLng,
    this.isOwner,
    this.isPrivate,
    this.isSponsored,
    this.likeStatus,
    this.ownerEntityType,
    this.ownerId,
    this.ownerImageUrl,
    this.ownerName,
    this.ownerProfileUrl,
    this.postTypeName,
    this.publishDate,
    this.publishDateUtc,
    this.id,
    this.totalComments,
    this.totalLikes,
    this.userCommentPublicModelList,
    this.message,
    this.checkin,
    this.receiverId,
  });

  TimelinePostModel.fromJson(Map<String, dynamic> json) {
    additionalAttributeValue = json['AdditionalAttributeValue'] ?? '';
    canDelete = json['CanDelete'] ?? false;
    dateCreated = json['DateCreated'] ?? '';
    dateCreatedUtc = json['DateCreatedUtc'] ?? '';
    dateUpdated = json['DateUpdated'] ?? '';
    dateUpdatedUtc = json['DateUpdatedUtc'] ?? '';
    fromLat = json['FromLat'] ?? 0.0;
    fromLng = json['FromLng'] ?? 0.0;
    isOwner = json['IsOwner'] ?? false;
    isPrivate = json['IsPrivate'] ?? false;
    isSponsored = json['IsSponsored'] ?? false;
    likeStatus = json['LikeStatus'] ?? 0;
    ownerEntityType = json['OwnerEntityType'] ?? '';
    ownerId = json['OwnerId'] ?? 0;
    ownerImageUrl = json['OwnerImageUrl'] ?? '';
    ownerName = json['OwnerName'] ?? '';
    ownerProfileUrl = json['OwnerProfileUrl'] ?? '';
    postTypeName = json['PostTypeName'] ?? '';
    publishDate = json['PublishDate'] ?? '';
    publishDateUtc = json['PublishDateUtc'] ?? '';
    id = json['Id'] ?? 0;
    totalComments = json['TotalComments'] ?? '';
    totalLikes = json['TotalLikes'] ?? 0;
    if (json['UserCommentPublicModelList'] != null) {
      userCommentPublicModelList = [];
      json['UserCommentPublicModelList'].forEach((v) {
        userCommentPublicModelList
            .add(new UserCommentPublicModelList.fromJson(v));
      });
    }
    message = json['Message'];
    checkin = json['Checkin'] ?? '';
    receiverId = json['ReceiverId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['AdditionalAttributeValue'] = this.additionalAttributeValue;
    data['CanDelete'] = this.canDelete;
    data['DateCreated'] = this.dateCreated;
    data['DateCreatedUtc'] = this.dateCreatedUtc;
    data['DateUpdated'] = this.dateUpdated;
    data['DateUpdatedUtc'] = this.dateUpdatedUtc;
    data['FromLat'] = this.fromLat;
    data['FromLng'] = this.fromLng;
    data['IsOwner'] = this.isOwner;
    data['IsPrivate'] = this.isPrivate;
    data['IsSponsored'] = this.isSponsored;
    data['LikeStatus'] = this.likeStatus;
    data['OwnerEntityType'] = this.ownerEntityType;
    data['OwnerId'] = this.ownerId;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['OwnerName'] = this.ownerName;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['PostTypeName'] = this.postTypeName;
    data['PublishDate'] = this.publishDate;
    data['PublishDateUtc'] = this.publishDateUtc;
    data['Id'] = this.id;
    data['TotalComments'] = this.totalComments;
    data['TotalLikes'] = this.totalLikes;
    if (this.userCommentPublicModelList != null) {
      data['UserCommentPublicModelList'] =
          this.userCommentPublicModelList.map((v) => v.toJson()).toList();
    }
    data['Message'] = this.message;
    data['Checkin'] = this.checkin;
    data['ReceiverId'] = this.receiverId;

    return data;
  }
}
