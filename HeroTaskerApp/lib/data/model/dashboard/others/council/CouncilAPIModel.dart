import 'package:aitl/data/model/dashboard/user/PublicUserModel.dart';

class CouncilGetAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  CouncilGetAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  CouncilGetAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<UserCompanyInfos> userCompanyInfos;

  ResponseData({this.userCompanyInfos});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserCompanyInfos'] != null) {
      userCompanyInfos = [];
      json['UserCompanyInfos'].forEach((v) {
        userCompanyInfos.add(new UserCompanyInfos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userCompanyInfos != null) {
      data['UserCompanyInfos'] =
          this.userCompanyInfos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class UserCompanyInfos {
  User user;
  int userId;
  String pointOfContactName;
  String pointOfContactNumber;
  String pointOfContactDesignation;
  String incorporationNumber;
  String companyType;
  String companyBusinessType;
  String companyRegistrationDateAndYear;
  String howManyEmployees;
  String howManyBranches;
  String officePhoneNumber;
  String website;
  String remarks;
  String location;
  String mainProducts;
  String ownership;
  String totalAnnualRevenue;
  String yearEstablished;
  String certifications;
  String productCertifications;
  String patents;
  String trademarks;
  String mainMarkets;
  String numberOfEmployeeResearchDevelopment;
  String aboutOurCompany;
  String companyName;
  String officeSize;
  String companyAdvantages;
  String companyOperationalStreet;
  String companyOperationalCity;
  String companyOperationalCountry;
  String companyOperationalZip;
  String otherProductsYouSell;
  double mainMarketsandDistributionPercentage;
  double totalMainMarketsandDistributionPercentage;
  int responseRate;
  int lastSixMonthTransactionAmount;
  int lastSixMonthTransactionCount;
  String facebookUrl;
  String twitterUrl;
  String linkedinUrl;
  int facebookPageId;
  String youtubeUrl;
  String contactEmail;
  String contactPhoneNumber;
  String contactAddress;
  String contactGoogleMapLocation;
  String officeFax;
  String reportFooter;
  String officeEmail;
  String reportLogo;
  String reportLocation;
  String companyTitleUrl;
  String emailFooter;
  String reportMobileNumber;
  String reportLogoUrl;
  String instagramUrl;
  String userName;
  String userEmail;
  String userMobileNumber;
  String comunityName;
  int userAccesType;
  int maximumNumberOfAdviser;
  String emailAddress;
  String fCANumber;
  String iCORegistrationNumber;
  String authorizationStatus;
  String nameOfPrincipal;
  String principalFCANumber;
  String principalAddress;
  String principalTelehone;
  String principalEmailAddress;
  String isActiveCustomerPrivacy;
  String isActiveCustomerAgreement;
  String isIpipelineActivation;
  String isEmailSending;
  String recommendationEmail;
  String caseUpdatesEmail;
  String suitabilityLettersEmail;
  String caseRemindersEmail;
  String userUpdatesEmail;
  String helpfulInformationEmail;
  String generalEmail;
  String newslettersAndMarketingEmail;
  int entityId;
  String entityName;
  String companyWebsite;
  String leadUpdatesEmail;
  int companyTypeId;
  int id;

  UserCompanyInfos(
      {this.user,
      this.userId,
      this.pointOfContactName,
      this.pointOfContactNumber,
      this.pointOfContactDesignation,
      this.incorporationNumber,
      this.companyType,
      this.companyBusinessType,
      this.companyRegistrationDateAndYear,
      this.howManyEmployees,
      this.howManyBranches,
      this.officePhoneNumber,
      this.website,
      this.remarks,
      this.location,
      this.mainProducts,
      this.ownership,
      this.totalAnnualRevenue,
      this.yearEstablished,
      this.certifications,
      this.productCertifications,
      this.patents,
      this.trademarks,
      this.mainMarkets,
      this.numberOfEmployeeResearchDevelopment,
      this.aboutOurCompany,
      this.companyName,
      this.officeSize,
      this.companyAdvantages,
      this.companyOperationalStreet,
      this.companyOperationalCity,
      this.companyOperationalCountry,
      this.companyOperationalZip,
      this.otherProductsYouSell,
      this.mainMarketsandDistributionPercentage,
      this.totalMainMarketsandDistributionPercentage,
      this.responseRate,
      this.lastSixMonthTransactionAmount,
      this.lastSixMonthTransactionCount,
      this.facebookUrl,
      this.twitterUrl,
      this.linkedinUrl,
      this.facebookPageId,
      this.youtubeUrl,
      this.contactEmail,
      this.contactPhoneNumber,
      this.contactAddress,
      this.contactGoogleMapLocation,
      this.officeFax,
      this.reportFooter,
      this.officeEmail,
      this.reportLogo,
      this.reportLocation,
      this.companyTitleUrl,
      this.emailFooter,
      this.reportMobileNumber,
      this.reportLogoUrl,
      this.instagramUrl,
      this.userName,
      this.userEmail,
      this.userMobileNumber,
      this.comunityName,
      this.userAccesType,
      this.maximumNumberOfAdviser,
      this.emailAddress,
      this.fCANumber,
      this.iCORegistrationNumber,
      this.authorizationStatus,
      this.nameOfPrincipal,
      this.principalFCANumber,
      this.principalAddress,
      this.principalTelehone,
      this.principalEmailAddress,
      this.isActiveCustomerPrivacy,
      this.isActiveCustomerAgreement,
      this.isIpipelineActivation,
      this.isEmailSending,
      this.recommendationEmail,
      this.caseUpdatesEmail,
      this.suitabilityLettersEmail,
      this.caseRemindersEmail,
      this.userUpdatesEmail,
      this.helpfulInformationEmail,
      this.generalEmail,
      this.newslettersAndMarketingEmail,
      this.entityId,
      this.entityName,
      this.companyWebsite,
      this.leadUpdatesEmail,
      this.companyTypeId,
      this.id});

  UserCompanyInfos.fromJson(Map<String, dynamic> json) {
    user = json['User'] != null ? new User.fromJson(json['User']) : null;
    userId = json['UserId'];
    pointOfContactName = json['PointOfContactName'];
    pointOfContactNumber = json['PointOfContactNumber'];
    pointOfContactDesignation = json['PointOfContactDesignation'];
    incorporationNumber = json['IncorporationNumber'];
    companyType = json['CompanyType'];
    companyBusinessType = json['CompanyBusinessType'];
    companyRegistrationDateAndYear = json['CompanyRegistrationDateAndYear'];
    howManyEmployees = json['HowManyEmployees'];
    howManyBranches = json['HowManyBranches'];
    officePhoneNumber = json['OfficePhoneNumber'];
    website = json['Website'];
    remarks = json['Remarks'];
    location = json['Location'];
    mainProducts = json['MainProducts'];
    ownership = json['Ownership'];
    totalAnnualRevenue = json['TotalAnnualRevenue'];
    yearEstablished = json['YearEstablished'];
    certifications = json['Certifications'];
    productCertifications = json['ProductCertifications'];
    patents = json['Patents'];
    trademarks = json['Trademarks'];
    mainMarkets = json['MainMarkets'];
    numberOfEmployeeResearchDevelopment =
        json['NumberOfEmployeeResearchDevelopment'];
    aboutOurCompany = json['AboutOurCompany'];
    companyName = json['CompanyName'];
    officeSize = json['OfficeSize'];
    companyAdvantages = json['CompanyAdvantages'];
    companyOperationalStreet = json['CompanyOperationalStreet'];
    companyOperationalCity = json['CompanyOperationalCity'];
    companyOperationalCountry = json['CompanyOperationalCountry'];
    companyOperationalZip = json['CompanyOperationalZip'];
    otherProductsYouSell = json['OtherProductsYouSell'];
    mainMarketsandDistributionPercentage =
        json['MainMarketsandDistributionPercentage'];
    totalMainMarketsandDistributionPercentage =
        json['TotalMainMarketsandDistributionPercentage'];
    responseRate = json['ResponseRate'];
    lastSixMonthTransactionAmount = json['LastSixMonthTransactionAmount'];
    lastSixMonthTransactionCount = json['LastSixMonthTransactionCount'];
    facebookUrl = json['FacebookUrl'];
    twitterUrl = json['TwitterUrl'];
    linkedinUrl = json['LinkedinUrl'];
    facebookPageId = json['FacebookPageId'];
    youtubeUrl = json['YoutubeUrl'];
    contactEmail = json['ContactEmail'];
    contactPhoneNumber = json['ContactPhoneNumber'];
    contactAddress = json['ContactAddress'];
    contactGoogleMapLocation = json['ContactGoogleMapLocation'];
    officeFax = json['OfficeFax'];
    reportFooter = json['ReportFooter'];
    officeEmail = json['OfficeEmail'];
    reportLogo = json['ReportLogo'];
    reportLocation = json['ReportLocation'];
    companyTitleUrl = json['CompanyTitleUrl'];
    emailFooter = json['EmailFooter'];
    reportMobileNumber = json['ReportMobileNumber'];
    reportLogoUrl = json['ReportLogoUrl'];
    instagramUrl = json['InstagramUrl'];
    userName = json['UserName'];
    userEmail = json['UserEmail'];
    userMobileNumber = json['UserMobileNumber'];
    comunityName = json['ComunityName'];
    userAccesType = json['UserAccesType'];
    maximumNumberOfAdviser = json['MaximumNumberOfAdviser'];
    emailAddress = json['EmailAddress'];
    fCANumber = json['FCANumber'];
    iCORegistrationNumber = json['ICORegistrationNumber'];
    authorizationStatus = json['AuthorizationStatus'];
    nameOfPrincipal = json['NameOfPrincipal'];
    principalFCANumber = json['PrincipalFCANumber'];
    principalAddress = json['PrincipalAddress'];
    principalTelehone = json['PrincipalTelehone'];
    principalEmailAddress = json['PrincipalEmailAddress'];
    isActiveCustomerPrivacy = json['IsActiveCustomerPrivacy'];
    isActiveCustomerAgreement = json['IsActiveCustomerAgreement'];
    isIpipelineActivation = json['IsIpipelineActivation'];
    isEmailSending = json['IsEmailSending'];
    recommendationEmail = json['RecommendationEmail'];
    caseUpdatesEmail = json['CaseUpdatesEmail'];
    suitabilityLettersEmail = json['SuitabilityLettersEmail'];
    caseRemindersEmail = json['CaseRemindersEmail'];
    userUpdatesEmail = json['UserUpdatesEmail'];
    helpfulInformationEmail = json['HelpfulInformationEmail'];
    generalEmail = json['GeneralEmail'];
    newslettersAndMarketingEmail = json['NewslettersAndMarketingEmail'];
    entityId = json['EntityId'];
    entityName = json['EntityName'];
    companyWebsite = json['CompanyWebsite'];
    leadUpdatesEmail = json['LeadUpdatesEmail'];
    companyTypeId = json['CompanyTypeId'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    data['UserId'] = this.userId;
    data['PointOfContactName'] = this.pointOfContactName;
    data['PointOfContactNumber'] = this.pointOfContactNumber;
    data['PointOfContactDesignation'] = this.pointOfContactDesignation;
    data['IncorporationNumber'] = this.incorporationNumber;
    data['CompanyType'] = this.companyType;
    data['CompanyBusinessType'] = this.companyBusinessType;
    data['CompanyRegistrationDateAndYear'] =
        this.companyRegistrationDateAndYear;
    data['HowManyEmployees'] = this.howManyEmployees;
    data['HowManyBranches'] = this.howManyBranches;
    data['OfficePhoneNumber'] = this.officePhoneNumber;
    data['Website'] = this.website;
    data['Remarks'] = this.remarks;
    data['Location'] = this.location;
    data['MainProducts'] = this.mainProducts;
    data['Ownership'] = this.ownership;
    data['TotalAnnualRevenue'] = this.totalAnnualRevenue;
    data['YearEstablished'] = this.yearEstablished;
    data['Certifications'] = this.certifications;
    data['ProductCertifications'] = this.productCertifications;
    data['Patents'] = this.patents;
    data['Trademarks'] = this.trademarks;
    data['MainMarkets'] = this.mainMarkets;
    data['NumberOfEmployeeResearchDevelopment'] =
        this.numberOfEmployeeResearchDevelopment;
    data['AboutOurCompany'] = this.aboutOurCompany;
    data['CompanyName'] = this.companyName;
    data['OfficeSize'] = this.officeSize;
    data['CompanyAdvantages'] = this.companyAdvantages;
    data['CompanyOperationalStreet'] = this.companyOperationalStreet;
    data['CompanyOperationalCity'] = this.companyOperationalCity;
    data['CompanyOperationalCountry'] = this.companyOperationalCountry;
    data['CompanyOperationalZip'] = this.companyOperationalZip;
    data['OtherProductsYouSell'] = this.otherProductsYouSell;
    data['MainMarketsandDistributionPercentage'] =
        this.mainMarketsandDistributionPercentage;
    data['TotalMainMarketsandDistributionPercentage'] =
        this.totalMainMarketsandDistributionPercentage;
    data['ResponseRate'] = this.responseRate;
    data['LastSixMonthTransactionAmount'] = this.lastSixMonthTransactionAmount;
    data['LastSixMonthTransactionCount'] = this.lastSixMonthTransactionCount;
    data['FacebookUrl'] = this.facebookUrl;
    data['TwitterUrl'] = this.twitterUrl;
    data['LinkedinUrl'] = this.linkedinUrl;
    data['FacebookPageId'] = this.facebookPageId;
    data['YoutubeUrl'] = this.youtubeUrl;
    data['ContactEmail'] = this.contactEmail;
    data['ContactPhoneNumber'] = this.contactPhoneNumber;
    data['ContactAddress'] = this.contactAddress;
    data['ContactGoogleMapLocation'] = this.contactGoogleMapLocation;
    data['OfficeFax'] = this.officeFax;
    data['ReportFooter'] = this.reportFooter;
    data['OfficeEmail'] = this.officeEmail;
    data['ReportLogo'] = this.reportLogo;
    data['ReportLocation'] = this.reportLocation;
    data['CompanyTitleUrl'] = this.companyTitleUrl;
    data['EmailFooter'] = this.emailFooter;
    data['ReportMobileNumber'] = this.reportMobileNumber;
    data['ReportLogoUrl'] = this.reportLogoUrl;
    data['InstagramUrl'] = this.instagramUrl;
    data['UserName'] = this.userName;
    data['UserEmail'] = this.userEmail;
    data['UserMobileNumber'] = this.userMobileNumber;
    data['ComunityName'] = this.comunityName;
    data['UserAccesType'] = this.userAccesType;
    data['MaximumNumberOfAdviser'] = this.maximumNumberOfAdviser;
    data['EmailAddress'] = this.emailAddress;
    data['FCANumber'] = this.fCANumber;
    data['ICORegistrationNumber'] = this.iCORegistrationNumber;
    data['AuthorizationStatus'] = this.authorizationStatus;
    data['NameOfPrincipal'] = this.nameOfPrincipal;
    data['PrincipalFCANumber'] = this.principalFCANumber;
    data['PrincipalAddress'] = this.principalAddress;
    data['PrincipalTelehone'] = this.principalTelehone;
    data['PrincipalEmailAddress'] = this.principalEmailAddress;
    data['IsActiveCustomerPrivacy'] = this.isActiveCustomerPrivacy;
    data['IsActiveCustomerAgreement'] = this.isActiveCustomerAgreement;
    data['IsIpipelineActivation'] = this.isIpipelineActivation;
    data['IsEmailSending'] = this.isEmailSending;
    data['RecommendationEmail'] = this.recommendationEmail;
    data['CaseUpdatesEmail'] = this.caseUpdatesEmail;
    data['SuitabilityLettersEmail'] = this.suitabilityLettersEmail;
    data['CaseRemindersEmail'] = this.caseRemindersEmail;
    data['UserUpdatesEmail'] = this.userUpdatesEmail;
    data['HelpfulInformationEmail'] = this.helpfulInformationEmail;
    data['GeneralEmail'] = this.generalEmail;
    data['NewslettersAndMarketingEmail'] = this.newslettersAndMarketingEmail;
    data['EntityId'] = this.entityId;
    data['EntityName'] = this.entityName;
    data['CompanyWebsite'] = this.companyWebsite;
    data['LeadUpdatesEmail'] = this.leadUpdatesEmail;
    data['CompanyTypeId'] = this.companyTypeId;
    data['Id'] = this.id;
    return data;
  }
}
