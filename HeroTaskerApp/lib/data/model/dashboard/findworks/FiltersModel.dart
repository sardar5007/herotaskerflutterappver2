import 'dart:convert';
import 'dart:developer';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:json_annotation/json_annotation.dart';
part 'FiltersModel.g.dart';

@JsonSerializable()
class FiltersModel {
  final int tabNo;
  final String location;
  final double lat;
  final double lng;
  final int distance;
  final int minPrice;
  final int maxPrice;
  final bool isAvailableTasksOnly;

  FiltersModel({
    this.tabNo,
    this.location,
    this.lat,
    this.lng,
    this.distance,
    this.minPrice,
    this.maxPrice,
    this.isAvailableTasksOnly,
  });

  factory FiltersModel.fromJson(Map<String, dynamic> json) =>
      _$FiltersModelFromJson(json);

  Map<String, dynamic> toJson() => _$FiltersModelToJson(this);
}

class FiltersSharedPref {
  void set(FiltersModel filtersModel) async {
    try {
      final SharedPreferences pref = await SharedPreferences.getInstance();
      pref.setString('filtersData', jsonEncode(filtersModel.toJson()));
    } catch (e) {
      log(e.toString());
    }
  }

  Future<FiltersModel> get() async {
    try {
      final SharedPreferences pref = await SharedPreferences.getInstance();
      //var f = pref.getString('filtersData');
      //print(f.toString());
      return FiltersModel.fromJson(
          await jsonDecode(pref.getString('filtersData')));
    } catch (e) {
      log(e.toString());
      return null;
    }
  }
}
