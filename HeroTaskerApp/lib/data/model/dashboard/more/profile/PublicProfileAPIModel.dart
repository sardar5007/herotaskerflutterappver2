import 'package:aitl/data/model/auth/UserModel.dart';

class PublicProfileAPIModel {
  ErrorMessages errorMessages;
  Messages messages;
  bool success;
  ResponseData responseData;

  PublicProfileAPIModel(
      {this.errorMessages, this.messages, this.success, this.responseData});

  PublicProfileAPIModel.fromJson(Map<String, dynamic> json) {
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toMap();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toMap();
    }
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  List<dynamic> login;
  ErrorMessages({this.login});
  factory ErrorMessages.fromJson(Map<String, dynamic> j) {
    return ErrorMessages(
      login: j['login'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'login': login,
      };
}

class Messages {
  List<dynamic> login;
  Messages({this.login});
  factory Messages.fromJson(Map<String, dynamic> j) {
    return Messages(
      login: j['login'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'login': login,
      };
}

class ResponseData {
  UserModel user;
  ResponseData({this.user});
  ResponseData.fromJson(Map<String, dynamic> json) {
    user = json['User'] != null ? new UserModel.fromJson(json['User']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}
