import 'UserRatingSummaryDataModel.dart';

class UserRatingSummaryAPIModel {
  bool success;
  ResponseData responseData;

  UserRatingSummaryAPIModel({this.success, this.responseData});

  UserRatingSummaryAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  List<UserRatingSummaryDataModel> userRatingSummaryData;

  ResponseData({this.userRatingSummaryData});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['UserRatingSummaryData'] != null) {
      userRatingSummaryData = [];
      json['UserRatingSummaryData'].forEach((v) {
        try {
          userRatingSummaryData.add(new UserRatingSummaryDataModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userRatingSummaryData != null) {
      data['UserRatingSummaryData'] =
          this.userRatingSummaryData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
