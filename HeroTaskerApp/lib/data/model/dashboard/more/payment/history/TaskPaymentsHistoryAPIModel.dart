import 'package:aitl/data/model/dashboard/more/payment/history/TaskPaymentsModel.dart';

class TaskPaymentsHistoryAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  TaskPaymentsHistoryAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  TaskPaymentsHistoryAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<TaskPaymentsModel> taskPayments;
  ResponseData({this.taskPayments});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['TaskPayments'] != null) {
      taskPayments = [];
      json['TaskPayments'].forEach((v) {
        try {
          taskPayments.add(new TaskPaymentsModel.fromJson(v));
        } catch (e) {}
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.taskPayments != null) {
      data['TaskPayments'] = this.taskPayments.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
