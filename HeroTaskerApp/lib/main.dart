import 'dart:io';

import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserDevice.dart';
import 'package:aitl/view/welcome/welcome_page.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:mypkg/controller/PinCert.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'Mixin.dart';
import 'config/cfg/AppDefine.dart';
import 'config/server/Server.dart';
import 'config/theme/theme_types/app_themes.dart';

//  https://medium.com/flutterdevs/flutter-getx-package-cd4a5ce48ce8
//import 'package:permission_handler/permission_handler.dart';
//  https://javiercbk.github.io/json_to_dart/
//  https://shapeshifter.design
//  https://github.com/fluttercommunity/flutter_after_layout
/*
If you want the whole table to be Centered, use the mainAxisAlignment property of Column.

Column
mainAxisAlignment: MainAxisAlignment.center //Center Column contents vertically,
crossAxisAlignment: CrossAxisAlignment.center //Center Column contents horizontally,

Row
mainAxisAlignment: MainAxisAlignment.center //Center Row contents horizontally,
crossAxisAlignment: CrossAxisAlignment.center //Center Row contents vertically,
*/
void main() async {
  //  set permission for webRTC
  WidgetsFlutterBinding.ensureInitialized();

  // set the publishable key for Stripe - this is mandatory
  //Stripe.publishableKey = StripeCfg.PUBLISH_KEY;

  /*await PermissionHandler().requestPermissions([
    PermissionGroup.camera,
    //PermissionGroup.location,
    PermissionGroup.storage,
  ]);*/

  //  socket.io
  //HttpOverrides.global = new MyHttpOverrides();
  //  firebase
  await Firebase.initializeApp();
  //  device settings
  await SystemChrome.setPreferredOrientations(
    [
      DeviceOrientation.portraitUp,
      //DeviceOrientation.landscapeLeft,
      //DeviceOrientation.landscapeRight
    ],
  );
  //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
        statusBarColor: Colors.white, statusBarIconBrightness: Brightness.dark),
  );

  //  error catching
  FlutterError.onError = (FlutterErrorDetails details) async {
    //if (!Server.ISLIVE) {
    print("mail::FlutterErrorDetails" + details.toString());
    //}
    /*final map = Map<String, dynamic>();
    map['key'] = Define.EMAIL_KEY;
    map['subject'] = Server.APP_NAME + "::FlutterErrorDetails";
    map['msg'] =
        "<html><body><center><h1>" + details.toString() + "</h1></body></html>";
    Map<String, String> headers = {"Accept": "application/json"};
    await http.post(Define.EMAIL_URL, headers: headers, body: map);*/
  };

  EasyLoading.instance
    ..displayDuration = const Duration(seconds: 3)
    ..loadingStyle = EasyLoadingStyle.custom
    ..textColor = Colors.black
    ..textStyle =
        TextStyle(fontFamily: "Roboto", fontSize: 17, color: Colors.white)
    ..backgroundColor = MyTheme.appbarDarkColor
    ..indicatorColor = Colors.white
    ..maskColor = Colors.white
    ..indicatorType = EasyLoadingIndicatorType.fadingCircle
    ..maskType = EasyLoadingMaskType.clear
    ..userInteractions = false;

  //  get device info
  userDevice.setUserDevice();

  //  Resolved issue: handshake error in client flutter dio BY munir@AITL
  HttpOverrides.global = new MyHttpOverrides();

  runApp(MyApp());
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class MyApp extends StatelessWidget with Mixin {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
        debugShowCheckedModeBanner: false,
        smartManagement: SmartManagement.full,
        enableLog: Server.isTest,
        defaultTransition: Transition.fade,
        title: AppDefine.APP_NAME,
        theme: MyTheme.themeData,
        darkTheme: AppThemes.dark,
        //theme: AppThemes.dark,
        themeMode: ThemeMode.system,
        builder: (context, widget) {
          // do your initialization here
          widget = EasyLoading.init()(
              context, widget); // assuming this is returning a widget
          /*widget = ResponsiveWrapper.builder(
            widget,
            maxWidth: 1200,
            minWidth: 480,
            defaultScale: true,
            breakpoints: [
              ResponsiveBreakpoint.resize(480, name: MOBILE),
              ResponsiveBreakpoint.autoScale(800, name: TABLET),
              ResponsiveBreakpoint.resize(1000, name: DESKTOP),
              ResponsiveBreakpoint.autoScale(2460, name: '4K'),
            ],
          );*/
          return widget;
        },
        home: WelcomeScreen());
    //home: Test());
  }
}

class Test extends StatefulWidget {
  @override
  State createState() => _TestState();
}

class _TestState extends State<Test> with Mixin {
  @override
  void initState() {
    super.initState();
    //wsSrv();
    Future.delayed(Duration.zero, () async {
      // ?s over, navigate to a new page
      Get.to(() => WebScreen(
            //'http://192.168.1.100/mm/'
            url: 'http://192.168.24.181/test.php',
            title: 'test',
          ));
    });
  }

  @mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: MyTheme.bgColor,
        body: Container(),
      ),
    );
  }
}
